package metatransform.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import wis.WAttribute;
import wis.WChange;
import wis.WEntity;
import wis.WModel;
import wis.WPackage;
import wis.type.EChangesType;

public class History {
	
	private static History instance=null;
	
	private WModel model;
	
	protected History(){
		
	}

	public static History getInstance(){
		if(instance==null)
			instance=new History();
		return instance;
	}
	
	public WModel getModel() {
		return model;
	}

	public void setModel(WModel model) {
		this.model = model;
	}
	
	public boolean isDeleted(WAttribute attr){
		WChange ch=getChange(createPath(attr));
		if(ch==null || ch.getType().getValue()!=EChangesType.DELETE_VALUE)
			return false;
		return true;
	}
	
	public boolean isInserted(WAttribute attr){
		WChange ch=getChange(createPath(attr));
		if(ch==null || ch.getType().getValue()!=EChangesType.INSERT_VALUE)
			return false;
		return true;
	}
	
	public List<WAttribute> getDeletedAttributes(WEntity entity){
		List<WChange> changes = getChangeStart(createPath(entity));
		List<WAttribute> result= new ArrayList<WAttribute>();
		for(WChange c:changes){
			if(c.getType().getValue()==EChangesType.DELETE_VALUE){
				result.add((WAttribute)c.getElement());
			}
		}
		return result;
	}
	
	protected static String getPackage(WEntity entity){
		String path="";
		Object pack = entity.eContainer();
		while(pack != null && pack instanceof WPackage){
			path+="."+((WPackage)pack).getName();
			pack = ((WPackage)pack).eContainer();
		}
		return path.substring(1);
	}
	public static String createPath(EObject element){
		if(element.eContainer() instanceof WChange){
			return ((WChange)element.eContainer()).getPath();
		}
		
		String path="package.";
		if(element instanceof WAttribute){
			WAttribute attribute = (WAttribute) element;
			path+=getPackage((WEntity)(attribute.eContainer()));
			path+=".entity."+((WEntity)(attribute.eContainer())).getName()+".attribute."+attribute.getName();
		}
		if(element instanceof WEntity){
			WEntity entity = (WEntity) element;
			path+=getPackage(entity);
			path+=".entity."+entity.getName();
		}
		return path;
	}
	
	public WChange getChange(String path){
		List<WChange> changes=instance.model.getChangesContainer().getChanges();
		List<WChange> results = new ArrayList<WChange>();
		for(int i=changes.size()-1;i>=0;i--){
			WChange c= changes.get(i);
			if(c.getPath().equals(path)){
				results.add(c);
			}
		}
		
		results = filterChanges(results);
		if(results==null || results.size()==0){
			return null;
		}
		return results.get(0);
	}
	
	public List<WChange> getChangeStart(String path){
		List<WChange> result=new ArrayList<WChange>();
		List<WChange> changes=instance.model.getChangesContainer().getChanges();
		for(int i=changes.size()-1;i>=0;i--){
			WChange c= changes.get(i);
			if(c.getPath().startsWith(path)){
				result.add(changes.get(i));
			}
		}
		return filterChanges(result);
	}
	
	protected List<WChange> filterChanges (List<WChange> changes){
		HashMap<String, WChange> versions = new HashMap<String, WChange>();
		List<WChange> filtered = new ArrayList<WChange>();
		
		filtered.addAll(changes);
		Iterator<WChange> i = filtered.iterator();
		while(i.hasNext()){
			WChange change = i.next();
			if (change.getVersion()<model.getDebugVersion()){
				i.remove();
			}
		}
		
		for(WChange c: changes){
			if(versions.containsKey(c.getPath())){
				if(c.getVersion()>versions.get(c.getPath()).getVersion()){
					versions.put(c.getPath(), c);
				}
			}else{
				versions.put(c.getPath(), c);
			}
		}
		
		filtered=new ArrayList<WChange>();
		filtered.addAll(versions.values());
		return filtered;
	}
	
	public WChange findChange(EObject element){
		return getChange(createPath(element));
	}
}
