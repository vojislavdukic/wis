package metatransform.util

import wis.WEntity

class UtilGenerator {
	static def generateLinkImports(WEntity entity,Paths.GeneratorType type){
	'''
		�FOR l : Util.getLinksFrom(entity)�
			import �Paths.fullQualifiedName(l.to.entity,type)�;
		�ENDFOR�
		�FOR l : Util.getLinksTo(entity)�
			import �Paths.fullQualifiedName(l.from.entity,type)�;
		�ENDFOR�
	'''
	}
}