package metatransform.util;

import java.util.ArrayList;
import java.util.List;

import wis.WAttribute;
import wis.WConnection;
import wis.WEntity;
import wis.WLink;
import wis.WPackage;

public class Util {

	public static List<WAttribute> getAttributes(WEntity entity){
		List<WAttribute> attributes=new ArrayList<WAttribute>();
		attributes.addAll(entity.getAttributes());
//		if(entity.getSuperClass()!=null){
//			attributes.addAll(getAttributes(entity.getSuperClass()));
//		}
		List<WAttribute> a = History.getInstance().getDeletedAttributes(entity);
//		System.out.println(a);
//		System.out.println(a.size());
		attributes.addAll(a);
		return attributes;
	}
	
	public static List<WLink> getLinksFrom(WEntity entity){
		List<WLink> links=new ArrayList<WLink>();
		WPackage pack=(WPackage) entity.eContainer();
		if(pack!=null){
			List<WLink> allLinks=pack.getLinks();
			for(WLink link: allLinks){
				if(link.getFrom().getEntity()==entity){
					links.add(link);
				}
			}
		}
		return links;
	}
	
	public static List<WLink> getLinksTo(WEntity entity){
		List<WLink> links=new ArrayList<WLink>();
		WPackage pack=(WPackage) entity.eContainer();
		if(pack!=null){
			List<WLink> allLinks=pack.getLinks();
			for(WLink link: allLinks){
				if(link.getTo().getEntity()==entity){
					links.add(link);
				}
			}
		}
		return links;
	}
	
	public static List<WConnection> getAllConnections(WEntity entity){
		List<WConnection> links=new ArrayList<WConnection>();
		WPackage pack=(WPackage) entity.eContainer();
		if(pack!=null){
			List<WLink> allLinks=pack.getLinks();
			for(WLink link: allLinks){
				if(link.getTo().getEntity()==entity){
					links.add(link.getFrom());
				}
				if(link.getFrom().getEntity()==entity){
					links.add(link.getTo());
				}
			}
		}
		return links;
	}
	
	public static String fu(String str){
		char first = Character.toUpperCase(str.charAt(0));
		return first + str.substring(1);
	}
	
	public static String fl(String str){
		char first = Character.toLowerCase(str.charAt(0));
		return first + str.substring(1);
	}
	
	public static String type(String t){
		if(t.equals("StringSingleline")||t.equals("StringMultiline")){
			return "String";
		}if(t.equals("Enumeration")){
//			return "Integer";
			return "String";
		}else{
			return t;
		}
	}
}
