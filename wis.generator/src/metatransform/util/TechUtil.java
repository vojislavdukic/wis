package metatransform.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import wis.WAttribute;
import wis.WChange;
import wis.WEntity;
import wis.WLink;
import wis.WModel;
import wis.WPackage;
import wis.WReport;
import wis.WisPackage;
import wis.xtext.WISStandaloneSetup;

public class TechUtil {
	
	public static void saveFile(String path, CharSequence cs) {
		File output = new File(path);
		PrintWriter pw = null;
		try {
			if (output.getParentFile() != null) {
				output.getParentFile().mkdirs();
			}			
			output.createNewFile();
			pw = new PrintWriter(new FileOutputStream(output));
			pw.print(cs);

			System.out.println("saved " + path);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (pw != null) {
				pw.close();
			}
		}
	}
	
	public static WModel loadEModel(String modulePath) {
		WModel wmodel=null;
		boolean isXmi=modulePath.endsWith(".xmi");
		boolean isXtext=modulePath.endsWith(".twis");
		WisPackage.eINSTANCE.eClass();
		URI uri=URI.createURI(modulePath);
		
		if(isXmi){
			Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
			Map<String, Object> m = reg.getExtensionToFactoryMap();
			m.put("xmi", new XMIResourceFactoryImpl());
			m.put("ecore", new EcoreResourceFactoryImpl());
			
			ResourceSet resSet = new ResourceSetImpl();
			Resource resource = resSet.getResource(uri, true);

			wmodel = (WModel) resource.getContents().get(0);
		}else if(isXtext){
			new WISStandaloneSetup().createInjectorAndDoEMFRegistration();
			ResourceSet resourceSet = new ResourceSetImpl();
			Resource resource = resourceSet.getResource(uri, true);
			wmodel = (WModel) resource.getContents().get(0);
		}
		prepareModel(wmodel);
		
		return wmodel;
	}
	
	public static void prepareModel(WModel model){
		for(WPackage p:model.getPackages()){
			prepareLabel(p);
		}
		
		for(WReport report:model.getReportsContainer().getReports()){
			prepareLabel(report);
		}
		
		for(WChange change:model.getChangesContainer().getChanges()){
			prepareLabel(change);
		}
	}
	
	public static void prepareLabel(WPackage pack){
		if(pack.getLabel()==null || pack.getLabel().trim().equals("")){
			pack.setLabel(createLabel(pack.getName()));
		}
		
		for(WEntity entity:pack.getEntities()){
			if(entity.getLabel()==null || entity.getLabel().trim().equals("")){
				entity.setLabel(createLabel(entity.getName()));
			}
			for(WAttribute attr:entity.getAttributes()){
				if(attr.getLabel()==null || attr.getLabel().trim().equals("")){
					attr.setLabel(createLabel(attr.getName()));
				}
			}
		}
		
		for(WLink link:pack.getLinks()){
			if(link.getFrom().getLabel()==null || link.getFrom().getLabel().trim().equals("")){
				if(link.getFrom().getName()!=null && !link.getFrom().getName().equals("")){
					link.getFrom().setLabel(createLabel(link.getFrom().getName()));
				}else{
					link.getFrom().setName(link.getFrom().getEntity().getName());
					link.getFrom().setLabel(createLabel(link.getFrom().getEntity().getName()));
				}
			}
			if(link.getTo().getLabel()==null || link.getTo().getLabel().trim().equals("")){
				if(link.getTo().getName()!=null && !link.getTo().getName().equals("")){
					link.getTo().setLabel(createLabel(link.getTo().getName()));
				}else{
					link.getTo().setName(link.getTo().getEntity().getName());
					link.getTo().setLabel(createLabel(link.getTo().getEntity().getName()));
				}
			}
		}
		
		for(WPackage p:pack.getSubpackages()){
			prepareLabel(p);
		}
	}
	
	public static void prepareLabel(WChange change){
		if(change.getElement()!=null && change.getElement() instanceof WAttribute){
			WAttribute attr= (WAttribute) change.getElement();
			if(attr.getLabel()==null || attr.getLabel().trim().equals("")){
				attr.setLabel(createLabel(attr.getName()));
			}
			attr.setVersion(change.getVersion());
		}
	}
	
	public static void prepareLabel(WReport report){
		if(report.getLabel()==null || report.getLabel().trim().equals("")){
			report.setLabel(createLabel(report.getName()));
		}
		for(WAttribute attr:report.getAttributes()){
			if(attr.getLabel()==null || attr.getLabel().trim().equals("")){
				attr.setLabel(createLabel(attr.getName()));
			}
		}
	}
	
	public static String createLabel(String name){
		name=Util.fu(name);
		Matcher matcher=Pattern.compile("([A-Z]+[a-z0-9]+)").matcher(name);
		matcher.find();
		String result=matcher.group(0);
		while (matcher.find()) {
			result+=" "+Util.fl(matcher.group(1));
		}
		return result;
	}
	
	
}
