package metatransform.util;

import java.util.ArrayList;
import java.util.List;

import wis.WEntity;
import wis.WModel;
import wis.WPackage;

public class Paths {
	public static final String appName="com.genericproject.server";
	public static final String appLabel="Generic App";
	
	//public static final String baseServerPath = "./generated/java";
	//public static final String baseClientPath = "./generated/app";
	
	public static final String baseServerPath = "../wis.frameworkServer/src/main/java";
	public static final String baseClientPath = "../wis.frameworkClient/src/app";
	
	//public static final String modelPath="../wis.core/model/EModel.xmi";
	public static final String modelPath="../wis.xtext/wis.xtext.Example/test.twis";
	public static final String dateFormat="dd/MM/yyyy";
	
	/**
	 * Get package name
	 * @param entity
	 * @return package name
	 */
	public static String packageName(WEntity entity){
		WPackage pack=(WPackage)entity.eContainer();
		String packageName=pack.getName();
		while(pack.eContainer() instanceof WPackage){
			packageName=pack.getName()+"."+packageName;
			pack=(WPackage)pack.eContainer();
		}
		return packageName;
	}
	
	public static String packageName(WPackage pack){
		String packageName=pack.getName();
		while(pack.eContainer() instanceof WPackage){
			packageName=pack.getName()+"."+packageName;
			pack=(WPackage)pack.eContainer();
		}
		return packageName;
	}
	
	public static List<WPackage> getAllPackages(WModel model){
		List<WPackage> packages=new ArrayList<WPackage>();
		for(WPackage p:model.getPackages()){
			packages.add(p);
			getAllPackagesRecursive(p, packages);
		}
		return packages;
	}
	
	protected static void getAllPackagesRecursive(WPackage pack, List<WPackage> list){
		for(WPackage p:pack.getSubpackages()){
			list.add(p);
			getAllPackagesRecursive(p,list);
		}
	}
	/**
	 * Generate full qualified name
	 * @param entity
	 * @param type
	 * @return
	 */
	public static String fullQualifiedName(WEntity entity, GeneratorType type){
		String prefix="";
		String sufix="";
		if(type==GeneratorType.JPA){
			prefix="model.jpa";
			sufix="Bean";
		}
		if(type==GeneratorType.Repository){
			prefix="model.repository";
			sufix="Repository";
		}
		if(type==GeneratorType.Controller){
			prefix="web.controller";
			sufix="Controller";
		}
		
		String fqn=packageName(entity);
		fqn=Paths.appName+"."+prefix.toLowerCase()+"."+fqn+"."+Util.fu(entity.getName())+sufix;
		
		return fqn;
	}
	
	public static enum GeneratorType{
		JPA,
		Repository,
		Controller,
		Client
	}
}
