package metatransform.generator;

import metatransform.generator.client.ClientGenerator;
import metatransform.generator.model.ModelGenerator;
import metatransform.generator.spring.SpringGenerator;

public class Generator {
	
	public static void main(String[] args){
		ModelGenerator.generateAll();
		ClientGenerator.generateAll();
		SpringGenerator.generateAll();
	}
}