package metatransform.generator.client;

import metatransform.generator.client.reports.AngularReportControllerGenerator;
import metatransform.generator.client.reports.AngularReportModuleGenerator;
import metatransform.generator.client.reports.AngularReportRouterGenerator;
import metatransform.util.Paths;
import metatransform.util.TechUtil;
import metatransform.util.Util;
import wis.WEntity;
import wis.WModel;
import wis.WPackage;
import wis.WReport;

public class ClientGenerator {
	public static AngularServiceGenerator angularService = new AngularServiceGenerator();
	public static AngularRouterGenerator angularRouter = new AngularRouterGenerator();
	public static AngularModuleGenerator angularModule = new AngularModuleGenerator();
	public static AngularMenuGenerator angularMenu = new AngularMenuGenerator();
	public static AngularControllerGenerator angularController = new AngularControllerGenerator();
	
	public static AngularReportRouterGenerator angularReportRouter = new AngularReportRouterGenerator();
	public static AngularReportModuleGenerator angularReportModule = new AngularReportModuleGenerator();
	public static AngularReportControllerGenerator angularReportController = new AngularReportControllerGenerator();
	
	public static void generateAll() {
		WModel emodel = TechUtil.loadEModel(Paths.modelPath);
		
		CharSequence cs;
		
		cs=angularMenu.generate(emodel);
		TechUtil.saveFile(Paths.baseClientPath+ "/"+"menu.tpl.html", cs);
		
		
		for(WPackage p:emodel.getPackages()){
			generateFromPackage(p);
		}

		//reports
		cs=angularReportRouter.generate(emodel);
		TechUtil.saveFile(Paths.baseClientPath+ "/reports/router/rtReport.js", cs);
		
		cs=angularReportModule.generate();
		TechUtil.saveFile(Paths.baseClientPath+ "/reports/app.js", cs);
		
		for(WReport report: emodel.getReportsContainer().getReports()){
			generateFromReport(report);
		}
			
	}
	
	public static void generateFromPackage(WPackage pack) {
		String packageName=Paths.packageName(pack).replace('.', '_');
		CharSequence cs;
		
		cs=angularRouter.generate(pack);
		TechUtil.saveFile(Paths.baseClientPath+ "/"+packageName+"/router/rt"+Util.fu(pack.getName())+".js", cs);
		
		cs=angularModule.generate(pack);
		TechUtil.saveFile(Paths.baseClientPath+ "/"+packageName+"/app.js", cs);
		
		for(WEntity entity: pack.getEntities()){
			cs=angularService.generate(entity);
			TechUtil.saveFile(Paths.baseClientPath+ "/"+packageName+"/service/srv"+Util.fu(entity.getName())+".js", cs);
			
			cs=angularController.generate(entity);
			TechUtil.saveFile(Paths.baseClientPath+ "/"+packageName+"/controller/ctrl"+Util.fu(entity.getName())+".js", cs);
		}
		
		for(WPackage p: pack.getSubpackages()){
			generateFromPackage(p);
		}	
	}
	
	public static void generateFromReport(WReport report) {
		CharSequence cs;
		
		cs=angularReportController.generate(report);
		TechUtil.saveFile(Paths.baseClientPath+ "/reports/controller/ctrlReport"+Util.fu(report.getName())+".js", cs);
	}
}
