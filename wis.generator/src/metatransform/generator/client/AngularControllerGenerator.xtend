package metatransform.generator.client

import metatransform.util.History
import metatransform.util.Paths
import metatransform.util.Util
import wis.WEntity
import wis.type.ECardinalityMax
import wis.type.EType

class AngularControllerGenerator{
	
	def generate(WEntity entity) {
    '''
		'use strict';

		angular.module('WIS.�Paths.packageName(entity)�')
		.controller('ctrl�entity.name.toFirstUpper�',['$scope',
		function($scope){
			$scope.model={
				label:'�entity.label�',
				serviceName:'Service�entity.name.toFirstUpper�',
				fields:[
					�FOR attr:Util.getAttributes(entity) SEPARATOR ','�
						{
							label:'�attr.label�',
							name:'�attr.name.toFirstLower�',
							type:'�fieldType(attr.type)�',
							�IF attr.type==EType.STRING_MULTILINE�multiline: true,�ENDIF�
							�IF attr.type==EType.DATE�dateFormat: '�Paths.dateFormat�',�ENDIF�
							�IF attr.type==EType.ENUMERATION�enumValues: [�FOR value:attr.enumeration.elements SEPARATOR ','�'�value.name�'�ENDFOR�],�ENDIF�
							�IF attr.disabled�disabled: true,�ENDIF�
							�IF attr.message!=null && !attr.message.equals("")�message: '�attr.message�',�ENDIF�
							�IF History.getInstance().isDeleted(attr)�
								disabled: true,
								classes: 'border-red',
							�ENDIF�
							�IF History.getInstance().isInserted(attr)�
								classes: 'border-green',
							�ENDIF�
							�IF History.getInstance().isDeleted(attr)�
								important: false,
							�ELSE�
								important: �attr.important�,
							�ENDIF�
							�IF History.getInstance().findChange(attr)!=null�
								message: '�History.getInstance().findChange(attr).getVersion()�. �History.getInstance().findChange(attr).getMessage()�',
							�ENDIF�
							validation:{
								required: �!attr.allowNull�,
								vType: '�attr.type.toString.toLowerCase�',
								customValidation:[
									�IF attr.validationRegex!=null && attr.validationRegex.length>0�
									{
										regexp: '�attr.validationRegex�',
										message: '�attr.validationMessage�'
									}
									�ENDIF�	
								]
							}
						}
					�ENDFOR�
				],
				relationships:[
					�generateLink(entity)�
				]
			};
		}]);
    '''}
    
    def fieldType(EType type){
    	if(type==EType.BOOLEAN){
    		return "checkbox";
    	}else if(type==EType.ENUMERATION){
    		return "combobox"
    	}else{
    		return "text";
    	}
    }
    
    def generateLink(WEntity entity){
    	'''
		�FOR conn : Util.getAllConnections(entity) SEPARATOR ','�
		{
			entity: {
				label:'�conn.entity.label�',
				serviceName:'Service�conn.entity.name.toFirstUpper�',
				fields:[
					�FOR attr:conn.entity.attributes SEPARATOR ','�
						{
							label:'�attr.label�',
							name:'�attr.name�',
							type:'�fieldType(attr.type)�',
							�IF attr.type==EType.STRING_MULTILINE�multiline: true,�ENDIF�
							�IF attr.type==EType.DATE�dateFormat: '�Paths.dateFormat�',�ENDIF�
							�IF attr.type==EType.ENUMERATION�enumValues: [�FOR value:attr.enumeration.elements SEPARATOR ','�'�value.name�'�ENDFOR�],�ENDIF�
							important: �attr.important�
						}
					�ENDFOR�
				]
			},
			label: '�conn.label�',
			name: '�conn.name�',
			editable: �conn.editable�,
			type: '�IF conn.max==ECardinalityMax.MANY�many�ELSE�one�ENDIF�'
		}
		�ENDFOR�
    	'''
    }
}