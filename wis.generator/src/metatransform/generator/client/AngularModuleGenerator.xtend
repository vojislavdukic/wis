package metatransform.generator.client

import metatransform.util.Paths
import wis.WPackage

class AngularModuleGenerator{
	
	def generate(WPackage pack) {
    '''
		'use strict';
		
		angular.module('WIS.�Paths.packageName(pack)�', [
		    'ngRoute',
		    'ngResource',
			'smart-table',
			'dialogs.main',
		    'checklist-model'
		]);
    '''}
}