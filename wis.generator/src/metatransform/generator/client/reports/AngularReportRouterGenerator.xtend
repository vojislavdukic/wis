package metatransform.generator.client.reports

import metatransform.util.Paths
import wis.WModel

class AngularReportRouterGenerator{
	
	def generate(WModel model) {
    '''
		'use strict';

		angular.module('WIS.reports').config(['$routeProvider', function ($routeProvider) {
		�IF model.reportsContainer.reports.length!=0�
		$routeProvider
		  	�FOR report:model.reportsContainer.reports�
		  	.when('/reports/�report.name.toLowerCase�', {
			  	templateUrl:'common/directive/reports/view/tmplGenericViewReport.tpl.html',
			  	controller:'ctrlReport�report.name.toFirstUpper�'
		  	})
		    �ENDFOR�;
		�ENDIF�
		}]);
    '''}
}