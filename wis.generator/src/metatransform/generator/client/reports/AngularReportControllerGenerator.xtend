package metatransform.generator.client.reports

import wis.WReport
import wis.type.EType

class AngularReportControllerGenerator {
	def generate(WReport report) {
    '''
		'use strict';

		angular.module('WIS.reports')
		.controller('ctrlReport�report.name.toFirstUpper�',['$scope',
		function($scope){
			$scope.model={
				label:'�report.label.toFirstUpper�',
				name:'�report.name.toFirstLower�',
				fields:[
					�FOR attribute:report.attributes SEPARATOR ','�
						{
							label:'�attribute.label�',
							name:'�attribute.name�',
							type:'�fieldType(attribute.type)�',
						}
					�ENDFOR�
				]
			};
		}]);
    '''}
    
    def fieldType(EType type){
    	if(type==EType.BOOLEAN){
    		return "checkbox";
    	}else{
    		return "text";
    	}
    }
    
}