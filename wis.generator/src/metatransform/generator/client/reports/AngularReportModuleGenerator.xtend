package metatransform.generator.client.reports

class AngularReportModuleGenerator {
	def generate() {
    '''
		'use strict';
		
		angular.module('WIS.reports', [
		    'ngRoute',
		    'ngResource',
			'smart-table',
			'dialogs.main',
		    'checklist-model'
		]);
    '''}
}