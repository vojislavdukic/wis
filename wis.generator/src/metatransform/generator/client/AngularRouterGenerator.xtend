package metatransform.generator.client

import metatransform.util.Paths
import wis.WPackage

class AngularRouterGenerator{
	
	def generate(WPackage pack) {
    '''
		'use strict';

		angular.module('WIS.�Paths.packageName(pack)�').config(['$routeProvider', function ($routeProvider) {
		  $routeProvider
		  	�FOR entity:pack.entities AFTER ';'�
		  	.when('/�Paths.packageName(entity).replace(".","/")�/�entity.name.toLowerCase�/', {
		  		templateUrl:'common/directive/GenericView/tmplGenericView.tpl.html',
		  		controller:'ctrl�entity.name.toFirstUpper�'
		  	})
		    �ENDFOR�
		}]);
    '''}
}