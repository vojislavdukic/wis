package metatransform.generator.client

import metatransform.util.Paths
import wis.WEntity

class AngularServiceGenerator{
	
	def generate(WEntity entity) {
    '''
		'use strict';
		
		angular.module('WIS.�Paths.packageName(entity)�').factory('Service�entity.name.toFirstUpper�', ['restResource', function ($restResource) {
		  return $restResource('�Paths.packageName(entity).replace(".","/")�/�entity.name.toLowerCase�');
		}]);
    '''}
}