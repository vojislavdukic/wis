package metatransform.generator.client

import metatransform.util.Paths
import wis.WModel

class AngularMenuGenerator{
	
	def generate(WModel model) {
    '''
		<div class="sidebar-nav">
			<div class="navbar navbar-default" role="navigation">
				<div class="navbar-collapse collapse sidebar-navbar-collapse">
		            <ul class="nav navbar-nav">
		                <li class="active"><a href="#/">�Paths.appLabel�</a></li>
		                �FOR pack:Paths.getAllPackages(model)�
		                <li class="dropdown">
		                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">�pack.label� <span class="caret"></span></a>
		                    <ul class="dropdown-menu" role="menu">
		                    	�FOR entity:pack.entities�
		                    	<li><a href="#/�Paths.packageName(entity).replace(".","/")�/�entity.name.toLowerCase�">�entity.label�</a></li>
		                        �ENDFOR�
		                    </ul>
		                </li>
		                �ENDFOR�
		                <li class="dropdown">
		                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Username <span class="caret"></span></a>
		                    <ul class="dropdown-menu" role="menu">
		                        <li><a>Proflie</a></li>
		                        <li><a >Logout</a></li>
		                    </ul>
		                </li>
		                <li class="dropdown">
		                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Reports<span class="caret"></span></a>
		                    <ul class="dropdown-menu" role="menu">
		                    	�FOR report:model.reportsContainer.reports�
		                    	<li><a href="#/reports/�report.name.toLowerCase�">�report.label�</a></li>
		                        �ENDFOR�
		                    </ul>
		                </li>
		            </ul>
		        </div>
		    </div>
		</div>

    '''}
}