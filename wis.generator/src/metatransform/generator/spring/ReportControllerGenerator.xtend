package metatransform.generator.spring

import metatransform.util.Paths
import wis.WModel

class ReportControllerGenerator {
	def generate(WModel model) {
	'''
		package �Paths.appName�.web.controller.reports;

		import java.io.OutputStream;
		import java.sql.Connection;
		import java.text.SimpleDateFormat;
		import java.util.HashMap;
		import java.util.Map;
		
		import javax.servlet.ServletContext;
		import javax.servlet.http.HttpServletRequest;
		import javax.servlet.http.HttpServletResponse;
		import javax.sql.DataSource;
		
		import net.sf.jasperreports.engine.JasperReport;
		import net.sf.jasperreports.engine.JasperRunManager;
		import net.sf.jasperreports.engine.util.JRLoader;
		
		import org.springframework.beans.factory.annotation.Autowired;
		import org.springframework.beans.factory.annotation.Qualifier;
		import org.springframework.stereotype.Controller;
		import org.springframework.web.bind.annotation.RequestBody;
		import org.springframework.web.bind.annotation.RequestMapping;
		import org.springframework.web.bind.annotation.RequestMethod;
		
		@Controller
		@RequestMapping("/report")
		public class ReportController {
		
			@Autowired
			private ServletContext servletContext;
			
			@Autowired
			@Qualifier("reportsDataSource")
			private DataSource ds;
		
			�FOR report:model.reportsContainer.reports  SEPARATOR '\n' AFTER '\n'�
				@RequestMapping(value = "/�report.name�", method = RequestMethod.POST)
				public void �report.name.toFirstLower�Report(@RequestBody Map<String, Object> params,
					HttpServletRequest request, HttpServletResponse response) throws Exception {
				
					HashMap<String,Object> parameters = new HashMap<String,Object>();
					�FOR attribute:report.attributes�
						parameters.put("�attribute.name�", params.get("�attribute.name�"));
					�ENDFOR�
					printReport(parameters,response,"�report.name�");
				}
		    �ENDFOR�
		    
			public void printReport(HashMap<String,Object> parameters,HttpServletResponse response,String reportName) throws Exception{
				JasperReport jasperReport = null;
				jasperReport = (JasperReport) JRLoader.loadObject(servletContext.getResourceAsStream("/WEB-INF/classes/reports/"+reportName+".jasper"));
				Connection connection=ds.getConnection();
		        byte[] byteStream = JasperRunManager.runReportToPdf(jasperReport, parameters, connection);                            
		        OutputStream outStream = response.getOutputStream();
		        response.setHeader("Content-Disposition","inline; filename=report.pdf");
		        response.setContentType("application/pdf");
		        response.setContentLength(byteStream.length);
		        outStream.write(byteStream,0,byteStream.length); 
		        response.flushBuffer();
		        outStream.close();
		        connection.close();
			}
		}
		
	'''
	}
}