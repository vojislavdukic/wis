package metatransform.generator.spring;

import metatransform.util.Paths;
import metatransform.util.TechUtil;
import wis.WEntity;
import wis.WModel;
import wis.WPackage;

public class SpringGenerator {
	public static ControllerGenerator controller = new ControllerGenerator();
	public static ReportControllerGenerator reportController = new ReportControllerGenerator();
	
	public static void generateAll() {
		WModel emodel = TechUtil.loadEModel(Paths.modelPath);
		
		for(WPackage p:emodel.getPackages()){
			generateFromPackage(p);
		}
		
		if(emodel.getReportsContainer().getReports().size()!=0){
			CharSequence cs;
			cs=reportController.generate(emodel);
			TechUtil.saveFile(Paths.baseServerPath+"/"+Paths.appName.replace('.', '/')+"/web/controller/reports/ReportController.java", cs);
		}
	}
	
	public static void generateFromPackage(WPackage pack) {
		for(WEntity entity: pack.getEntities()){
			CharSequence cs;
			
			cs=controller.generate(entity);
			TechUtil.saveFile(Paths.baseServerPath+ "/"+Paths.fullQualifiedName(entity, Paths.GeneratorType.Controller).replace('.', '/')+".java", cs);
		}
		
		for(WPackage p: pack.getSubpackages()){
			generateFromPackage(p);
		}	
	}
	
}
