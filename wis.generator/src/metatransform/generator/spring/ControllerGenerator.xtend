package metatransform.generator.spring

import metatransform.util.Paths
import wis.WEntity

class ControllerGenerator {
	def generate(WEntity entity) {
		var packageName=Paths.packageName(entity)
		
		var className=entity.name.toFirstUpper+"Bean"
		var fieldName=entity.name.toFirstLower
		var controllerName=entity.name.toFirstUpper+"Controller"
		var repositoryClassName=entity.name.toFirstUpper+"Repository"
		var repositoryField=fieldName+"Repository"
    '''
		package �Paths.appName�.web.controller.�Paths.packageName(entity)�;

		import java.util.List;

		import org.slf4j.Logger;
		import org.springframework.beans.factory.annotation.Autowired;
		import org.springframework.web.bind.annotation.PathVariable;
		import org.springframework.web.bind.annotation.RequestMapping;
		import org.springframework.web.bind.annotation.RequestMethod;
		import org.springframework.web.bind.annotation.RestController;
		import org.springframework.web.bind.annotation.PathVariable;
		import org.springframework.web.bind.annotation.RequestBody;
		
		import com.genericproject.server.aspect.logging.util.Loggable;
		
		import �Paths.appName�.model.repository.�packageName�.�repositoryClassName�;
		import �Paths.appName�.model.jpa.�packageName�.�className�;
		
		@RestController
		@RequestMapping("/�packageName.replace('.','/')�/�fieldName�")
		public class �controllerName� {
			
			@Loggable
			Logger logger;
		
			@Autowired
			�repositoryClassName� �repositoryField�;
		
			@RequestMapping(method = RequestMethod.GET)
			public List<�className�> getAll() {
				List<�className�> retVal = �repositoryField�.findAll();
				return  retVal;
			}
			
			@RequestMapping(value = "/{id}", method = RequestMethod.GET)
			public �className� get(@PathVariable("id") Long id) {		
				return �repositoryField�.findOne(id);
			}
			
			@RequestMapping(value = "/{id}/eager", method = RequestMethod.GET)
			public �className� getEager(@PathVariable("id") Long id) {		
				return �repositoryField�.findOneEager(id);
			}
		
			@RequestMapping(method = RequestMethod.POST)
			public �className� insert(@RequestBody �className� �className.toFirstLower�) {
				return �repositoryField�.save(�className.toFirstLower�);
			}
			
			@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
			public String delete(@PathVariable("id") Long id) {
				�repositoryField�.delete(id);
				return "Ok";
			}
		
			@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
			public �className� update(@PathVariable("id") Long id, @RequestBody �className� �className.toFirstLower�) {
				�repositoryField�.save(�className.toFirstLower�);
				return �className.toFirstLower�;
			}
		}
    '''}
}