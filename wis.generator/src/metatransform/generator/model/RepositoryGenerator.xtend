package metatransform.generator.model

import metatransform.util.Paths
import wis.WEntity

class RepositoryGenerator{
	
	def generate(WEntity entity) {
    '''
		package �Paths.appName�.model.repository.�Paths.packageName(entity)�;
		
		import org.springframework.stereotype.Repository;
		import �Paths.appName�.model.repository.util.GenericRepository;
		import �Paths.appName�.model.jpa.�Paths.packageName(entity)�.�entity.name.toFirstUpper�Bean;
		
		@Repository
		public interface �entity.name.toFirstUpper�Repository extends GenericRepository<�entity.name.toFirstUpper�Bean, Long>{

		}
	'''}
	
}