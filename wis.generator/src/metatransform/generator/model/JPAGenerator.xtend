package metatransform.generator.model

import metatransform.util.Paths
import metatransform.util.Util
import metatransform.util.UtilGenerator
import wis.WEntity
import wis.WLink
import wis.type.ECardinalityMax
import wis.type.ECardinalityMin
import wis.type.EType

class JPAGenerator{
	
	def generate(WEntity entity) {
    '''
		package �Paths.appName�.model.jpa.�Paths.packageName(entity)�;
		
		import java.util.List;
		import java.util.Date;
		import javax.persistence.Id;
		import javax.persistence.Column;
		import javax.persistence.Entity;
		import javax.persistence.Table;
		import javax.persistence.JoinColumn;
		import javax.persistence.JoinTable;
		import javax.persistence.OneToOne;
		import javax.persistence.OneToMany;
		import javax.persistence.ManyToOne;
		import javax.persistence.ManyToMany;
		import javax.persistence.Version;
		import java.io.Serializable;
		import javax.persistence.GeneratedValue;
		import javax.persistence.GenerationType;
		import javax.persistence.FetchType;
		import javax.persistence.CascadeType;
		import org.hibernate.annotations.Where;
		import com.fasterxml.jackson.annotation.JsonInclude;
		import com.fasterxml.jackson.annotation.JsonInclude.Include;
		import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
		import com.fasterxml.jackson.annotation.JsonFormat;
		
		
		�UtilGenerator.generateLinkImports(entity,Paths.GeneratorType.JPA)�
		
		@Entity
		@Table(name="�entity.name.toLowerCase�")
		@JsonInclude(Include.NON_NULL)
		@JsonIgnoreProperties({"$resolved","isSelected","$promise"})
		public class �entity.name.toFirstUpper�Bean implements Serializable {
			
			@Id
			@GeneratedValue(strategy = GenerationType.AUTO)
			@Column(name = "�entity.name.toLowerCase�_id")
			protected Long id;
			�FOR a : Util.getAttributes(entity) SEPARATOR '\n' AFTER '\n'�
			@Column(nullable = �a.allowNull�,name="�a.name.toLowerCase�"�IF (a.type.value==EType.STRING_SINGLELINE)||(a.type.value==EType.STRING_MULTILINE)�,length=�a.length��ENDIF�)
			�IF a.type==EType.DATE�@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="�Paths.dateFormat�") �ENDIF� 
			�generateAttr(a.type.literal,a.name.toFirstLower)�
			�ENDFOR�
			�FOR l : Util.getLinksFrom(entity) SEPARATOR '\n' AFTER '\n'�
			�generateLinkFrom(l)�
			�ENDFOR�
			�FOR l : Util.getLinksTo(entity) SEPARATOR '\n' AFTER '\n'�
			�generateLinkTo(l)�
			�ENDFOR�
			public Long getId() {
				return id;
			}
		
			public void setId(Long id) {
				this.id = id;
			}
			
			�FOR a : Util.getAttributes(entity)  SEPARATOR '\n' AFTER '\n'�
			�generateGaS(a.type.literal,a.name.toFirstLower)�
			�ENDFOR�
			�FOR l : Util.getLinksFrom(entity) SEPARATOR '\n' AFTER '\n'�
			�generateLinkGaS(l.to.name,l.to.entity.name,l.to.min,l.to.max)�
			�ENDFOR�
			�FOR l : Util.getLinksTo(entity)  SEPARATOR '\n' AFTER '\n'�
			�generateLinkGaS(l.from.name,l.from.entity.name,l.to.min,l.from.max)�
			�ENDFOR�
		}
    '''}
    
    def generateAttr(String type, String name) {
    '''
		private �Util.type(type)� �name� �IF type=="Boolean"�= false�ENDIF��IF type=="Date"�= new Date()�ENDIF�;
    '''}
    
    def generateGaS(String type, String name) {
    '''
    	public void set�name.toFirstUpper�(�Util.type(type)� �name�){
    		this.�name�=�name�;
    	}
    	
    	public �Util.type(type)� get�name.toFirstUpper�(){
    		return �name�;
    	}
    '''}
    
    def generateLinkFrom(WLink link) {
    '''
    	�IF link.to.max.value==ECardinalityMax.MANY_VALUE�
    		�IF link.from.max.value==ECardinalityMax.ONE_VALUE�
    		@OneToMany(mappedBy = "�link.from.name.toFirstLower�", fetch = FetchType.LAZY, cascade = { CascadeType.REFRESH }) 
			�ELSE�
			@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
			@JoinTable(
				name="�link.from.name.toLowerCase�_�link.to.name.toLowerCase�",
				joinColumns={@JoinColumn(name="�link.from.name.toLowerCase�_�link.from.entity.name.toLowerCase�_id", nullable = false, updatable = false)},
				inverseJoinColumns={@JoinColumn(name="�link.to.name.toLowerCase�_�link.to.entity.name.toLowerCase�_id", nullable = false, updatable = false)})
			�ENDIF�
			private List<�link.to.entity.name.toFirstUpper�Bean> �link.to.name.toFirstLower�;
    	�ELSE�
    		�IF link.from.max.value==ECardinalityMax.ONE_VALUE�
    		@OneToOne(fetch = FetchType.LAZY,cascade=CascadeType.REFRESH)
			@JoinColumn(name = "�link.to.name.toLowerCase�_�link.to.entity.name.toLowerCase�_id")
			�ELSE�
			@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
			@JoinColumn(name = "�link.to.name.toLowerCase�_�link.to.entity.name.toLowerCase�_id")
			�ENDIF�
			private �link.to.entity.name.toFirstUpper�Bean �link.to.name.toFirstLower�;
    	�ENDIF�
    '''}
    
    def generateLinkTo(WLink link) {
    '''
    	�IF link.from.max.value==ECardinalityMax.MANY_VALUE�
    		�IF link.to.max.value==ECardinalityMax.ONE_VALUE�
			@OneToMany(mappedBy = "�link.to.name.toFirstLower�", fetch = FetchType.LAZY, cascade = { CascadeType.REFRESH })
			�ELSE�
			@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
			@JoinTable(
				name="�link.from.name.toLowerCase�_�link.to.name.toLowerCase�",
				joinColumns={@JoinColumn(name="�link.to.name.toLowerCase�_�link.to.entity.name.toLowerCase�_id", nullable = false, updatable = false)},
				inverseJoinColumns={@JoinColumn(name="�link.from.name.toLowerCase�_�link.from.entity.name.toLowerCase�_id", nullable = false, updatable = false)})
			�ENDIF�
			private List<�link.from.entity.name.toFirstUpper�Bean> �link.from.name.toFirstLower�;
    	�ELSE�
    		�IF link.to.max.value==ECardinalityMax.ONE_VALUE�
			@OneToOne(mappedBy="�link.to.name.toFirstLower�",fetch = FetchType.LAZY,cascade=CascadeType.REFRESH)
			�ELSE�
			@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
			@JoinColumn(name = "�link.from.name.toLowerCase�_�link.from.entity.name.toLowerCase�_id")
			�ENDIF�
			private �link.from.entity.name.toFirstUpper�Bean �link.from.name.toFirstLower�;				
    	�ENDIF�
    '''}
    
    def generateLinkGaS(String linkName, String entityName, ECardinalityMin minc, ECardinalityMax maxc) {
    '''
    	�IF maxc.value==ECardinalityMax.MANY_VALUE�
    		public void set�linkName.toFirstUpper�(List<�entityName.toFirstUpper�Bean> �linkName.toFirstLower�){
    			this.�linkName.toFirstLower�=�linkName.toFirstLower�;
    		}
    		
    		public List<�entityName.toFirstUpper�Bean> get�linkName.toFirstUpper�(){
    			return �linkName.toFirstLower�;
    		}
    	�ELSE�
    		public void set�linkName.toFirstUpper�(�entityName.toFirstUpper�Bean �linkName.toFirstLower�){
    			this.�linkName.toFirstLower�=�linkName.toFirstLower�;
    		}
    		
    		public �entityName.toFirstUpper�Bean get�linkName.toFirstUpper�(){
    			return �linkName.toFirstLower�;
    		}
    	�ENDIF�
    '''}

}