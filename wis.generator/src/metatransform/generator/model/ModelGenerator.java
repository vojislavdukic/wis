package metatransform.generator.model;

import metatransform.util.History;
import metatransform.util.Paths;
import metatransform.util.TechUtil;
import wis.WEntity;
import wis.WModel;
import wis.WPackage;

public class ModelGenerator {
	public static JPAGenerator jpa = new JPAGenerator();
	public static RepositoryGenerator repository = new RepositoryGenerator();
	
	public static void generateAll() {
		WModel emodel = TechUtil.loadEModel(Paths.modelPath);
		
		History.getInstance().setModel(emodel);
		
		for(WPackage p:emodel.getPackages()){
			generateFromPackage(p);
		}
	}
	
	public static void generateFromPackage(WPackage pack) {
		for(WEntity entity: pack.getEntities()){
			CharSequence cs;
			
			cs=jpa.generate(entity);
			TechUtil.saveFile(Paths.baseServerPath + "/"+Paths.fullQualifiedName(entity, Paths.GeneratorType.JPA).replace('.', '/')+".java", cs);
			
			cs=repository.generate(entity);
			TechUtil.saveFile(Paths.baseServerPath + "/"+Paths.fullQualifiedName(entity, Paths.GeneratorType.Repository).replace('.', '/')+".java", cs);
			
		}
		for(WPackage p: pack.getSubpackages()){
			generateFromPackage(p);
		}	
	}
}
