/*
 * generated by Xtext
 */
package wis.xtext;

/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
public class WISRuntimeModule extends wis.xtext.AbstractWISRuntimeModule {

}
