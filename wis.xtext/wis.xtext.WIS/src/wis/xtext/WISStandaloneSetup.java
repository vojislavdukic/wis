/*
 * generated by Xtext
 */
package wis.xtext;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class WISStandaloneSetup extends WISStandaloneSetupGenerated{

	public static void doSetup() {
		new WISStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}

