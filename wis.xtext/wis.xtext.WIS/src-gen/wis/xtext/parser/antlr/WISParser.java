/*
 * generated by Xtext
 */
package wis.xtext.parser.antlr;

import com.google.inject.Inject;

import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import wis.xtext.services.WISGrammarAccess;

public class WISParser extends org.eclipse.xtext.parser.antlr.AbstractAntlrParser {
	
	@Inject
	private WISGrammarAccess grammarAccess;
	
	@Override
	protected void setInitialHiddenTokens(XtextTokenStream tokenStream) {
		tokenStream.setInitialHiddenTokens("RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT");
	}
	
	@Override
	protected wis.xtext.parser.antlr.internal.InternalWISParser createParser(XtextTokenStream stream) {
		return new wis.xtext.parser.antlr.internal.InternalWISParser(stream, getGrammarAccess());
	}
	
	@Override 
	protected String getDefaultRuleName() {
		return "WModel";
	}
	
	public WISGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}
	
	public void setGrammarAccess(WISGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
	
}
