package wis.xtext.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import wis.xtext.services.WISGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalWISParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'model'", "'{'", "'title'", "'version'", "'debug'", "'debug version'", "'}'", "'packages'", "','", "'reports'", "'enumerations'", "'history'", "'package'", "'entities'", "'label'", "'links'", "'generate'", "'subpackages'", "'message'", "'messageDev'", "'path'", "'type'", "'elementType'", "'element'", "'attribute'", "'link'", "'entity'", "'true'", "'false'", "'attributes'", "'from'", "'to'", "'isPublic'", "'placeholder'", "'important'", "'validationRegex'", "'validationMessage'", "'allowNull'", "'length'", "'enumeration'", "'disabled'", "'representation'", "'elements'", "'as'", "'min'", "'max'", "'editable'", "'-'", "'zero'", "'one'", "'many'", "'Combobox'", "'Radio'", "'Delete'", "'Insert'", "'Update'", "'Attribute'", "'Entity'", "'Link'", "'Package'", "'StringSingleline'", "'StringMultiline'", "'Integer'", "'Double'", "'Date'", "'Boolean'", "'Enumeration'", "'Custom'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__59=59;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__55=55;
    public static final int T__12=12;
    public static final int T__56=56;
    public static final int T__13=13;
    public static final int T__57=57;
    public static final int T__14=14;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__67=67;
    public static final int T__24=24;
    public static final int T__68=68;
    public static final int T__25=25;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__20=20;
    public static final int T__64=64;
    public static final int T__21=21;
    public static final int T__65=65;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__77=77;
    public static final int T__34=34;
    public static final int T__78=78;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__73=73;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__74=74;
    public static final int T__31=31;
    public static final int T__75=75;
    public static final int T__32=32;
    public static final int T__76=76;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalWISParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalWISParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalWISParser.tokenNames; }
    public String getGrammarFileName() { return "../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g"; }



     	private WISGrammarAccess grammarAccess;
     	
        public InternalWISParser(TokenStream input, WISGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "WModel";	
       	}
       	
       	@Override
       	protected WISGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleWModel"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:68:1: entryRuleWModel returns [EObject current=null] : iv_ruleWModel= ruleWModel EOF ;
    public final EObject entryRuleWModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWModel = null;


        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:69:2: (iv_ruleWModel= ruleWModel EOF )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:70:2: iv_ruleWModel= ruleWModel EOF
            {
             newCompositeNode(grammarAccess.getWModelRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleWModel_in_entryRuleWModel75);
            iv_ruleWModel=ruleWModel();

            state._fsp--;

             current =iv_ruleWModel; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleWModel85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWModel"


    // $ANTLR start "ruleWModel"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:77:1: ruleWModel returns [EObject current=null] : ( () (otherlv_1= 'model' otherlv_2= '{' (otherlv_3= 'title' ( (lv_name_4_0= ruleEString ) ) ) (otherlv_5= 'version' ( (lv_version_6_0= ruleEIntegerObject ) ) ) (otherlv_7= 'debug' ( (lv_debug_8_0= ruleEBooleanObject ) ) )? (otherlv_9= 'debug version' ( (lv_debugVersion_10_0= ruleEIntegerObject ) ) )? otherlv_11= '}' ) (otherlv_12= 'packages' otherlv_13= '{' ( (lv_packages_14_0= ruleWPackage ) ) (otherlv_15= ',' ( (lv_packages_16_0= ruleWPackage ) ) )* otherlv_17= '}' )? (otherlv_18= 'reports' ( (lv_reportsContainer_19_0= ruleWReports ) ) )? (otherlv_20= 'enumerations' otherlv_21= '{' ( (lv_enumerations_22_0= ruleWEnumerations ) ) otherlv_23= '}' )? (otherlv_24= 'history' otherlv_25= '{' ( (lv_changesContainer_26_0= ruleWChanges ) ) otherlv_27= '}' )? ) ;
    public final EObject ruleWModel() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token otherlv_23=null;
        Token otherlv_24=null;
        Token otherlv_25=null;
        Token otherlv_27=null;
        AntlrDatatypeRuleToken lv_name_4_0 = null;

        AntlrDatatypeRuleToken lv_version_6_0 = null;

        AntlrDatatypeRuleToken lv_debug_8_0 = null;

        AntlrDatatypeRuleToken lv_debugVersion_10_0 = null;

        EObject lv_packages_14_0 = null;

        EObject lv_packages_16_0 = null;

        EObject lv_reportsContainer_19_0 = null;

        EObject lv_enumerations_22_0 = null;

        EObject lv_changesContainer_26_0 = null;


         enterRule(); 
            
        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:80:28: ( ( () (otherlv_1= 'model' otherlv_2= '{' (otherlv_3= 'title' ( (lv_name_4_0= ruleEString ) ) ) (otherlv_5= 'version' ( (lv_version_6_0= ruleEIntegerObject ) ) ) (otherlv_7= 'debug' ( (lv_debug_8_0= ruleEBooleanObject ) ) )? (otherlv_9= 'debug version' ( (lv_debugVersion_10_0= ruleEIntegerObject ) ) )? otherlv_11= '}' ) (otherlv_12= 'packages' otherlv_13= '{' ( (lv_packages_14_0= ruleWPackage ) ) (otherlv_15= ',' ( (lv_packages_16_0= ruleWPackage ) ) )* otherlv_17= '}' )? (otherlv_18= 'reports' ( (lv_reportsContainer_19_0= ruleWReports ) ) )? (otherlv_20= 'enumerations' otherlv_21= '{' ( (lv_enumerations_22_0= ruleWEnumerations ) ) otherlv_23= '}' )? (otherlv_24= 'history' otherlv_25= '{' ( (lv_changesContainer_26_0= ruleWChanges ) ) otherlv_27= '}' )? ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:81:1: ( () (otherlv_1= 'model' otherlv_2= '{' (otherlv_3= 'title' ( (lv_name_4_0= ruleEString ) ) ) (otherlv_5= 'version' ( (lv_version_6_0= ruleEIntegerObject ) ) ) (otherlv_7= 'debug' ( (lv_debug_8_0= ruleEBooleanObject ) ) )? (otherlv_9= 'debug version' ( (lv_debugVersion_10_0= ruleEIntegerObject ) ) )? otherlv_11= '}' ) (otherlv_12= 'packages' otherlv_13= '{' ( (lv_packages_14_0= ruleWPackage ) ) (otherlv_15= ',' ( (lv_packages_16_0= ruleWPackage ) ) )* otherlv_17= '}' )? (otherlv_18= 'reports' ( (lv_reportsContainer_19_0= ruleWReports ) ) )? (otherlv_20= 'enumerations' otherlv_21= '{' ( (lv_enumerations_22_0= ruleWEnumerations ) ) otherlv_23= '}' )? (otherlv_24= 'history' otherlv_25= '{' ( (lv_changesContainer_26_0= ruleWChanges ) ) otherlv_27= '}' )? )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:81:1: ( () (otherlv_1= 'model' otherlv_2= '{' (otherlv_3= 'title' ( (lv_name_4_0= ruleEString ) ) ) (otherlv_5= 'version' ( (lv_version_6_0= ruleEIntegerObject ) ) ) (otherlv_7= 'debug' ( (lv_debug_8_0= ruleEBooleanObject ) ) )? (otherlv_9= 'debug version' ( (lv_debugVersion_10_0= ruleEIntegerObject ) ) )? otherlv_11= '}' ) (otherlv_12= 'packages' otherlv_13= '{' ( (lv_packages_14_0= ruleWPackage ) ) (otherlv_15= ',' ( (lv_packages_16_0= ruleWPackage ) ) )* otherlv_17= '}' )? (otherlv_18= 'reports' ( (lv_reportsContainer_19_0= ruleWReports ) ) )? (otherlv_20= 'enumerations' otherlv_21= '{' ( (lv_enumerations_22_0= ruleWEnumerations ) ) otherlv_23= '}' )? (otherlv_24= 'history' otherlv_25= '{' ( (lv_changesContainer_26_0= ruleWChanges ) ) otherlv_27= '}' )? )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:81:2: () (otherlv_1= 'model' otherlv_2= '{' (otherlv_3= 'title' ( (lv_name_4_0= ruleEString ) ) ) (otherlv_5= 'version' ( (lv_version_6_0= ruleEIntegerObject ) ) ) (otherlv_7= 'debug' ( (lv_debug_8_0= ruleEBooleanObject ) ) )? (otherlv_9= 'debug version' ( (lv_debugVersion_10_0= ruleEIntegerObject ) ) )? otherlv_11= '}' ) (otherlv_12= 'packages' otherlv_13= '{' ( (lv_packages_14_0= ruleWPackage ) ) (otherlv_15= ',' ( (lv_packages_16_0= ruleWPackage ) ) )* otherlv_17= '}' )? (otherlv_18= 'reports' ( (lv_reportsContainer_19_0= ruleWReports ) ) )? (otherlv_20= 'enumerations' otherlv_21= '{' ( (lv_enumerations_22_0= ruleWEnumerations ) ) otherlv_23= '}' )? (otherlv_24= 'history' otherlv_25= '{' ( (lv_changesContainer_26_0= ruleWChanges ) ) otherlv_27= '}' )?
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:81:2: ()
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:82:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getWModelAccess().getWModelAction_0(),
                        current);
                

            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:87:2: (otherlv_1= 'model' otherlv_2= '{' (otherlv_3= 'title' ( (lv_name_4_0= ruleEString ) ) ) (otherlv_5= 'version' ( (lv_version_6_0= ruleEIntegerObject ) ) ) (otherlv_7= 'debug' ( (lv_debug_8_0= ruleEBooleanObject ) ) )? (otherlv_9= 'debug version' ( (lv_debugVersion_10_0= ruleEIntegerObject ) ) )? otherlv_11= '}' )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:87:4: otherlv_1= 'model' otherlv_2= '{' (otherlv_3= 'title' ( (lv_name_4_0= ruleEString ) ) ) (otherlv_5= 'version' ( (lv_version_6_0= ruleEIntegerObject ) ) ) (otherlv_7= 'debug' ( (lv_debug_8_0= ruleEBooleanObject ) ) )? (otherlv_9= 'debug version' ( (lv_debugVersion_10_0= ruleEIntegerObject ) ) )? otherlv_11= '}'
            {
            otherlv_1=(Token)match(input,11,FollowSets000.FOLLOW_11_in_ruleWModel132); 

                	newLeafNode(otherlv_1, grammarAccess.getWModelAccess().getModelKeyword_1_0());
                
            otherlv_2=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleWModel144); 

                	newLeafNode(otherlv_2, grammarAccess.getWModelAccess().getLeftCurlyBracketKeyword_1_1());
                
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:95:1: (otherlv_3= 'title' ( (lv_name_4_0= ruleEString ) ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:95:3: otherlv_3= 'title' ( (lv_name_4_0= ruleEString ) )
            {
            otherlv_3=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleWModel157); 

                	newLeafNode(otherlv_3, grammarAccess.getWModelAccess().getTitleKeyword_1_2_0());
                
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:99:1: ( (lv_name_4_0= ruleEString ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:100:1: (lv_name_4_0= ruleEString )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:100:1: (lv_name_4_0= ruleEString )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:101:3: lv_name_4_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getWModelAccess().getNameEStringParserRuleCall_1_2_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWModel178);
            lv_name_4_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getWModelRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_4_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:117:3: (otherlv_5= 'version' ( (lv_version_6_0= ruleEIntegerObject ) ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:117:5: otherlv_5= 'version' ( (lv_version_6_0= ruleEIntegerObject ) )
            {
            otherlv_5=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleWModel192); 

                	newLeafNode(otherlv_5, grammarAccess.getWModelAccess().getVersionKeyword_1_3_0());
                
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:121:1: ( (lv_version_6_0= ruleEIntegerObject ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:122:1: (lv_version_6_0= ruleEIntegerObject )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:122:1: (lv_version_6_0= ruleEIntegerObject )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:123:3: lv_version_6_0= ruleEIntegerObject
            {
             
            	        newCompositeNode(grammarAccess.getWModelAccess().getVersionEIntegerObjectParserRuleCall_1_3_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEIntegerObject_in_ruleWModel213);
            lv_version_6_0=ruleEIntegerObject();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getWModelRule());
            	        }
                   		set(
                   			current, 
                   			"version",
                    		lv_version_6_0, 
                    		"EIntegerObject");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:139:3: (otherlv_7= 'debug' ( (lv_debug_8_0= ruleEBooleanObject ) ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==15) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:139:5: otherlv_7= 'debug' ( (lv_debug_8_0= ruleEBooleanObject ) )
                    {
                    otherlv_7=(Token)match(input,15,FollowSets000.FOLLOW_15_in_ruleWModel227); 

                        	newLeafNode(otherlv_7, grammarAccess.getWModelAccess().getDebugKeyword_1_4_0());
                        
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:143:1: ( (lv_debug_8_0= ruleEBooleanObject ) )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:144:1: (lv_debug_8_0= ruleEBooleanObject )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:144:1: (lv_debug_8_0= ruleEBooleanObject )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:145:3: lv_debug_8_0= ruleEBooleanObject
                    {
                     
                    	        newCompositeNode(grammarAccess.getWModelAccess().getDebugEBooleanObjectParserRuleCall_1_4_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEBooleanObject_in_ruleWModel248);
                    lv_debug_8_0=ruleEBooleanObject();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getWModelRule());
                    	        }
                           		set(
                           			current, 
                           			"debug",
                            		lv_debug_8_0, 
                            		"EBooleanObject");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:161:4: (otherlv_9= 'debug version' ( (lv_debugVersion_10_0= ruleEIntegerObject ) ) )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==16) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:161:6: otherlv_9= 'debug version' ( (lv_debugVersion_10_0= ruleEIntegerObject ) )
                    {
                    otherlv_9=(Token)match(input,16,FollowSets000.FOLLOW_16_in_ruleWModel263); 

                        	newLeafNode(otherlv_9, grammarAccess.getWModelAccess().getDebugVersionKeyword_1_5_0());
                        
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:165:1: ( (lv_debugVersion_10_0= ruleEIntegerObject ) )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:166:1: (lv_debugVersion_10_0= ruleEIntegerObject )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:166:1: (lv_debugVersion_10_0= ruleEIntegerObject )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:167:3: lv_debugVersion_10_0= ruleEIntegerObject
                    {
                     
                    	        newCompositeNode(grammarAccess.getWModelAccess().getDebugVersionEIntegerObjectParserRuleCall_1_5_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEIntegerObject_in_ruleWModel284);
                    lv_debugVersion_10_0=ruleEIntegerObject();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getWModelRule());
                    	        }
                           		set(
                           			current, 
                           			"debugVersion",
                            		lv_debugVersion_10_0, 
                            		"EIntegerObject");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            otherlv_11=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleWModel298); 

                	newLeafNode(otherlv_11, grammarAccess.getWModelAccess().getRightCurlyBracketKeyword_1_6());
                

            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:187:2: (otherlv_12= 'packages' otherlv_13= '{' ( (lv_packages_14_0= ruleWPackage ) ) (otherlv_15= ',' ( (lv_packages_16_0= ruleWPackage ) ) )* otherlv_17= '}' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==18) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:187:4: otherlv_12= 'packages' otherlv_13= '{' ( (lv_packages_14_0= ruleWPackage ) ) (otherlv_15= ',' ( (lv_packages_16_0= ruleWPackage ) ) )* otherlv_17= '}'
                    {
                    otherlv_12=(Token)match(input,18,FollowSets000.FOLLOW_18_in_ruleWModel312); 

                        	newLeafNode(otherlv_12, grammarAccess.getWModelAccess().getPackagesKeyword_2_0());
                        
                    otherlv_13=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleWModel324); 

                        	newLeafNode(otherlv_13, grammarAccess.getWModelAccess().getLeftCurlyBracketKeyword_2_1());
                        
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:195:1: ( (lv_packages_14_0= ruleWPackage ) )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:196:1: (lv_packages_14_0= ruleWPackage )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:196:1: (lv_packages_14_0= ruleWPackage )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:197:3: lv_packages_14_0= ruleWPackage
                    {
                     
                    	        newCompositeNode(grammarAccess.getWModelAccess().getPackagesWPackageParserRuleCall_2_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleWPackage_in_ruleWModel345);
                    lv_packages_14_0=ruleWPackage();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getWModelRule());
                    	        }
                           		add(
                           			current, 
                           			"packages",
                            		lv_packages_14_0, 
                            		"WPackage");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:213:2: (otherlv_15= ',' ( (lv_packages_16_0= ruleWPackage ) ) )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==19) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:213:4: otherlv_15= ',' ( (lv_packages_16_0= ruleWPackage ) )
                    	    {
                    	    otherlv_15=(Token)match(input,19,FollowSets000.FOLLOW_19_in_ruleWModel358); 

                    	        	newLeafNode(otherlv_15, grammarAccess.getWModelAccess().getCommaKeyword_2_3_0());
                    	        
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:217:1: ( (lv_packages_16_0= ruleWPackage ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:218:1: (lv_packages_16_0= ruleWPackage )
                    	    {
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:218:1: (lv_packages_16_0= ruleWPackage )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:219:3: lv_packages_16_0= ruleWPackage
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getWModelAccess().getPackagesWPackageParserRuleCall_2_3_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleWPackage_in_ruleWModel379);
                    	    lv_packages_16_0=ruleWPackage();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getWModelRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"packages",
                    	            		lv_packages_16_0, 
                    	            		"WPackage");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);

                    otherlv_17=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleWModel393); 

                        	newLeafNode(otherlv_17, grammarAccess.getWModelAccess().getRightCurlyBracketKeyword_2_4());
                        

                    }
                    break;

            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:239:3: (otherlv_18= 'reports' ( (lv_reportsContainer_19_0= ruleWReports ) ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==20) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:239:5: otherlv_18= 'reports' ( (lv_reportsContainer_19_0= ruleWReports ) )
                    {
                    otherlv_18=(Token)match(input,20,FollowSets000.FOLLOW_20_in_ruleWModel408); 

                        	newLeafNode(otherlv_18, grammarAccess.getWModelAccess().getReportsKeyword_3_0());
                        
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:243:1: ( (lv_reportsContainer_19_0= ruleWReports ) )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:244:1: (lv_reportsContainer_19_0= ruleWReports )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:244:1: (lv_reportsContainer_19_0= ruleWReports )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:245:3: lv_reportsContainer_19_0= ruleWReports
                    {
                     
                    	        newCompositeNode(grammarAccess.getWModelAccess().getReportsContainerWReportsParserRuleCall_3_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleWReports_in_ruleWModel429);
                    lv_reportsContainer_19_0=ruleWReports();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getWModelRule());
                    	        }
                           		set(
                           			current, 
                           			"reportsContainer",
                            		lv_reportsContainer_19_0, 
                            		"WReports");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:261:4: (otherlv_20= 'enumerations' otherlv_21= '{' ( (lv_enumerations_22_0= ruleWEnumerations ) ) otherlv_23= '}' )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==21) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:261:6: otherlv_20= 'enumerations' otherlv_21= '{' ( (lv_enumerations_22_0= ruleWEnumerations ) ) otherlv_23= '}'
                    {
                    otherlv_20=(Token)match(input,21,FollowSets000.FOLLOW_21_in_ruleWModel444); 

                        	newLeafNode(otherlv_20, grammarAccess.getWModelAccess().getEnumerationsKeyword_4_0());
                        
                    otherlv_21=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleWModel456); 

                        	newLeafNode(otherlv_21, grammarAccess.getWModelAccess().getLeftCurlyBracketKeyword_4_1());
                        
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:269:1: ( (lv_enumerations_22_0= ruleWEnumerations ) )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:270:1: (lv_enumerations_22_0= ruleWEnumerations )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:270:1: (lv_enumerations_22_0= ruleWEnumerations )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:271:3: lv_enumerations_22_0= ruleWEnumerations
                    {
                     
                    	        newCompositeNode(grammarAccess.getWModelAccess().getEnumerationsWEnumerationsParserRuleCall_4_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleWEnumerations_in_ruleWModel477);
                    lv_enumerations_22_0=ruleWEnumerations();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getWModelRule());
                    	        }
                           		set(
                           			current, 
                           			"enumerations",
                            		lv_enumerations_22_0, 
                            		"WEnumerations");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_23=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleWModel489); 

                        	newLeafNode(otherlv_23, grammarAccess.getWModelAccess().getRightCurlyBracketKeyword_4_3());
                        

                    }
                    break;

            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:291:3: (otherlv_24= 'history' otherlv_25= '{' ( (lv_changesContainer_26_0= ruleWChanges ) ) otherlv_27= '}' )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==22) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:291:5: otherlv_24= 'history' otherlv_25= '{' ( (lv_changesContainer_26_0= ruleWChanges ) ) otherlv_27= '}'
                    {
                    otherlv_24=(Token)match(input,22,FollowSets000.FOLLOW_22_in_ruleWModel504); 

                        	newLeafNode(otherlv_24, grammarAccess.getWModelAccess().getHistoryKeyword_5_0());
                        
                    otherlv_25=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleWModel516); 

                        	newLeafNode(otherlv_25, grammarAccess.getWModelAccess().getLeftCurlyBracketKeyword_5_1());
                        
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:299:1: ( (lv_changesContainer_26_0= ruleWChanges ) )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:300:1: (lv_changesContainer_26_0= ruleWChanges )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:300:1: (lv_changesContainer_26_0= ruleWChanges )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:301:3: lv_changesContainer_26_0= ruleWChanges
                    {
                     
                    	        newCompositeNode(grammarAccess.getWModelAccess().getChangesContainerWChangesParserRuleCall_5_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleWChanges_in_ruleWModel537);
                    lv_changesContainer_26_0=ruleWChanges();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getWModelRule());
                    	        }
                           		set(
                           			current, 
                           			"changesContainer",
                            		lv_changesContainer_26_0, 
                            		"WChanges");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_27=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleWModel549); 

                        	newLeafNode(otherlv_27, grammarAccess.getWModelAccess().getRightCurlyBracketKeyword_5_3());
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWModel"


    // $ANTLR start "entryRuleWPackage"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:329:1: entryRuleWPackage returns [EObject current=null] : iv_ruleWPackage= ruleWPackage EOF ;
    public final EObject entryRuleWPackage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWPackage = null;


        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:330:2: (iv_ruleWPackage= ruleWPackage EOF )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:331:2: iv_ruleWPackage= ruleWPackage EOF
            {
             newCompositeNode(grammarAccess.getWPackageRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleWPackage_in_entryRuleWPackage587);
            iv_ruleWPackage=ruleWPackage();

            state._fsp--;

             current =iv_ruleWPackage; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleWPackage597); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWPackage"


    // $ANTLR start "ruleWPackage"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:338:1: ruleWPackage returns [EObject current=null] : ( () otherlv_1= 'package' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? (otherlv_6= 'entities' otherlv_7= '{' ( (lv_entities_8_0= ruleWEntity ) ) (otherlv_9= ',' ( (lv_entities_10_0= ruleWEntity ) ) )* otherlv_11= '}' ) (otherlv_12= 'label' ( (lv_label_13_0= ruleEString ) ) )? (otherlv_14= 'links' otherlv_15= '{' ( (lv_links_16_0= ruleWLink ) ) (otherlv_17= ',' ( (lv_links_18_0= ruleWLink ) ) )* otherlv_19= '}' )? (otherlv_20= 'generate' ( (lv_generate_21_0= ruleEBooleanObject ) ) )? (otherlv_22= 'subpackages' otherlv_23= '{' ( (lv_subpackages_24_0= ruleWPackage ) ) (otherlv_25= ',' ( (lv_subpackages_26_0= ruleWPackage ) ) )* otherlv_27= '}' )? otherlv_28= '}' )? ) ;
    public final EObject ruleWPackage() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token otherlv_22=null;
        Token otherlv_23=null;
        Token otherlv_25=null;
        Token otherlv_27=null;
        Token otherlv_28=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_version_5_0 = null;

        EObject lv_entities_8_0 = null;

        EObject lv_entities_10_0 = null;

        AntlrDatatypeRuleToken lv_label_13_0 = null;

        EObject lv_links_16_0 = null;

        EObject lv_links_18_0 = null;

        AntlrDatatypeRuleToken lv_generate_21_0 = null;

        EObject lv_subpackages_24_0 = null;

        EObject lv_subpackages_26_0 = null;


         enterRule(); 
            
        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:341:28: ( ( () otherlv_1= 'package' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? (otherlv_6= 'entities' otherlv_7= '{' ( (lv_entities_8_0= ruleWEntity ) ) (otherlv_9= ',' ( (lv_entities_10_0= ruleWEntity ) ) )* otherlv_11= '}' ) (otherlv_12= 'label' ( (lv_label_13_0= ruleEString ) ) )? (otherlv_14= 'links' otherlv_15= '{' ( (lv_links_16_0= ruleWLink ) ) (otherlv_17= ',' ( (lv_links_18_0= ruleWLink ) ) )* otherlv_19= '}' )? (otherlv_20= 'generate' ( (lv_generate_21_0= ruleEBooleanObject ) ) )? (otherlv_22= 'subpackages' otherlv_23= '{' ( (lv_subpackages_24_0= ruleWPackage ) ) (otherlv_25= ',' ( (lv_subpackages_26_0= ruleWPackage ) ) )* otherlv_27= '}' )? otherlv_28= '}' )? ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:342:1: ( () otherlv_1= 'package' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? (otherlv_6= 'entities' otherlv_7= '{' ( (lv_entities_8_0= ruleWEntity ) ) (otherlv_9= ',' ( (lv_entities_10_0= ruleWEntity ) ) )* otherlv_11= '}' ) (otherlv_12= 'label' ( (lv_label_13_0= ruleEString ) ) )? (otherlv_14= 'links' otherlv_15= '{' ( (lv_links_16_0= ruleWLink ) ) (otherlv_17= ',' ( (lv_links_18_0= ruleWLink ) ) )* otherlv_19= '}' )? (otherlv_20= 'generate' ( (lv_generate_21_0= ruleEBooleanObject ) ) )? (otherlv_22= 'subpackages' otherlv_23= '{' ( (lv_subpackages_24_0= ruleWPackage ) ) (otherlv_25= ',' ( (lv_subpackages_26_0= ruleWPackage ) ) )* otherlv_27= '}' )? otherlv_28= '}' )? )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:342:1: ( () otherlv_1= 'package' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? (otherlv_6= 'entities' otherlv_7= '{' ( (lv_entities_8_0= ruleWEntity ) ) (otherlv_9= ',' ( (lv_entities_10_0= ruleWEntity ) ) )* otherlv_11= '}' ) (otherlv_12= 'label' ( (lv_label_13_0= ruleEString ) ) )? (otherlv_14= 'links' otherlv_15= '{' ( (lv_links_16_0= ruleWLink ) ) (otherlv_17= ',' ( (lv_links_18_0= ruleWLink ) ) )* otherlv_19= '}' )? (otherlv_20= 'generate' ( (lv_generate_21_0= ruleEBooleanObject ) ) )? (otherlv_22= 'subpackages' otherlv_23= '{' ( (lv_subpackages_24_0= ruleWPackage ) ) (otherlv_25= ',' ( (lv_subpackages_26_0= ruleWPackage ) ) )* otherlv_27= '}' )? otherlv_28= '}' )? )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:342:2: () otherlv_1= 'package' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? (otherlv_6= 'entities' otherlv_7= '{' ( (lv_entities_8_0= ruleWEntity ) ) (otherlv_9= ',' ( (lv_entities_10_0= ruleWEntity ) ) )* otherlv_11= '}' ) (otherlv_12= 'label' ( (lv_label_13_0= ruleEString ) ) )? (otherlv_14= 'links' otherlv_15= '{' ( (lv_links_16_0= ruleWLink ) ) (otherlv_17= ',' ( (lv_links_18_0= ruleWLink ) ) )* otherlv_19= '}' )? (otherlv_20= 'generate' ( (lv_generate_21_0= ruleEBooleanObject ) ) )? (otherlv_22= 'subpackages' otherlv_23= '{' ( (lv_subpackages_24_0= ruleWPackage ) ) (otherlv_25= ',' ( (lv_subpackages_26_0= ruleWPackage ) ) )* otherlv_27= '}' )? otherlv_28= '}' )?
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:342:2: ()
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:343:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getWPackageAccess().getWPackageAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,23,FollowSets000.FOLLOW_23_in_ruleWPackage643); 

                	newLeafNode(otherlv_1, grammarAccess.getWPackageAccess().getPackageKeyword_1());
                
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:352:1: ( (lv_name_2_0= ruleEString ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:353:1: (lv_name_2_0= ruleEString )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:353:1: (lv_name_2_0= ruleEString )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:354:3: lv_name_2_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getWPackageAccess().getNameEStringParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWPackage664);
            lv_name_2_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getWPackageRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_2_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:370:2: (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? (otherlv_6= 'entities' otherlv_7= '{' ( (lv_entities_8_0= ruleWEntity ) ) (otherlv_9= ',' ( (lv_entities_10_0= ruleWEntity ) ) )* otherlv_11= '}' ) (otherlv_12= 'label' ( (lv_label_13_0= ruleEString ) ) )? (otherlv_14= 'links' otherlv_15= '{' ( (lv_links_16_0= ruleWLink ) ) (otherlv_17= ',' ( (lv_links_18_0= ruleWLink ) ) )* otherlv_19= '}' )? (otherlv_20= 'generate' ( (lv_generate_21_0= ruleEBooleanObject ) ) )? (otherlv_22= 'subpackages' otherlv_23= '{' ( (lv_subpackages_24_0= ruleWPackage ) ) (otherlv_25= ',' ( (lv_subpackages_26_0= ruleWPackage ) ) )* otherlv_27= '}' )? otherlv_28= '}' )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==12) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:370:4: otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? (otherlv_6= 'entities' otherlv_7= '{' ( (lv_entities_8_0= ruleWEntity ) ) (otherlv_9= ',' ( (lv_entities_10_0= ruleWEntity ) ) )* otherlv_11= '}' ) (otherlv_12= 'label' ( (lv_label_13_0= ruleEString ) ) )? (otherlv_14= 'links' otherlv_15= '{' ( (lv_links_16_0= ruleWLink ) ) (otherlv_17= ',' ( (lv_links_18_0= ruleWLink ) ) )* otherlv_19= '}' )? (otherlv_20= 'generate' ( (lv_generate_21_0= ruleEBooleanObject ) ) )? (otherlv_22= 'subpackages' otherlv_23= '{' ( (lv_subpackages_24_0= ruleWPackage ) ) (otherlv_25= ',' ( (lv_subpackages_26_0= ruleWPackage ) ) )* otherlv_27= '}' )? otherlv_28= '}'
                    {
                    otherlv_3=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleWPackage677); 

                        	newLeafNode(otherlv_3, grammarAccess.getWPackageAccess().getLeftCurlyBracketKeyword_3_0());
                        
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:374:1: (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )?
                    int alt8=2;
                    int LA8_0 = input.LA(1);

                    if ( (LA8_0==14) ) {
                        alt8=1;
                    }
                    switch (alt8) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:374:3: otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) )
                            {
                            otherlv_4=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleWPackage690); 

                                	newLeafNode(otherlv_4, grammarAccess.getWPackageAccess().getVersionKeyword_3_1_0());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:378:1: ( (lv_version_5_0= ruleEIntegerObject ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:379:1: (lv_version_5_0= ruleEIntegerObject )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:379:1: (lv_version_5_0= ruleEIntegerObject )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:380:3: lv_version_5_0= ruleEIntegerObject
                            {
                             
                            	        newCompositeNode(grammarAccess.getWPackageAccess().getVersionEIntegerObjectParserRuleCall_3_1_1_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleEIntegerObject_in_ruleWPackage711);
                            lv_version_5_0=ruleEIntegerObject();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWPackageRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"version",
                                    		lv_version_5_0, 
                                    		"EIntegerObject");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:396:4: (otherlv_6= 'entities' otherlv_7= '{' ( (lv_entities_8_0= ruleWEntity ) ) (otherlv_9= ',' ( (lv_entities_10_0= ruleWEntity ) ) )* otherlv_11= '}' )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:396:6: otherlv_6= 'entities' otherlv_7= '{' ( (lv_entities_8_0= ruleWEntity ) ) (otherlv_9= ',' ( (lv_entities_10_0= ruleWEntity ) ) )* otherlv_11= '}'
                    {
                    otherlv_6=(Token)match(input,24,FollowSets000.FOLLOW_24_in_ruleWPackage726); 

                        	newLeafNode(otherlv_6, grammarAccess.getWPackageAccess().getEntitiesKeyword_3_2_0());
                        
                    otherlv_7=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleWPackage738); 

                        	newLeafNode(otherlv_7, grammarAccess.getWPackageAccess().getLeftCurlyBracketKeyword_3_2_1());
                        
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:404:1: ( (lv_entities_8_0= ruleWEntity ) )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:405:1: (lv_entities_8_0= ruleWEntity )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:405:1: (lv_entities_8_0= ruleWEntity )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:406:3: lv_entities_8_0= ruleWEntity
                    {
                     
                    	        newCompositeNode(grammarAccess.getWPackageAccess().getEntitiesWEntityParserRuleCall_3_2_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleWEntity_in_ruleWPackage759);
                    lv_entities_8_0=ruleWEntity();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getWPackageRule());
                    	        }
                           		add(
                           			current, 
                           			"entities",
                            		lv_entities_8_0, 
                            		"WEntity");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:422:2: (otherlv_9= ',' ( (lv_entities_10_0= ruleWEntity ) ) )*
                    loop9:
                    do {
                        int alt9=2;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0==19) ) {
                            alt9=1;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:422:4: otherlv_9= ',' ( (lv_entities_10_0= ruleWEntity ) )
                    	    {
                    	    otherlv_9=(Token)match(input,19,FollowSets000.FOLLOW_19_in_ruleWPackage772); 

                    	        	newLeafNode(otherlv_9, grammarAccess.getWPackageAccess().getCommaKeyword_3_2_3_0());
                    	        
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:426:1: ( (lv_entities_10_0= ruleWEntity ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:427:1: (lv_entities_10_0= ruleWEntity )
                    	    {
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:427:1: (lv_entities_10_0= ruleWEntity )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:428:3: lv_entities_10_0= ruleWEntity
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getWPackageAccess().getEntitiesWEntityParserRuleCall_3_2_3_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleWEntity_in_ruleWPackage793);
                    	    lv_entities_10_0=ruleWEntity();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getWPackageRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"entities",
                    	            		lv_entities_10_0, 
                    	            		"WEntity");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);

                    otherlv_11=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleWPackage807); 

                        	newLeafNode(otherlv_11, grammarAccess.getWPackageAccess().getRightCurlyBracketKeyword_3_2_4());
                        

                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:448:2: (otherlv_12= 'label' ( (lv_label_13_0= ruleEString ) ) )?
                    int alt10=2;
                    int LA10_0 = input.LA(1);

                    if ( (LA10_0==25) ) {
                        alt10=1;
                    }
                    switch (alt10) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:448:4: otherlv_12= 'label' ( (lv_label_13_0= ruleEString ) )
                            {
                            otherlv_12=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleWPackage821); 

                                	newLeafNode(otherlv_12, grammarAccess.getWPackageAccess().getLabelKeyword_3_3_0());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:452:1: ( (lv_label_13_0= ruleEString ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:453:1: (lv_label_13_0= ruleEString )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:453:1: (lv_label_13_0= ruleEString )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:454:3: lv_label_13_0= ruleEString
                            {
                             
                            	        newCompositeNode(grammarAccess.getWPackageAccess().getLabelEStringParserRuleCall_3_3_1_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWPackage842);
                            lv_label_13_0=ruleEString();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWPackageRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"label",
                                    		lv_label_13_0, 
                                    		"EString");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:470:4: (otherlv_14= 'links' otherlv_15= '{' ( (lv_links_16_0= ruleWLink ) ) (otherlv_17= ',' ( (lv_links_18_0= ruleWLink ) ) )* otherlv_19= '}' )?
                    int alt12=2;
                    int LA12_0 = input.LA(1);

                    if ( (LA12_0==26) ) {
                        alt12=1;
                    }
                    switch (alt12) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:470:6: otherlv_14= 'links' otherlv_15= '{' ( (lv_links_16_0= ruleWLink ) ) (otherlv_17= ',' ( (lv_links_18_0= ruleWLink ) ) )* otherlv_19= '}'
                            {
                            otherlv_14=(Token)match(input,26,FollowSets000.FOLLOW_26_in_ruleWPackage857); 

                                	newLeafNode(otherlv_14, grammarAccess.getWPackageAccess().getLinksKeyword_3_4_0());
                                
                            otherlv_15=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleWPackage869); 

                                	newLeafNode(otherlv_15, grammarAccess.getWPackageAccess().getLeftCurlyBracketKeyword_3_4_1());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:478:1: ( (lv_links_16_0= ruleWLink ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:479:1: (lv_links_16_0= ruleWLink )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:479:1: (lv_links_16_0= ruleWLink )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:480:3: lv_links_16_0= ruleWLink
                            {
                             
                            	        newCompositeNode(grammarAccess.getWPackageAccess().getLinksWLinkParserRuleCall_3_4_2_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleWLink_in_ruleWPackage890);
                            lv_links_16_0=ruleWLink();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWPackageRule());
                            	        }
                                   		add(
                                   			current, 
                                   			"links",
                                    		lv_links_16_0, 
                                    		"WLink");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }

                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:496:2: (otherlv_17= ',' ( (lv_links_18_0= ruleWLink ) ) )*
                            loop11:
                            do {
                                int alt11=2;
                                int LA11_0 = input.LA(1);

                                if ( (LA11_0==19) ) {
                                    alt11=1;
                                }


                                switch (alt11) {
                            	case 1 :
                            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:496:4: otherlv_17= ',' ( (lv_links_18_0= ruleWLink ) )
                            	    {
                            	    otherlv_17=(Token)match(input,19,FollowSets000.FOLLOW_19_in_ruleWPackage903); 

                            	        	newLeafNode(otherlv_17, grammarAccess.getWPackageAccess().getCommaKeyword_3_4_3_0());
                            	        
                            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:500:1: ( (lv_links_18_0= ruleWLink ) )
                            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:501:1: (lv_links_18_0= ruleWLink )
                            	    {
                            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:501:1: (lv_links_18_0= ruleWLink )
                            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:502:3: lv_links_18_0= ruleWLink
                            	    {
                            	     
                            	    	        newCompositeNode(grammarAccess.getWPackageAccess().getLinksWLinkParserRuleCall_3_4_3_1_0()); 
                            	    	    
                            	    pushFollow(FollowSets000.FOLLOW_ruleWLink_in_ruleWPackage924);
                            	    lv_links_18_0=ruleWLink();

                            	    state._fsp--;


                            	    	        if (current==null) {
                            	    	            current = createModelElementForParent(grammarAccess.getWPackageRule());
                            	    	        }
                            	           		add(
                            	           			current, 
                            	           			"links",
                            	            		lv_links_18_0, 
                            	            		"WLink");
                            	    	        afterParserOrEnumRuleCall();
                            	    	    

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop11;
                                }
                            } while (true);

                            otherlv_19=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleWPackage938); 

                                	newLeafNode(otherlv_19, grammarAccess.getWPackageAccess().getRightCurlyBracketKeyword_3_4_4());
                                

                            }
                            break;

                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:522:3: (otherlv_20= 'generate' ( (lv_generate_21_0= ruleEBooleanObject ) ) )?
                    int alt13=2;
                    int LA13_0 = input.LA(1);

                    if ( (LA13_0==27) ) {
                        alt13=1;
                    }
                    switch (alt13) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:522:5: otherlv_20= 'generate' ( (lv_generate_21_0= ruleEBooleanObject ) )
                            {
                            otherlv_20=(Token)match(input,27,FollowSets000.FOLLOW_27_in_ruleWPackage953); 

                                	newLeafNode(otherlv_20, grammarAccess.getWPackageAccess().getGenerateKeyword_3_5_0());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:526:1: ( (lv_generate_21_0= ruleEBooleanObject ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:527:1: (lv_generate_21_0= ruleEBooleanObject )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:527:1: (lv_generate_21_0= ruleEBooleanObject )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:528:3: lv_generate_21_0= ruleEBooleanObject
                            {
                             
                            	        newCompositeNode(grammarAccess.getWPackageAccess().getGenerateEBooleanObjectParserRuleCall_3_5_1_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleEBooleanObject_in_ruleWPackage974);
                            lv_generate_21_0=ruleEBooleanObject();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWPackageRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"generate",
                                    		lv_generate_21_0, 
                                    		"EBooleanObject");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:544:4: (otherlv_22= 'subpackages' otherlv_23= '{' ( (lv_subpackages_24_0= ruleWPackage ) ) (otherlv_25= ',' ( (lv_subpackages_26_0= ruleWPackage ) ) )* otherlv_27= '}' )?
                    int alt15=2;
                    int LA15_0 = input.LA(1);

                    if ( (LA15_0==28) ) {
                        alt15=1;
                    }
                    switch (alt15) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:544:6: otherlv_22= 'subpackages' otherlv_23= '{' ( (lv_subpackages_24_0= ruleWPackage ) ) (otherlv_25= ',' ( (lv_subpackages_26_0= ruleWPackage ) ) )* otherlv_27= '}'
                            {
                            otherlv_22=(Token)match(input,28,FollowSets000.FOLLOW_28_in_ruleWPackage989); 

                                	newLeafNode(otherlv_22, grammarAccess.getWPackageAccess().getSubpackagesKeyword_3_6_0());
                                
                            otherlv_23=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleWPackage1001); 

                                	newLeafNode(otherlv_23, grammarAccess.getWPackageAccess().getLeftCurlyBracketKeyword_3_6_1());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:552:1: ( (lv_subpackages_24_0= ruleWPackage ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:553:1: (lv_subpackages_24_0= ruleWPackage )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:553:1: (lv_subpackages_24_0= ruleWPackage )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:554:3: lv_subpackages_24_0= ruleWPackage
                            {
                             
                            	        newCompositeNode(grammarAccess.getWPackageAccess().getSubpackagesWPackageParserRuleCall_3_6_2_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleWPackage_in_ruleWPackage1022);
                            lv_subpackages_24_0=ruleWPackage();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWPackageRule());
                            	        }
                                   		add(
                                   			current, 
                                   			"subpackages",
                                    		lv_subpackages_24_0, 
                                    		"WPackage");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }

                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:570:2: (otherlv_25= ',' ( (lv_subpackages_26_0= ruleWPackage ) ) )*
                            loop14:
                            do {
                                int alt14=2;
                                int LA14_0 = input.LA(1);

                                if ( (LA14_0==19) ) {
                                    alt14=1;
                                }


                                switch (alt14) {
                            	case 1 :
                            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:570:4: otherlv_25= ',' ( (lv_subpackages_26_0= ruleWPackage ) )
                            	    {
                            	    otherlv_25=(Token)match(input,19,FollowSets000.FOLLOW_19_in_ruleWPackage1035); 

                            	        	newLeafNode(otherlv_25, grammarAccess.getWPackageAccess().getCommaKeyword_3_6_3_0());
                            	        
                            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:574:1: ( (lv_subpackages_26_0= ruleWPackage ) )
                            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:575:1: (lv_subpackages_26_0= ruleWPackage )
                            	    {
                            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:575:1: (lv_subpackages_26_0= ruleWPackage )
                            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:576:3: lv_subpackages_26_0= ruleWPackage
                            	    {
                            	     
                            	    	        newCompositeNode(grammarAccess.getWPackageAccess().getSubpackagesWPackageParserRuleCall_3_6_3_1_0()); 
                            	    	    
                            	    pushFollow(FollowSets000.FOLLOW_ruleWPackage_in_ruleWPackage1056);
                            	    lv_subpackages_26_0=ruleWPackage();

                            	    state._fsp--;


                            	    	        if (current==null) {
                            	    	            current = createModelElementForParent(grammarAccess.getWPackageRule());
                            	    	        }
                            	           		add(
                            	           			current, 
                            	           			"subpackages",
                            	            		lv_subpackages_26_0, 
                            	            		"WPackage");
                            	    	        afterParserOrEnumRuleCall();
                            	    	    

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop14;
                                }
                            } while (true);

                            otherlv_27=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleWPackage1070); 

                                	newLeafNode(otherlv_27, grammarAccess.getWPackageAccess().getRightCurlyBracketKeyword_3_6_4());
                                

                            }
                            break;

                    }

                    otherlv_28=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleWPackage1084); 

                        	newLeafNode(otherlv_28, grammarAccess.getWPackageAccess().getRightCurlyBracketKeyword_3_7());
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWPackage"


    // $ANTLR start "entryRuleWReports"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:608:1: entryRuleWReports returns [EObject current=null] : iv_ruleWReports= ruleWReports EOF ;
    public final EObject entryRuleWReports() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWReports = null;


        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:609:2: (iv_ruleWReports= ruleWReports EOF )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:610:2: iv_ruleWReports= ruleWReports EOF
            {
             newCompositeNode(grammarAccess.getWReportsRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleWReports_in_entryRuleWReports1122);
            iv_ruleWReports=ruleWReports();

            state._fsp--;

             current =iv_ruleWReports; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleWReports1132); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWReports"


    // $ANTLR start "ruleWReports"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:617:1: ruleWReports returns [EObject current=null] : ( () otherlv_1= '{' ( (lv_reports_2_0= ruleWReport ) ) (otherlv_3= ',' ( (lv_reports_4_0= ruleWReport ) ) )* otherlv_5= '}' ) ;
    public final EObject ruleWReports() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_reports_2_0 = null;

        EObject lv_reports_4_0 = null;


         enterRule(); 
            
        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:620:28: ( ( () otherlv_1= '{' ( (lv_reports_2_0= ruleWReport ) ) (otherlv_3= ',' ( (lv_reports_4_0= ruleWReport ) ) )* otherlv_5= '}' ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:621:1: ( () otherlv_1= '{' ( (lv_reports_2_0= ruleWReport ) ) (otherlv_3= ',' ( (lv_reports_4_0= ruleWReport ) ) )* otherlv_5= '}' )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:621:1: ( () otherlv_1= '{' ( (lv_reports_2_0= ruleWReport ) ) (otherlv_3= ',' ( (lv_reports_4_0= ruleWReport ) ) )* otherlv_5= '}' )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:621:2: () otherlv_1= '{' ( (lv_reports_2_0= ruleWReport ) ) (otherlv_3= ',' ( (lv_reports_4_0= ruleWReport ) ) )* otherlv_5= '}'
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:621:2: ()
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:622:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getWReportsAccess().getWReportsAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleWReports1178); 

                	newLeafNode(otherlv_1, grammarAccess.getWReportsAccess().getLeftCurlyBracketKeyword_1());
                
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:631:1: ( (lv_reports_2_0= ruleWReport ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:632:1: (lv_reports_2_0= ruleWReport )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:632:1: (lv_reports_2_0= ruleWReport )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:633:3: lv_reports_2_0= ruleWReport
            {
             
            	        newCompositeNode(grammarAccess.getWReportsAccess().getReportsWReportParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleWReport_in_ruleWReports1199);
            lv_reports_2_0=ruleWReport();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getWReportsRule());
            	        }
                   		add(
                   			current, 
                   			"reports",
                    		lv_reports_2_0, 
                    		"WReport");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:649:2: (otherlv_3= ',' ( (lv_reports_4_0= ruleWReport ) ) )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==19) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:649:4: otherlv_3= ',' ( (lv_reports_4_0= ruleWReport ) )
            	    {
            	    otherlv_3=(Token)match(input,19,FollowSets000.FOLLOW_19_in_ruleWReports1212); 

            	        	newLeafNode(otherlv_3, grammarAccess.getWReportsAccess().getCommaKeyword_3_0());
            	        
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:653:1: ( (lv_reports_4_0= ruleWReport ) )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:654:1: (lv_reports_4_0= ruleWReport )
            	    {
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:654:1: (lv_reports_4_0= ruleWReport )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:655:3: lv_reports_4_0= ruleWReport
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getWReportsAccess().getReportsWReportParserRuleCall_3_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleWReport_in_ruleWReports1233);
            	    lv_reports_4_0=ruleWReport();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getWReportsRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"reports",
            	            		lv_reports_4_0, 
            	            		"WReport");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

            otherlv_5=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleWReports1247); 

                	newLeafNode(otherlv_5, grammarAccess.getWReportsAccess().getRightCurlyBracketKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWReports"


    // $ANTLR start "entryRuleWEnumerations"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:683:1: entryRuleWEnumerations returns [EObject current=null] : iv_ruleWEnumerations= ruleWEnumerations EOF ;
    public final EObject entryRuleWEnumerations() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWEnumerations = null;


        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:684:2: (iv_ruleWEnumerations= ruleWEnumerations EOF )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:685:2: iv_ruleWEnumerations= ruleWEnumerations EOF
            {
             newCompositeNode(grammarAccess.getWEnumerationsRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleWEnumerations_in_entryRuleWEnumerations1283);
            iv_ruleWEnumerations=ruleWEnumerations();

            state._fsp--;

             current =iv_ruleWEnumerations; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleWEnumerations1293); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWEnumerations"


    // $ANTLR start "ruleWEnumerations"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:692:1: ruleWEnumerations returns [EObject current=null] : ( () ( (lv_enumerations_1_0= ruleWEnum ) ) ) ;
    public final EObject ruleWEnumerations() throws RecognitionException {
        EObject current = null;

        EObject lv_enumerations_1_0 = null;


         enterRule(); 
            
        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:695:28: ( ( () ( (lv_enumerations_1_0= ruleWEnum ) ) ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:696:1: ( () ( (lv_enumerations_1_0= ruleWEnum ) ) )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:696:1: ( () ( (lv_enumerations_1_0= ruleWEnum ) ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:696:2: () ( (lv_enumerations_1_0= ruleWEnum ) )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:696:2: ()
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:697:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getWEnumerationsAccess().getWEnumerationsAction_0(),
                        current);
                

            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:702:2: ( (lv_enumerations_1_0= ruleWEnum ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:703:1: (lv_enumerations_1_0= ruleWEnum )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:703:1: (lv_enumerations_1_0= ruleWEnum )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:704:3: lv_enumerations_1_0= ruleWEnum
            {
             
            	        newCompositeNode(grammarAccess.getWEnumerationsAccess().getEnumerationsWEnumParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleWEnum_in_ruleWEnumerations1348);
            lv_enumerations_1_0=ruleWEnum();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getWEnumerationsRule());
            	        }
                   		set(
                   			current, 
                   			"enumerations",
                    		lv_enumerations_1_0, 
                    		"WEnum");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWEnumerations"


    // $ANTLR start "entryRuleWChanges"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:728:1: entryRuleWChanges returns [EObject current=null] : iv_ruleWChanges= ruleWChanges EOF ;
    public final EObject entryRuleWChanges() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWChanges = null;


        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:729:2: (iv_ruleWChanges= ruleWChanges EOF )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:730:2: iv_ruleWChanges= ruleWChanges EOF
            {
             newCompositeNode(grammarAccess.getWChangesRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleWChanges_in_entryRuleWChanges1384);
            iv_ruleWChanges=ruleWChanges();

            state._fsp--;

             current =iv_ruleWChanges; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleWChanges1394); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWChanges"


    // $ANTLR start "ruleWChanges"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:737:1: ruleWChanges returns [EObject current=null] : ( () ( ( (lv_changes_1_0= ruleWChange ) ) (otherlv_2= ',' ( (lv_changes_3_0= ruleWChange ) ) )* )? ) ;
    public final EObject ruleWChanges() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject lv_changes_1_0 = null;

        EObject lv_changes_3_0 = null;


         enterRule(); 
            
        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:740:28: ( ( () ( ( (lv_changes_1_0= ruleWChange ) ) (otherlv_2= ',' ( (lv_changes_3_0= ruleWChange ) ) )* )? ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:741:1: ( () ( ( (lv_changes_1_0= ruleWChange ) ) (otherlv_2= ',' ( (lv_changes_3_0= ruleWChange ) ) )* )? )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:741:1: ( () ( ( (lv_changes_1_0= ruleWChange ) ) (otherlv_2= ',' ( (lv_changes_3_0= ruleWChange ) ) )* )? )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:741:2: () ( ( (lv_changes_1_0= ruleWChange ) ) (otherlv_2= ',' ( (lv_changes_3_0= ruleWChange ) ) )* )?
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:741:2: ()
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:742:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getWChangesAccess().getWChangesAction_0(),
                        current);
                

            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:747:2: ( ( (lv_changes_1_0= ruleWChange ) ) (otherlv_2= ',' ( (lv_changes_3_0= ruleWChange ) ) )* )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==12) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:747:3: ( (lv_changes_1_0= ruleWChange ) ) (otherlv_2= ',' ( (lv_changes_3_0= ruleWChange ) ) )*
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:747:3: ( (lv_changes_1_0= ruleWChange ) )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:748:1: (lv_changes_1_0= ruleWChange )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:748:1: (lv_changes_1_0= ruleWChange )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:749:3: lv_changes_1_0= ruleWChange
                    {
                     
                    	        newCompositeNode(grammarAccess.getWChangesAccess().getChangesWChangeParserRuleCall_1_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleWChange_in_ruleWChanges1450);
                    lv_changes_1_0=ruleWChange();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getWChangesRule());
                    	        }
                           		add(
                           			current, 
                           			"changes",
                            		lv_changes_1_0, 
                            		"WChange");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:765:2: (otherlv_2= ',' ( (lv_changes_3_0= ruleWChange ) ) )*
                    loop18:
                    do {
                        int alt18=2;
                        int LA18_0 = input.LA(1);

                        if ( (LA18_0==19) ) {
                            alt18=1;
                        }


                        switch (alt18) {
                    	case 1 :
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:765:4: otherlv_2= ',' ( (lv_changes_3_0= ruleWChange ) )
                    	    {
                    	    otherlv_2=(Token)match(input,19,FollowSets000.FOLLOW_19_in_ruleWChanges1463); 

                    	        	newLeafNode(otherlv_2, grammarAccess.getWChangesAccess().getCommaKeyword_1_1_0());
                    	        
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:769:1: ( (lv_changes_3_0= ruleWChange ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:770:1: (lv_changes_3_0= ruleWChange )
                    	    {
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:770:1: (lv_changes_3_0= ruleWChange )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:771:3: lv_changes_3_0= ruleWChange
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getWChangesAccess().getChangesWChangeParserRuleCall_1_1_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleWChange_in_ruleWChanges1484);
                    	    lv_changes_3_0=ruleWChange();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getWChangesRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"changes",
                    	            		lv_changes_3_0, 
                    	            		"WChange");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop18;
                        }
                    } while (true);


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWChanges"


    // $ANTLR start "entryRuleWChange"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:795:1: entryRuleWChange returns [EObject current=null] : iv_ruleWChange= ruleWChange EOF ;
    public final EObject entryRuleWChange() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWChange = null;


        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:796:2: (iv_ruleWChange= ruleWChange EOF )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:797:2: iv_ruleWChange= ruleWChange EOF
            {
             newCompositeNode(grammarAccess.getWChangeRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleWChange_in_entryRuleWChange1524);
            iv_ruleWChange=ruleWChange();

            state._fsp--;

             current =iv_ruleWChange; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleWChange1534); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWChange"


    // $ANTLR start "ruleWChange"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:804:1: ruleWChange returns [EObject current=null] : ( () (otherlv_1= '{' (otherlv_2= 'version' ( (lv_version_3_0= ruleEIntegerObject ) ) )? (otherlv_4= 'message' ( (lv_message_5_0= ruleEString ) ) )? (otherlv_6= 'messageDev' ( (lv_messageDev_7_0= ruleEString ) ) )? (otherlv_8= 'path' ( (lv_path_9_0= ruleEString ) ) )? (otherlv_10= 'type' ( (lv_type_11_0= ruleEChangesType ) ) )? (otherlv_12= 'elementType' ( (lv_elementType_13_0= ruleEElementChangesType ) ) )? (otherlv_14= 'element' ( (otherlv_15= 'attribute' otherlv_16= '{' ( (lv_element_17_0= ruleWAttribute ) ) otherlv_18= '}' ) | (otherlv_19= 'link' otherlv_20= '{' ( (lv_element_21_0= ruleWLink ) ) otherlv_22= '}' ) | (otherlv_23= 'entity' otherlv_24= '{' ( (lv_element_25_0= ruleWEntity ) ) otherlv_26= '}' ) | (otherlv_27= 'package' otherlv_28= '{' ( (lv_element_29_0= ruleWPackage ) ) otherlv_30= '}' ) ) )? otherlv_31= '}' ) ) ;
    public final EObject ruleWChange() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_18=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token otherlv_22=null;
        Token otherlv_23=null;
        Token otherlv_24=null;
        Token otherlv_26=null;
        Token otherlv_27=null;
        Token otherlv_28=null;
        Token otherlv_30=null;
        Token otherlv_31=null;
        AntlrDatatypeRuleToken lv_version_3_0 = null;

        AntlrDatatypeRuleToken lv_message_5_0 = null;

        AntlrDatatypeRuleToken lv_messageDev_7_0 = null;

        AntlrDatatypeRuleToken lv_path_9_0 = null;

        Enumerator lv_type_11_0 = null;

        Enumerator lv_elementType_13_0 = null;

        EObject lv_element_17_0 = null;

        EObject lv_element_21_0 = null;

        EObject lv_element_25_0 = null;

        EObject lv_element_29_0 = null;


         enterRule(); 
            
        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:807:28: ( ( () (otherlv_1= '{' (otherlv_2= 'version' ( (lv_version_3_0= ruleEIntegerObject ) ) )? (otherlv_4= 'message' ( (lv_message_5_0= ruleEString ) ) )? (otherlv_6= 'messageDev' ( (lv_messageDev_7_0= ruleEString ) ) )? (otherlv_8= 'path' ( (lv_path_9_0= ruleEString ) ) )? (otherlv_10= 'type' ( (lv_type_11_0= ruleEChangesType ) ) )? (otherlv_12= 'elementType' ( (lv_elementType_13_0= ruleEElementChangesType ) ) )? (otherlv_14= 'element' ( (otherlv_15= 'attribute' otherlv_16= '{' ( (lv_element_17_0= ruleWAttribute ) ) otherlv_18= '}' ) | (otherlv_19= 'link' otherlv_20= '{' ( (lv_element_21_0= ruleWLink ) ) otherlv_22= '}' ) | (otherlv_23= 'entity' otherlv_24= '{' ( (lv_element_25_0= ruleWEntity ) ) otherlv_26= '}' ) | (otherlv_27= 'package' otherlv_28= '{' ( (lv_element_29_0= ruleWPackage ) ) otherlv_30= '}' ) ) )? otherlv_31= '}' ) ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:808:1: ( () (otherlv_1= '{' (otherlv_2= 'version' ( (lv_version_3_0= ruleEIntegerObject ) ) )? (otherlv_4= 'message' ( (lv_message_5_0= ruleEString ) ) )? (otherlv_6= 'messageDev' ( (lv_messageDev_7_0= ruleEString ) ) )? (otherlv_8= 'path' ( (lv_path_9_0= ruleEString ) ) )? (otherlv_10= 'type' ( (lv_type_11_0= ruleEChangesType ) ) )? (otherlv_12= 'elementType' ( (lv_elementType_13_0= ruleEElementChangesType ) ) )? (otherlv_14= 'element' ( (otherlv_15= 'attribute' otherlv_16= '{' ( (lv_element_17_0= ruleWAttribute ) ) otherlv_18= '}' ) | (otherlv_19= 'link' otherlv_20= '{' ( (lv_element_21_0= ruleWLink ) ) otherlv_22= '}' ) | (otherlv_23= 'entity' otherlv_24= '{' ( (lv_element_25_0= ruleWEntity ) ) otherlv_26= '}' ) | (otherlv_27= 'package' otherlv_28= '{' ( (lv_element_29_0= ruleWPackage ) ) otherlv_30= '}' ) ) )? otherlv_31= '}' ) )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:808:1: ( () (otherlv_1= '{' (otherlv_2= 'version' ( (lv_version_3_0= ruleEIntegerObject ) ) )? (otherlv_4= 'message' ( (lv_message_5_0= ruleEString ) ) )? (otherlv_6= 'messageDev' ( (lv_messageDev_7_0= ruleEString ) ) )? (otherlv_8= 'path' ( (lv_path_9_0= ruleEString ) ) )? (otherlv_10= 'type' ( (lv_type_11_0= ruleEChangesType ) ) )? (otherlv_12= 'elementType' ( (lv_elementType_13_0= ruleEElementChangesType ) ) )? (otherlv_14= 'element' ( (otherlv_15= 'attribute' otherlv_16= '{' ( (lv_element_17_0= ruleWAttribute ) ) otherlv_18= '}' ) | (otherlv_19= 'link' otherlv_20= '{' ( (lv_element_21_0= ruleWLink ) ) otherlv_22= '}' ) | (otherlv_23= 'entity' otherlv_24= '{' ( (lv_element_25_0= ruleWEntity ) ) otherlv_26= '}' ) | (otherlv_27= 'package' otherlv_28= '{' ( (lv_element_29_0= ruleWPackage ) ) otherlv_30= '}' ) ) )? otherlv_31= '}' ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:808:2: () (otherlv_1= '{' (otherlv_2= 'version' ( (lv_version_3_0= ruleEIntegerObject ) ) )? (otherlv_4= 'message' ( (lv_message_5_0= ruleEString ) ) )? (otherlv_6= 'messageDev' ( (lv_messageDev_7_0= ruleEString ) ) )? (otherlv_8= 'path' ( (lv_path_9_0= ruleEString ) ) )? (otherlv_10= 'type' ( (lv_type_11_0= ruleEChangesType ) ) )? (otherlv_12= 'elementType' ( (lv_elementType_13_0= ruleEElementChangesType ) ) )? (otherlv_14= 'element' ( (otherlv_15= 'attribute' otherlv_16= '{' ( (lv_element_17_0= ruleWAttribute ) ) otherlv_18= '}' ) | (otherlv_19= 'link' otherlv_20= '{' ( (lv_element_21_0= ruleWLink ) ) otherlv_22= '}' ) | (otherlv_23= 'entity' otherlv_24= '{' ( (lv_element_25_0= ruleWEntity ) ) otherlv_26= '}' ) | (otherlv_27= 'package' otherlv_28= '{' ( (lv_element_29_0= ruleWPackage ) ) otherlv_30= '}' ) ) )? otherlv_31= '}' )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:808:2: ()
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:809:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getWChangeAccess().getWChangeAction_0(),
                        current);
                

            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:814:2: (otherlv_1= '{' (otherlv_2= 'version' ( (lv_version_3_0= ruleEIntegerObject ) ) )? (otherlv_4= 'message' ( (lv_message_5_0= ruleEString ) ) )? (otherlv_6= 'messageDev' ( (lv_messageDev_7_0= ruleEString ) ) )? (otherlv_8= 'path' ( (lv_path_9_0= ruleEString ) ) )? (otherlv_10= 'type' ( (lv_type_11_0= ruleEChangesType ) ) )? (otherlv_12= 'elementType' ( (lv_elementType_13_0= ruleEElementChangesType ) ) )? (otherlv_14= 'element' ( (otherlv_15= 'attribute' otherlv_16= '{' ( (lv_element_17_0= ruleWAttribute ) ) otherlv_18= '}' ) | (otherlv_19= 'link' otherlv_20= '{' ( (lv_element_21_0= ruleWLink ) ) otherlv_22= '}' ) | (otherlv_23= 'entity' otherlv_24= '{' ( (lv_element_25_0= ruleWEntity ) ) otherlv_26= '}' ) | (otherlv_27= 'package' otherlv_28= '{' ( (lv_element_29_0= ruleWPackage ) ) otherlv_30= '}' ) ) )? otherlv_31= '}' )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:814:4: otherlv_1= '{' (otherlv_2= 'version' ( (lv_version_3_0= ruleEIntegerObject ) ) )? (otherlv_4= 'message' ( (lv_message_5_0= ruleEString ) ) )? (otherlv_6= 'messageDev' ( (lv_messageDev_7_0= ruleEString ) ) )? (otherlv_8= 'path' ( (lv_path_9_0= ruleEString ) ) )? (otherlv_10= 'type' ( (lv_type_11_0= ruleEChangesType ) ) )? (otherlv_12= 'elementType' ( (lv_elementType_13_0= ruleEElementChangesType ) ) )? (otherlv_14= 'element' ( (otherlv_15= 'attribute' otherlv_16= '{' ( (lv_element_17_0= ruleWAttribute ) ) otherlv_18= '}' ) | (otherlv_19= 'link' otherlv_20= '{' ( (lv_element_21_0= ruleWLink ) ) otherlv_22= '}' ) | (otherlv_23= 'entity' otherlv_24= '{' ( (lv_element_25_0= ruleWEntity ) ) otherlv_26= '}' ) | (otherlv_27= 'package' otherlv_28= '{' ( (lv_element_29_0= ruleWPackage ) ) otherlv_30= '}' ) ) )? otherlv_31= '}'
            {
            otherlv_1=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleWChange1581); 

                	newLeafNode(otherlv_1, grammarAccess.getWChangeAccess().getLeftCurlyBracketKeyword_1_0());
                
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:818:1: (otherlv_2= 'version' ( (lv_version_3_0= ruleEIntegerObject ) ) )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==14) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:818:3: otherlv_2= 'version' ( (lv_version_3_0= ruleEIntegerObject ) )
                    {
                    otherlv_2=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleWChange1594); 

                        	newLeafNode(otherlv_2, grammarAccess.getWChangeAccess().getVersionKeyword_1_1_0());
                        
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:822:1: ( (lv_version_3_0= ruleEIntegerObject ) )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:823:1: (lv_version_3_0= ruleEIntegerObject )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:823:1: (lv_version_3_0= ruleEIntegerObject )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:824:3: lv_version_3_0= ruleEIntegerObject
                    {
                     
                    	        newCompositeNode(grammarAccess.getWChangeAccess().getVersionEIntegerObjectParserRuleCall_1_1_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEIntegerObject_in_ruleWChange1615);
                    lv_version_3_0=ruleEIntegerObject();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getWChangeRule());
                    	        }
                           		set(
                           			current, 
                           			"version",
                            		lv_version_3_0, 
                            		"EIntegerObject");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:840:4: (otherlv_4= 'message' ( (lv_message_5_0= ruleEString ) ) )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==29) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:840:6: otherlv_4= 'message' ( (lv_message_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,29,FollowSets000.FOLLOW_29_in_ruleWChange1630); 

                        	newLeafNode(otherlv_4, grammarAccess.getWChangeAccess().getMessageKeyword_1_2_0());
                        
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:844:1: ( (lv_message_5_0= ruleEString ) )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:845:1: (lv_message_5_0= ruleEString )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:845:1: (lv_message_5_0= ruleEString )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:846:3: lv_message_5_0= ruleEString
                    {
                     
                    	        newCompositeNode(grammarAccess.getWChangeAccess().getMessageEStringParserRuleCall_1_2_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWChange1651);
                    lv_message_5_0=ruleEString();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getWChangeRule());
                    	        }
                           		set(
                           			current, 
                           			"message",
                            		lv_message_5_0, 
                            		"EString");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:862:4: (otherlv_6= 'messageDev' ( (lv_messageDev_7_0= ruleEString ) ) )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==30) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:862:6: otherlv_6= 'messageDev' ( (lv_messageDev_7_0= ruleEString ) )
                    {
                    otherlv_6=(Token)match(input,30,FollowSets000.FOLLOW_30_in_ruleWChange1666); 

                        	newLeafNode(otherlv_6, grammarAccess.getWChangeAccess().getMessageDevKeyword_1_3_0());
                        
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:866:1: ( (lv_messageDev_7_0= ruleEString ) )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:867:1: (lv_messageDev_7_0= ruleEString )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:867:1: (lv_messageDev_7_0= ruleEString )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:868:3: lv_messageDev_7_0= ruleEString
                    {
                     
                    	        newCompositeNode(grammarAccess.getWChangeAccess().getMessageDevEStringParserRuleCall_1_3_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWChange1687);
                    lv_messageDev_7_0=ruleEString();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getWChangeRule());
                    	        }
                           		set(
                           			current, 
                           			"messageDev",
                            		lv_messageDev_7_0, 
                            		"EString");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:884:4: (otherlv_8= 'path' ( (lv_path_9_0= ruleEString ) ) )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==31) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:884:6: otherlv_8= 'path' ( (lv_path_9_0= ruleEString ) )
                    {
                    otherlv_8=(Token)match(input,31,FollowSets000.FOLLOW_31_in_ruleWChange1702); 

                        	newLeafNode(otherlv_8, grammarAccess.getWChangeAccess().getPathKeyword_1_4_0());
                        
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:888:1: ( (lv_path_9_0= ruleEString ) )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:889:1: (lv_path_9_0= ruleEString )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:889:1: (lv_path_9_0= ruleEString )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:890:3: lv_path_9_0= ruleEString
                    {
                     
                    	        newCompositeNode(grammarAccess.getWChangeAccess().getPathEStringParserRuleCall_1_4_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWChange1723);
                    lv_path_9_0=ruleEString();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getWChangeRule());
                    	        }
                           		set(
                           			current, 
                           			"path",
                            		lv_path_9_0, 
                            		"EString");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:906:4: (otherlv_10= 'type' ( (lv_type_11_0= ruleEChangesType ) ) )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==32) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:906:6: otherlv_10= 'type' ( (lv_type_11_0= ruleEChangesType ) )
                    {
                    otherlv_10=(Token)match(input,32,FollowSets000.FOLLOW_32_in_ruleWChange1738); 

                        	newLeafNode(otherlv_10, grammarAccess.getWChangeAccess().getTypeKeyword_1_5_0());
                        
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:910:1: ( (lv_type_11_0= ruleEChangesType ) )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:911:1: (lv_type_11_0= ruleEChangesType )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:911:1: (lv_type_11_0= ruleEChangesType )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:912:3: lv_type_11_0= ruleEChangesType
                    {
                     
                    	        newCompositeNode(grammarAccess.getWChangeAccess().getTypeEChangesTypeEnumRuleCall_1_5_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEChangesType_in_ruleWChange1759);
                    lv_type_11_0=ruleEChangesType();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getWChangeRule());
                    	        }
                           		set(
                           			current, 
                           			"type",
                            		lv_type_11_0, 
                            		"EChangesType");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:928:4: (otherlv_12= 'elementType' ( (lv_elementType_13_0= ruleEElementChangesType ) ) )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==33) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:928:6: otherlv_12= 'elementType' ( (lv_elementType_13_0= ruleEElementChangesType ) )
                    {
                    otherlv_12=(Token)match(input,33,FollowSets000.FOLLOW_33_in_ruleWChange1774); 

                        	newLeafNode(otherlv_12, grammarAccess.getWChangeAccess().getElementTypeKeyword_1_6_0());
                        
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:932:1: ( (lv_elementType_13_0= ruleEElementChangesType ) )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:933:1: (lv_elementType_13_0= ruleEElementChangesType )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:933:1: (lv_elementType_13_0= ruleEElementChangesType )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:934:3: lv_elementType_13_0= ruleEElementChangesType
                    {
                     
                    	        newCompositeNode(grammarAccess.getWChangeAccess().getElementTypeEElementChangesTypeEnumRuleCall_1_6_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEElementChangesType_in_ruleWChange1795);
                    lv_elementType_13_0=ruleEElementChangesType();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getWChangeRule());
                    	        }
                           		set(
                           			current, 
                           			"elementType",
                            		lv_elementType_13_0, 
                            		"EElementChangesType");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:950:4: (otherlv_14= 'element' ( (otherlv_15= 'attribute' otherlv_16= '{' ( (lv_element_17_0= ruleWAttribute ) ) otherlv_18= '}' ) | (otherlv_19= 'link' otherlv_20= '{' ( (lv_element_21_0= ruleWLink ) ) otherlv_22= '}' ) | (otherlv_23= 'entity' otherlv_24= '{' ( (lv_element_25_0= ruleWEntity ) ) otherlv_26= '}' ) | (otherlv_27= 'package' otherlv_28= '{' ( (lv_element_29_0= ruleWPackage ) ) otherlv_30= '}' ) ) )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==34) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:950:6: otherlv_14= 'element' ( (otherlv_15= 'attribute' otherlv_16= '{' ( (lv_element_17_0= ruleWAttribute ) ) otherlv_18= '}' ) | (otherlv_19= 'link' otherlv_20= '{' ( (lv_element_21_0= ruleWLink ) ) otherlv_22= '}' ) | (otherlv_23= 'entity' otherlv_24= '{' ( (lv_element_25_0= ruleWEntity ) ) otherlv_26= '}' ) | (otherlv_27= 'package' otherlv_28= '{' ( (lv_element_29_0= ruleWPackage ) ) otherlv_30= '}' ) )
                    {
                    otherlv_14=(Token)match(input,34,FollowSets000.FOLLOW_34_in_ruleWChange1810); 

                        	newLeafNode(otherlv_14, grammarAccess.getWChangeAccess().getElementKeyword_1_7_0());
                        
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:954:1: ( (otherlv_15= 'attribute' otherlv_16= '{' ( (lv_element_17_0= ruleWAttribute ) ) otherlv_18= '}' ) | (otherlv_19= 'link' otherlv_20= '{' ( (lv_element_21_0= ruleWLink ) ) otherlv_22= '}' ) | (otherlv_23= 'entity' otherlv_24= '{' ( (lv_element_25_0= ruleWEntity ) ) otherlv_26= '}' ) | (otherlv_27= 'package' otherlv_28= '{' ( (lv_element_29_0= ruleWPackage ) ) otherlv_30= '}' ) )
                    int alt26=4;
                    switch ( input.LA(1) ) {
                    case 35:
                        {
                        alt26=1;
                        }
                        break;
                    case 36:
                        {
                        alt26=2;
                        }
                        break;
                    case 37:
                        {
                        alt26=3;
                        }
                        break;
                    case 23:
                        {
                        alt26=4;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 26, 0, input);

                        throw nvae;
                    }

                    switch (alt26) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:954:2: (otherlv_15= 'attribute' otherlv_16= '{' ( (lv_element_17_0= ruleWAttribute ) ) otherlv_18= '}' )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:954:2: (otherlv_15= 'attribute' otherlv_16= '{' ( (lv_element_17_0= ruleWAttribute ) ) otherlv_18= '}' )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:954:4: otherlv_15= 'attribute' otherlv_16= '{' ( (lv_element_17_0= ruleWAttribute ) ) otherlv_18= '}'
                            {
                            otherlv_15=(Token)match(input,35,FollowSets000.FOLLOW_35_in_ruleWChange1824); 

                                	newLeafNode(otherlv_15, grammarAccess.getWChangeAccess().getAttributeKeyword_1_7_1_0_0());
                                
                            otherlv_16=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleWChange1836); 

                                	newLeafNode(otherlv_16, grammarAccess.getWChangeAccess().getLeftCurlyBracketKeyword_1_7_1_0_1());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:962:1: ( (lv_element_17_0= ruleWAttribute ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:963:1: (lv_element_17_0= ruleWAttribute )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:963:1: (lv_element_17_0= ruleWAttribute )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:964:3: lv_element_17_0= ruleWAttribute
                            {
                             
                            	        newCompositeNode(grammarAccess.getWChangeAccess().getElementWAttributeParserRuleCall_1_7_1_0_2_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleWAttribute_in_ruleWChange1857);
                            lv_element_17_0=ruleWAttribute();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWChangeRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"element",
                                    		lv_element_17_0, 
                                    		"WAttribute");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }

                            otherlv_18=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleWChange1869); 

                                	newLeafNode(otherlv_18, grammarAccess.getWChangeAccess().getRightCurlyBracketKeyword_1_7_1_0_3());
                                

                            }


                            }
                            break;
                        case 2 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:985:6: (otherlv_19= 'link' otherlv_20= '{' ( (lv_element_21_0= ruleWLink ) ) otherlv_22= '}' )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:985:6: (otherlv_19= 'link' otherlv_20= '{' ( (lv_element_21_0= ruleWLink ) ) otherlv_22= '}' )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:985:8: otherlv_19= 'link' otherlv_20= '{' ( (lv_element_21_0= ruleWLink ) ) otherlv_22= '}'
                            {
                            otherlv_19=(Token)match(input,36,FollowSets000.FOLLOW_36_in_ruleWChange1889); 

                                	newLeafNode(otherlv_19, grammarAccess.getWChangeAccess().getLinkKeyword_1_7_1_1_0());
                                
                            otherlv_20=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleWChange1901); 

                                	newLeafNode(otherlv_20, grammarAccess.getWChangeAccess().getLeftCurlyBracketKeyword_1_7_1_1_1());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:993:1: ( (lv_element_21_0= ruleWLink ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:994:1: (lv_element_21_0= ruleWLink )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:994:1: (lv_element_21_0= ruleWLink )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:995:3: lv_element_21_0= ruleWLink
                            {
                             
                            	        newCompositeNode(grammarAccess.getWChangeAccess().getElementWLinkParserRuleCall_1_7_1_1_2_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleWLink_in_ruleWChange1922);
                            lv_element_21_0=ruleWLink();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWChangeRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"element",
                                    		lv_element_21_0, 
                                    		"WLink");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }

                            otherlv_22=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleWChange1934); 

                                	newLeafNode(otherlv_22, grammarAccess.getWChangeAccess().getRightCurlyBracketKeyword_1_7_1_1_3());
                                

                            }


                            }
                            break;
                        case 3 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1016:6: (otherlv_23= 'entity' otherlv_24= '{' ( (lv_element_25_0= ruleWEntity ) ) otherlv_26= '}' )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1016:6: (otherlv_23= 'entity' otherlv_24= '{' ( (lv_element_25_0= ruleWEntity ) ) otherlv_26= '}' )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1016:8: otherlv_23= 'entity' otherlv_24= '{' ( (lv_element_25_0= ruleWEntity ) ) otherlv_26= '}'
                            {
                            otherlv_23=(Token)match(input,37,FollowSets000.FOLLOW_37_in_ruleWChange1954); 

                                	newLeafNode(otherlv_23, grammarAccess.getWChangeAccess().getEntityKeyword_1_7_1_2_0());
                                
                            otherlv_24=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleWChange1966); 

                                	newLeafNode(otherlv_24, grammarAccess.getWChangeAccess().getLeftCurlyBracketKeyword_1_7_1_2_1());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1024:1: ( (lv_element_25_0= ruleWEntity ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1025:1: (lv_element_25_0= ruleWEntity )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1025:1: (lv_element_25_0= ruleWEntity )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1026:3: lv_element_25_0= ruleWEntity
                            {
                             
                            	        newCompositeNode(grammarAccess.getWChangeAccess().getElementWEntityParserRuleCall_1_7_1_2_2_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleWEntity_in_ruleWChange1987);
                            lv_element_25_0=ruleWEntity();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWChangeRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"element",
                                    		lv_element_25_0, 
                                    		"WEntity");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }

                            otherlv_26=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleWChange1999); 

                                	newLeafNode(otherlv_26, grammarAccess.getWChangeAccess().getRightCurlyBracketKeyword_1_7_1_2_3());
                                

                            }


                            }
                            break;
                        case 4 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1047:6: (otherlv_27= 'package' otherlv_28= '{' ( (lv_element_29_0= ruleWPackage ) ) otherlv_30= '}' )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1047:6: (otherlv_27= 'package' otherlv_28= '{' ( (lv_element_29_0= ruleWPackage ) ) otherlv_30= '}' )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1047:8: otherlv_27= 'package' otherlv_28= '{' ( (lv_element_29_0= ruleWPackage ) ) otherlv_30= '}'
                            {
                            otherlv_27=(Token)match(input,23,FollowSets000.FOLLOW_23_in_ruleWChange2019); 

                                	newLeafNode(otherlv_27, grammarAccess.getWChangeAccess().getPackageKeyword_1_7_1_3_0());
                                
                            otherlv_28=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleWChange2031); 

                                	newLeafNode(otherlv_28, grammarAccess.getWChangeAccess().getLeftCurlyBracketKeyword_1_7_1_3_1());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1055:1: ( (lv_element_29_0= ruleWPackage ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1056:1: (lv_element_29_0= ruleWPackage )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1056:1: (lv_element_29_0= ruleWPackage )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1057:3: lv_element_29_0= ruleWPackage
                            {
                             
                            	        newCompositeNode(grammarAccess.getWChangeAccess().getElementWPackageParserRuleCall_1_7_1_3_2_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleWPackage_in_ruleWChange2052);
                            lv_element_29_0=ruleWPackage();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWChangeRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"element",
                                    		lv_element_29_0, 
                                    		"WPackage");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }

                            otherlv_30=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleWChange2064); 

                                	newLeafNode(otherlv_30, grammarAccess.getWChangeAccess().getRightCurlyBracketKeyword_1_7_1_3_3());
                                

                            }


                            }
                            break;

                    }


                    }
                    break;

            }

            otherlv_31=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleWChange2080); 

                	newLeafNode(otherlv_31, grammarAccess.getWChangeAccess().getRightCurlyBracketKeyword_1_8());
                

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWChange"


    // $ANTLR start "entryRuleEString"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1089:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1090:2: (iv_ruleEString= ruleEString EOF )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1091:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_entryRuleEString2118);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEString2129); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1098:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;

         enterRule(); 
            
        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1101:28: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1102:1: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1102:1: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==RULE_STRING) ) {
                alt28=1;
            }
            else if ( (LA28_0==RULE_ID) ) {
                alt28=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 28, 0, input);

                throw nvae;
            }
            switch (alt28) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1102:6: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_ruleEString2169); 

                    		current.merge(this_STRING_0);
                        
                     
                        newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                        

                    }
                    break;
                case 2 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1110:10: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_ruleEString2195); 

                    		current.merge(this_ID_1);
                        
                     
                        newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleEBooleanObject"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1125:1: entryRuleEBooleanObject returns [String current=null] : iv_ruleEBooleanObject= ruleEBooleanObject EOF ;
    public final String entryRuleEBooleanObject() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEBooleanObject = null;


        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1126:2: (iv_ruleEBooleanObject= ruleEBooleanObject EOF )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1127:2: iv_ruleEBooleanObject= ruleEBooleanObject EOF
            {
             newCompositeNode(grammarAccess.getEBooleanObjectRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEBooleanObject_in_entryRuleEBooleanObject2241);
            iv_ruleEBooleanObject=ruleEBooleanObject();

            state._fsp--;

             current =iv_ruleEBooleanObject.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEBooleanObject2252); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEBooleanObject"


    // $ANTLR start "ruleEBooleanObject"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1134:1: ruleEBooleanObject returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'true' | kw= 'false' ) ;
    public final AntlrDatatypeRuleToken ruleEBooleanObject() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1137:28: ( (kw= 'true' | kw= 'false' ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1138:1: (kw= 'true' | kw= 'false' )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1138:1: (kw= 'true' | kw= 'false' )
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==38) ) {
                alt29=1;
            }
            else if ( (LA29_0==39) ) {
                alt29=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;
            }
            switch (alt29) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1139:2: kw= 'true'
                    {
                    kw=(Token)match(input,38,FollowSets000.FOLLOW_38_in_ruleEBooleanObject2290); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getEBooleanObjectAccess().getTrueKeyword_0()); 
                        

                    }
                    break;
                case 2 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1146:2: kw= 'false'
                    {
                    kw=(Token)match(input,39,FollowSets000.FOLLOW_39_in_ruleEBooleanObject2309); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getEBooleanObjectAccess().getFalseKeyword_1()); 
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEBooleanObject"


    // $ANTLR start "entryRuleWEntity"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1159:1: entryRuleWEntity returns [EObject current=null] : iv_ruleWEntity= ruleWEntity EOF ;
    public final EObject entryRuleWEntity() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWEntity = null;


        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1160:2: (iv_ruleWEntity= ruleWEntity EOF )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1161:2: iv_ruleWEntity= ruleWEntity EOF
            {
             newCompositeNode(grammarAccess.getWEntityRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleWEntity_in_entryRuleWEntity2349);
            iv_ruleWEntity=ruleWEntity();

            state._fsp--;

             current =iv_ruleWEntity; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleWEntity2359); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWEntity"


    // $ANTLR start "ruleWEntity"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1168:1: ruleWEntity returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+ {...}?) ) )? ) ;
    public final EObject ruleWEntity() throws RecognitionException {
        EObject current = null;

        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_version_5_0 = null;

        AntlrDatatypeRuleToken lv_label_7_0 = null;

        AntlrDatatypeRuleToken lv_generate_9_0 = null;

        EObject lv_attributes_12_0 = null;

        EObject lv_attributes_14_0 = null;


         enterRule(); 
            
        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1171:28: ( ( () ( (lv_name_1_0= ruleEString ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+ {...}?) ) )? ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1172:1: ( () ( (lv_name_1_0= ruleEString ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+ {...}?) ) )? )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1172:1: ( () ( (lv_name_1_0= ruleEString ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+ {...}?) ) )? )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1172:2: () ( (lv_name_1_0= ruleEString ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+ {...}?) ) )?
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1172:2: ()
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1173:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getWEntityAccess().getWEntityAction_0(),
                        current);
                

            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1178:2: ( (lv_name_1_0= ruleEString ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1179:1: (lv_name_1_0= ruleEString )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1179:1: (lv_name_1_0= ruleEString )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1180:3: lv_name_1_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getWEntityAccess().getNameEStringParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWEntity2414);
            lv_name_1_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getWEntityRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1196:2: ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+ {...}?) ) )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==12||LA33_0==25||LA33_0==27||LA33_0==40) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1198:1: ( ( ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+ {...}?) )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1198:1: ( ( ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+ {...}?) )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1199:2: ( ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+ {...}?)
                    {
                     
                    	  getUnorderedGroupHelper().enter(grammarAccess.getWEntityAccess().getUnorderedGroup_2());
                    	
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1202:2: ( ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+ {...}?)
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1203:3: ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+ {...}?
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1203:3: ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+
                    int cnt32=0;
                    loop32:
                    do {
                        int alt32=5;
                        int LA32_0 = input.LA(1);

                        if ( LA32_0 ==12 && getUnorderedGroupHelper().canSelect(grammarAccess.getWEntityAccess().getUnorderedGroup_2(), 0) ) {
                            alt32=1;
                        }
                        else if ( LA32_0 ==25 && getUnorderedGroupHelper().canSelect(grammarAccess.getWEntityAccess().getUnorderedGroup_2(), 1) ) {
                            alt32=2;
                        }
                        else if ( LA32_0 ==27 && getUnorderedGroupHelper().canSelect(grammarAccess.getWEntityAccess().getUnorderedGroup_2(), 2) ) {
                            alt32=3;
                        }
                        else if ( LA32_0 ==40 && getUnorderedGroupHelper().canSelect(grammarAccess.getWEntityAccess().getUnorderedGroup_2(), 3) ) {
                            alt32=4;
                        }


                        switch (alt32) {
                    	case 1 :
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1205:4: ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) )
                    	    {
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1205:4: ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1206:5: {...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) )
                    	    {
                    	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getWEntityAccess().getUnorderedGroup_2(), 0) ) {
                    	        throw new FailedPredicateException(input, "ruleWEntity", "getUnorderedGroupHelper().canSelect(grammarAccess.getWEntityAccess().getUnorderedGroup_2(), 0)");
                    	    }
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1206:104: ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1207:6: ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) )
                    	    {
                    	     
                    	    	 				  getUnorderedGroupHelper().select(grammarAccess.getWEntityAccess().getUnorderedGroup_2(), 0);
                    	    	 				
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1210:6: ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1210:7: {...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? )
                    	    {
                    	    if ( !((true)) ) {
                    	        throw new FailedPredicateException(input, "ruleWEntity", "true");
                    	    }
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1210:16: (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1210:18: otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )?
                    	    {
                    	    otherlv_3=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleWEntity2472); 

                    	        	newLeafNode(otherlv_3, grammarAccess.getWEntityAccess().getLeftCurlyBracketKeyword_2_0_0());
                    	        
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1214:1: (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )?
                    	    int alt30=2;
                    	    int LA30_0 = input.LA(1);

                    	    if ( (LA30_0==14) ) {
                    	        alt30=1;
                    	    }
                    	    switch (alt30) {
                    	        case 1 :
                    	            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1214:3: otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) )
                    	            {
                    	            otherlv_4=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleWEntity2485); 

                    	                	newLeafNode(otherlv_4, grammarAccess.getWEntityAccess().getVersionKeyword_2_0_1_0());
                    	                
                    	            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1218:1: ( (lv_version_5_0= ruleEIntegerObject ) )
                    	            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1219:1: (lv_version_5_0= ruleEIntegerObject )
                    	            {
                    	            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1219:1: (lv_version_5_0= ruleEIntegerObject )
                    	            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1220:3: lv_version_5_0= ruleEIntegerObject
                    	            {
                    	             
                    	            	        newCompositeNode(grammarAccess.getWEntityAccess().getVersionEIntegerObjectParserRuleCall_2_0_1_1_0()); 
                    	            	    
                    	            pushFollow(FollowSets000.FOLLOW_ruleEIntegerObject_in_ruleWEntity2506);
                    	            lv_version_5_0=ruleEIntegerObject();

                    	            state._fsp--;


                    	            	        if (current==null) {
                    	            	            current = createModelElementForParent(grammarAccess.getWEntityRule());
                    	            	        }
                    	                   		set(
                    	                   			current, 
                    	                   			"version",
                    	                    		lv_version_5_0, 
                    	                    		"EIntegerObject");
                    	            	        afterParserOrEnumRuleCall();
                    	            	    

                    	            }


                    	            }


                    	            }
                    	            break;

                    	    }


                    	    }


                    	    }

                    	     
                    	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getWEntityAccess().getUnorderedGroup_2());
                    	    	 				

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 2 :
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1243:4: ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) )
                    	    {
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1243:4: ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1244:5: {...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) )
                    	    {
                    	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getWEntityAccess().getUnorderedGroup_2(), 1) ) {
                    	        throw new FailedPredicateException(input, "ruleWEntity", "getUnorderedGroupHelper().canSelect(grammarAccess.getWEntityAccess().getUnorderedGroup_2(), 1)");
                    	    }
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1244:104: ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1245:6: ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) )
                    	    {
                    	     
                    	    	 				  getUnorderedGroupHelper().select(grammarAccess.getWEntityAccess().getUnorderedGroup_2(), 1);
                    	    	 				
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1248:6: ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1248:7: {...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) )
                    	    {
                    	    if ( !((true)) ) {
                    	        throw new FailedPredicateException(input, "ruleWEntity", "true");
                    	    }
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1248:16: (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1248:18: otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) )
                    	    {
                    	    otherlv_6=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleWEntity2576); 

                    	        	newLeafNode(otherlv_6, grammarAccess.getWEntityAccess().getLabelKeyword_2_1_0());
                    	        
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1252:1: ( (lv_label_7_0= ruleEString ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1253:1: (lv_label_7_0= ruleEString )
                    	    {
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1253:1: (lv_label_7_0= ruleEString )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1254:3: lv_label_7_0= ruleEString
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getWEntityAccess().getLabelEStringParserRuleCall_2_1_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWEntity2597);
                    	    lv_label_7_0=ruleEString();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getWEntityRule());
                    	    	        }
                    	           		set(
                    	           			current, 
                    	           			"label",
                    	            		lv_label_7_0, 
                    	            		"EString");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }


                    	    }

                    	     
                    	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getWEntityAccess().getUnorderedGroup_2());
                    	    	 				

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 3 :
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1277:4: ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) )
                    	    {
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1277:4: ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1278:5: {...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) )
                    	    {
                    	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getWEntityAccess().getUnorderedGroup_2(), 2) ) {
                    	        throw new FailedPredicateException(input, "ruleWEntity", "getUnorderedGroupHelper().canSelect(grammarAccess.getWEntityAccess().getUnorderedGroup_2(), 2)");
                    	    }
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1278:104: ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1279:6: ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) )
                    	    {
                    	     
                    	    	 				  getUnorderedGroupHelper().select(grammarAccess.getWEntityAccess().getUnorderedGroup_2(), 2);
                    	    	 				
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1282:6: ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1282:7: {...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) )
                    	    {
                    	    if ( !((true)) ) {
                    	        throw new FailedPredicateException(input, "ruleWEntity", "true");
                    	    }
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1282:16: (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1282:18: otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) )
                    	    {
                    	    otherlv_8=(Token)match(input,27,FollowSets000.FOLLOW_27_in_ruleWEntity2665); 

                    	        	newLeafNode(otherlv_8, grammarAccess.getWEntityAccess().getGenerateKeyword_2_2_0());
                    	        
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1286:1: ( (lv_generate_9_0= ruleEBooleanObject ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1287:1: (lv_generate_9_0= ruleEBooleanObject )
                    	    {
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1287:1: (lv_generate_9_0= ruleEBooleanObject )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1288:3: lv_generate_9_0= ruleEBooleanObject
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getWEntityAccess().getGenerateEBooleanObjectParserRuleCall_2_2_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleEBooleanObject_in_ruleWEntity2686);
                    	    lv_generate_9_0=ruleEBooleanObject();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getWEntityRule());
                    	    	        }
                    	           		set(
                    	           			current, 
                    	           			"generate",
                    	            		lv_generate_9_0, 
                    	            		"EBooleanObject");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }


                    	    }

                    	     
                    	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getWEntityAccess().getUnorderedGroup_2());
                    	    	 				

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 4 :
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1311:4: ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) )
                    	    {
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1311:4: ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1312:5: {...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) )
                    	    {
                    	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getWEntityAccess().getUnorderedGroup_2(), 3) ) {
                    	        throw new FailedPredicateException(input, "ruleWEntity", "getUnorderedGroupHelper().canSelect(grammarAccess.getWEntityAccess().getUnorderedGroup_2(), 3)");
                    	    }
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1312:104: ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1313:6: ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) )
                    	    {
                    	     
                    	    	 				  getUnorderedGroupHelper().select(grammarAccess.getWEntityAccess().getUnorderedGroup_2(), 3);
                    	    	 				
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1316:6: ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1316:7: {...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' )
                    	    {
                    	    if ( !((true)) ) {
                    	        throw new FailedPredicateException(input, "ruleWEntity", "true");
                    	    }
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1316:16: ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1316:17: (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}'
                    	    {
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1316:17: (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1316:19: otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}'
                    	    {
                    	    otherlv_10=(Token)match(input,40,FollowSets000.FOLLOW_40_in_ruleWEntity2755); 

                    	        	newLeafNode(otherlv_10, grammarAccess.getWEntityAccess().getAttributesKeyword_2_3_0_0());
                    	        
                    	    otherlv_11=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleWEntity2767); 

                    	        	newLeafNode(otherlv_11, grammarAccess.getWEntityAccess().getLeftCurlyBracketKeyword_2_3_0_1());
                    	        
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1324:1: ( (lv_attributes_12_0= ruleWAttribute ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1325:1: (lv_attributes_12_0= ruleWAttribute )
                    	    {
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1325:1: (lv_attributes_12_0= ruleWAttribute )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1326:3: lv_attributes_12_0= ruleWAttribute
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getWEntityAccess().getAttributesWAttributeParserRuleCall_2_3_0_2_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleWAttribute_in_ruleWEntity2788);
                    	    lv_attributes_12_0=ruleWAttribute();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getWEntityRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"attributes",
                    	            		lv_attributes_12_0, 
                    	            		"WAttribute");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }

                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1342:2: (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )*
                    	    loop31:
                    	    do {
                    	        int alt31=2;
                    	        int LA31_0 = input.LA(1);

                    	        if ( (LA31_0==19) ) {
                    	            alt31=1;
                    	        }


                    	        switch (alt31) {
                    	    	case 1 :
                    	    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1342:4: otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) )
                    	    	    {
                    	    	    otherlv_13=(Token)match(input,19,FollowSets000.FOLLOW_19_in_ruleWEntity2801); 

                    	    	        	newLeafNode(otherlv_13, grammarAccess.getWEntityAccess().getCommaKeyword_2_3_0_3_0());
                    	    	        
                    	    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1346:1: ( (lv_attributes_14_0= ruleWAttribute ) )
                    	    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1347:1: (lv_attributes_14_0= ruleWAttribute )
                    	    	    {
                    	    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1347:1: (lv_attributes_14_0= ruleWAttribute )
                    	    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1348:3: lv_attributes_14_0= ruleWAttribute
                    	    	    {
                    	    	     
                    	    	    	        newCompositeNode(grammarAccess.getWEntityAccess().getAttributesWAttributeParserRuleCall_2_3_0_3_1_0()); 
                    	    	    	    
                    	    	    pushFollow(FollowSets000.FOLLOW_ruleWAttribute_in_ruleWEntity2822);
                    	    	    lv_attributes_14_0=ruleWAttribute();

                    	    	    state._fsp--;


                    	    	    	        if (current==null) {
                    	    	    	            current = createModelElementForParent(grammarAccess.getWEntityRule());
                    	    	    	        }
                    	    	           		add(
                    	    	           			current, 
                    	    	           			"attributes",
                    	    	            		lv_attributes_14_0, 
                    	    	            		"WAttribute");
                    	    	    	        afterParserOrEnumRuleCall();
                    	    	    	    

                    	    	    }


                    	    	    }


                    	    	    }
                    	    	    break;

                    	    	default :
                    	    	    break loop31;
                    	        }
                    	    } while (true);

                    	    otherlv_15=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleWEntity2836); 

                    	        	newLeafNode(otherlv_15, grammarAccess.getWEntityAccess().getRightCurlyBracketKeyword_2_3_0_4());
                    	        

                    	    }

                    	    otherlv_16=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleWEntity2849); 

                    	        	newLeafNode(otherlv_16, grammarAccess.getWEntityAccess().getRightCurlyBracketKeyword_2_3_1());
                    	        

                    	    }


                    	    }

                    	     
                    	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getWEntityAccess().getUnorderedGroup_2());
                    	    	 				

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt32 >= 1 ) break loop32;
                                EarlyExitException eee =
                                    new EarlyExitException(32, input);
                                throw eee;
                        }
                        cnt32++;
                    } while (true);

                    if ( ! getUnorderedGroupHelper().canLeave(grammarAccess.getWEntityAccess().getUnorderedGroup_2()) ) {
                        throw new FailedPredicateException(input, "ruleWEntity", "getUnorderedGroupHelper().canLeave(grammarAccess.getWEntityAccess().getUnorderedGroup_2())");
                    }

                    }


                    }

                     
                    	  getUnorderedGroupHelper().leave(grammarAccess.getWEntityAccess().getUnorderedGroup_2());
                    	

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWEntity"


    // $ANTLR start "entryRuleWLink"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1395:1: entryRuleWLink returns [EObject current=null] : iv_ruleWLink= ruleWLink EOF ;
    public final EObject entryRuleWLink() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWLink = null;


        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1396:2: (iv_ruleWLink= ruleWLink EOF )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1397:2: iv_ruleWLink= ruleWLink EOF
            {
             newCompositeNode(grammarAccess.getWLinkRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleWLink_in_entryRuleWLink2933);
            iv_ruleWLink=ruleWLink();

            state._fsp--;

             current =iv_ruleWLink; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleWLink2943); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWLink"


    // $ANTLR start "ruleWLink"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1404:1: ruleWLink returns [EObject current=null] : ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'from' ( (lv_from_10_0= ruleWConnection ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_11= 'to' ( (lv_to_12_0= ruleWConnection ) ) ) otherlv_13= '}' ) ) ) ) )+ {...}?) ) ) ;
    public final EObject ruleWLink() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_version_4_0 = null;

        AntlrDatatypeRuleToken lv_label_6_0 = null;

        AntlrDatatypeRuleToken lv_generate_8_0 = null;

        EObject lv_from_10_0 = null;

        EObject lv_to_12_0 = null;


         enterRule(); 
            
        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1407:28: ( ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'from' ( (lv_from_10_0= ruleWConnection ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_11= 'to' ( (lv_to_12_0= ruleWConnection ) ) ) otherlv_13= '}' ) ) ) ) )+ {...}?) ) ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1408:1: ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'from' ( (lv_from_10_0= ruleWConnection ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_11= 'to' ( (lv_to_12_0= ruleWConnection ) ) ) otherlv_13= '}' ) ) ) ) )+ {...}?) ) )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1408:1: ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'from' ( (lv_from_10_0= ruleWConnection ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_11= 'to' ( (lv_to_12_0= ruleWConnection ) ) ) otherlv_13= '}' ) ) ) ) )+ {...}?) ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1410:1: ( ( ( ({...}? => ( ({...}? => ( ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'from' ( (lv_from_10_0= ruleWConnection ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_11= 'to' ( (lv_to_12_0= ruleWConnection ) ) ) otherlv_13= '}' ) ) ) ) )+ {...}?) )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1410:1: ( ( ( ({...}? => ( ({...}? => ( ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'from' ( (lv_from_10_0= ruleWConnection ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_11= 'to' ( (lv_to_12_0= ruleWConnection ) ) ) otherlv_13= '}' ) ) ) ) )+ {...}?) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1411:2: ( ( ({...}? => ( ({...}? => ( ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'from' ( (lv_from_10_0= ruleWConnection ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_11= 'to' ( (lv_to_12_0= ruleWConnection ) ) ) otherlv_13= '}' ) ) ) ) )+ {...}?)
            {
             
            	  getUnorderedGroupHelper().enter(grammarAccess.getWLinkAccess().getUnorderedGroup());
            	
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1414:2: ( ( ({...}? => ( ({...}? => ( ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'from' ( (lv_from_10_0= ruleWConnection ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_11= 'to' ( (lv_to_12_0= ruleWConnection ) ) ) otherlv_13= '}' ) ) ) ) )+ {...}?)
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1415:3: ( ({...}? => ( ({...}? => ( ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'from' ( (lv_from_10_0= ruleWConnection ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_11= 'to' ( (lv_to_12_0= ruleWConnection ) ) ) otherlv_13= '}' ) ) ) ) )+ {...}?
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1415:3: ( ({...}? => ( ({...}? => ( ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'from' ( (lv_from_10_0= ruleWConnection ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_11= 'to' ( (lv_to_12_0= ruleWConnection ) ) ) otherlv_13= '}' ) ) ) ) )+
            int cnt36=0;
            loop36:
            do {
                int alt36=5;
                int LA36_0 = input.LA(1);

                if ( LA36_0 >=RULE_STRING && LA36_0<=RULE_ID && getUnorderedGroupHelper().canSelect(grammarAccess.getWLinkAccess().getUnorderedGroup(), 0) ) {
                    alt36=1;
                }
                else if ( LA36_0 ==27 && getUnorderedGroupHelper().canSelect(grammarAccess.getWLinkAccess().getUnorderedGroup(), 1) ) {
                    alt36=2;
                }
                else if ( LA36_0 ==41 && getUnorderedGroupHelper().canSelect(grammarAccess.getWLinkAccess().getUnorderedGroup(), 2) ) {
                    alt36=3;
                }
                else if ( LA36_0 ==42 && getUnorderedGroupHelper().canSelect(grammarAccess.getWLinkAccess().getUnorderedGroup(), 3) ) {
                    alt36=4;
                }


                switch (alt36) {
            	case 1 :
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1417:4: ({...}? => ( ({...}? => ( ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? ) ) ) )
            	    {
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1417:4: ({...}? => ( ({...}? => ( ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? ) ) ) )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1418:5: {...}? => ( ({...}? => ( ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getWLinkAccess().getUnorderedGroup(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleWLink", "getUnorderedGroupHelper().canSelect(grammarAccess.getWLinkAccess().getUnorderedGroup(), 0)");
            	    }
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1418:100: ( ({...}? => ( ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? ) ) )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1419:6: ({...}? => ( ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getWLinkAccess().getUnorderedGroup(), 0);
            	    	 				
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1422:6: ({...}? => ( ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? ) )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1422:7: {...}? => ( ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleWLink", "true");
            	    }
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1422:16: ( ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1422:17: ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )?
            	    {
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1422:17: ( (lv_name_1_0= ruleEString ) )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1423:1: (lv_name_1_0= ruleEString )
            	    {
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1423:1: (lv_name_1_0= ruleEString )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1424:3: lv_name_1_0= ruleEString
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getWLinkAccess().getNameEStringParserRuleCall_0_0_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWLink3034);
            	    lv_name_1_0=ruleEString();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getWLinkRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"name",
            	            		lv_name_1_0, 
            	            		"EString");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }

            	    otherlv_2=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleWLink3046); 

            	        	newLeafNode(otherlv_2, grammarAccess.getWLinkAccess().getLeftCurlyBracketKeyword_0_1());
            	        
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1444:1: (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )?
            	    int alt34=2;
            	    int LA34_0 = input.LA(1);

            	    if ( (LA34_0==14) ) {
            	        alt34=1;
            	    }
            	    switch (alt34) {
            	        case 1 :
            	            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1444:3: otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) )
            	            {
            	            otherlv_3=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleWLink3059); 

            	                	newLeafNode(otherlv_3, grammarAccess.getWLinkAccess().getVersionKeyword_0_2_0());
            	                
            	            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1448:1: ( (lv_version_4_0= ruleEIntegerObject ) )
            	            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1449:1: (lv_version_4_0= ruleEIntegerObject )
            	            {
            	            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1449:1: (lv_version_4_0= ruleEIntegerObject )
            	            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1450:3: lv_version_4_0= ruleEIntegerObject
            	            {
            	             
            	            	        newCompositeNode(grammarAccess.getWLinkAccess().getVersionEIntegerObjectParserRuleCall_0_2_1_0()); 
            	            	    
            	            pushFollow(FollowSets000.FOLLOW_ruleEIntegerObject_in_ruleWLink3080);
            	            lv_version_4_0=ruleEIntegerObject();

            	            state._fsp--;


            	            	        if (current==null) {
            	            	            current = createModelElementForParent(grammarAccess.getWLinkRule());
            	            	        }
            	                   		set(
            	                   			current, 
            	                   			"version",
            	                    		lv_version_4_0, 
            	                    		"EIntegerObject");
            	            	        afterParserOrEnumRuleCall();
            	            	    

            	            }


            	            }


            	            }
            	            break;

            	    }

            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1466:4: (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )?
            	    int alt35=2;
            	    int LA35_0 = input.LA(1);

            	    if ( (LA35_0==25) ) {
            	        alt35=1;
            	    }
            	    switch (alt35) {
            	        case 1 :
            	            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1466:6: otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) )
            	            {
            	            otherlv_5=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleWLink3095); 

            	                	newLeafNode(otherlv_5, grammarAccess.getWLinkAccess().getLabelKeyword_0_3_0());
            	                
            	            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1470:1: ( (lv_label_6_0= ruleEString ) )
            	            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1471:1: (lv_label_6_0= ruleEString )
            	            {
            	            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1471:1: (lv_label_6_0= ruleEString )
            	            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1472:3: lv_label_6_0= ruleEString
            	            {
            	             
            	            	        newCompositeNode(grammarAccess.getWLinkAccess().getLabelEStringParserRuleCall_0_3_1_0()); 
            	            	    
            	            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWLink3116);
            	            lv_label_6_0=ruleEString();

            	            state._fsp--;


            	            	        if (current==null) {
            	            	            current = createModelElementForParent(grammarAccess.getWLinkRule());
            	            	        }
            	                   		set(
            	                   			current, 
            	                   			"label",
            	                    		lv_label_6_0, 
            	                    		"EString");
            	            	        afterParserOrEnumRuleCall();
            	            	    

            	            }


            	            }


            	            }
            	            break;

            	    }


            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getWLinkAccess().getUnorderedGroup());
            	    	 				

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1495:4: ({...}? => ( ({...}? => (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) ) ) ) )
            	    {
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1495:4: ({...}? => ( ({...}? => (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) ) ) ) )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1496:5: {...}? => ( ({...}? => (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getWLinkAccess().getUnorderedGroup(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleWLink", "getUnorderedGroupHelper().canSelect(grammarAccess.getWLinkAccess().getUnorderedGroup(), 1)");
            	    }
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1496:100: ( ({...}? => (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) ) ) )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1497:6: ({...}? => (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getWLinkAccess().getUnorderedGroup(), 1);
            	    	 				
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1500:6: ({...}? => (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) ) )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1500:7: {...}? => (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleWLink", "true");
            	    }
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1500:16: (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1500:18: otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) )
            	    {
            	    otherlv_7=(Token)match(input,27,FollowSets000.FOLLOW_27_in_ruleWLink3186); 

            	        	newLeafNode(otherlv_7, grammarAccess.getWLinkAccess().getGenerateKeyword_1_0());
            	        
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1504:1: ( (lv_generate_8_0= ruleEBooleanObject ) )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1505:1: (lv_generate_8_0= ruleEBooleanObject )
            	    {
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1505:1: (lv_generate_8_0= ruleEBooleanObject )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1506:3: lv_generate_8_0= ruleEBooleanObject
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getWLinkAccess().getGenerateEBooleanObjectParserRuleCall_1_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleEBooleanObject_in_ruleWLink3207);
            	    lv_generate_8_0=ruleEBooleanObject();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getWLinkRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"generate",
            	            		lv_generate_8_0, 
            	            		"EBooleanObject");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getWLinkAccess().getUnorderedGroup());
            	    	 				

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1529:4: ({...}? => ( ({...}? => (otherlv_9= 'from' ( (lv_from_10_0= ruleWConnection ) ) ) ) ) )
            	    {
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1529:4: ({...}? => ( ({...}? => (otherlv_9= 'from' ( (lv_from_10_0= ruleWConnection ) ) ) ) ) )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1530:5: {...}? => ( ({...}? => (otherlv_9= 'from' ( (lv_from_10_0= ruleWConnection ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getWLinkAccess().getUnorderedGroup(), 2) ) {
            	        throw new FailedPredicateException(input, "ruleWLink", "getUnorderedGroupHelper().canSelect(grammarAccess.getWLinkAccess().getUnorderedGroup(), 2)");
            	    }
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1530:100: ( ({...}? => (otherlv_9= 'from' ( (lv_from_10_0= ruleWConnection ) ) ) ) )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1531:6: ({...}? => (otherlv_9= 'from' ( (lv_from_10_0= ruleWConnection ) ) ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getWLinkAccess().getUnorderedGroup(), 2);
            	    	 				
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1534:6: ({...}? => (otherlv_9= 'from' ( (lv_from_10_0= ruleWConnection ) ) ) )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1534:7: {...}? => (otherlv_9= 'from' ( (lv_from_10_0= ruleWConnection ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleWLink", "true");
            	    }
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1534:16: (otherlv_9= 'from' ( (lv_from_10_0= ruleWConnection ) ) )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1534:18: otherlv_9= 'from' ( (lv_from_10_0= ruleWConnection ) )
            	    {
            	    otherlv_9=(Token)match(input,41,FollowSets000.FOLLOW_41_in_ruleWLink3275); 

            	        	newLeafNode(otherlv_9, grammarAccess.getWLinkAccess().getFromKeyword_2_0());
            	        
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1538:1: ( (lv_from_10_0= ruleWConnection ) )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1539:1: (lv_from_10_0= ruleWConnection )
            	    {
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1539:1: (lv_from_10_0= ruleWConnection )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1540:3: lv_from_10_0= ruleWConnection
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getWLinkAccess().getFromWConnectionParserRuleCall_2_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleWConnection_in_ruleWLink3296);
            	    lv_from_10_0=ruleWConnection();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getWLinkRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"from",
            	            		lv_from_10_0, 
            	            		"WConnection");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getWLinkAccess().getUnorderedGroup());
            	    	 				

            	    }


            	    }


            	    }
            	    break;
            	case 4 :
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1563:4: ({...}? => ( ({...}? => ( (otherlv_11= 'to' ( (lv_to_12_0= ruleWConnection ) ) ) otherlv_13= '}' ) ) ) )
            	    {
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1563:4: ({...}? => ( ({...}? => ( (otherlv_11= 'to' ( (lv_to_12_0= ruleWConnection ) ) ) otherlv_13= '}' ) ) ) )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1564:5: {...}? => ( ({...}? => ( (otherlv_11= 'to' ( (lv_to_12_0= ruleWConnection ) ) ) otherlv_13= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getWLinkAccess().getUnorderedGroup(), 3) ) {
            	        throw new FailedPredicateException(input, "ruleWLink", "getUnorderedGroupHelper().canSelect(grammarAccess.getWLinkAccess().getUnorderedGroup(), 3)");
            	    }
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1564:100: ( ({...}? => ( (otherlv_11= 'to' ( (lv_to_12_0= ruleWConnection ) ) ) otherlv_13= '}' ) ) )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1565:6: ({...}? => ( (otherlv_11= 'to' ( (lv_to_12_0= ruleWConnection ) ) ) otherlv_13= '}' ) )
            	    {
            	     
            	    	 				  getUnorderedGroupHelper().select(grammarAccess.getWLinkAccess().getUnorderedGroup(), 3);
            	    	 				
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1568:6: ({...}? => ( (otherlv_11= 'to' ( (lv_to_12_0= ruleWConnection ) ) ) otherlv_13= '}' ) )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1568:7: {...}? => ( (otherlv_11= 'to' ( (lv_to_12_0= ruleWConnection ) ) ) otherlv_13= '}' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleWLink", "true");
            	    }
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1568:16: ( (otherlv_11= 'to' ( (lv_to_12_0= ruleWConnection ) ) ) otherlv_13= '}' )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1568:17: (otherlv_11= 'to' ( (lv_to_12_0= ruleWConnection ) ) ) otherlv_13= '}'
            	    {
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1568:17: (otherlv_11= 'to' ( (lv_to_12_0= ruleWConnection ) ) )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1568:19: otherlv_11= 'to' ( (lv_to_12_0= ruleWConnection ) )
            	    {
            	    otherlv_11=(Token)match(input,42,FollowSets000.FOLLOW_42_in_ruleWLink3365); 

            	        	newLeafNode(otherlv_11, grammarAccess.getWLinkAccess().getToKeyword_3_0_0());
            	        
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1572:1: ( (lv_to_12_0= ruleWConnection ) )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1573:1: (lv_to_12_0= ruleWConnection )
            	    {
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1573:1: (lv_to_12_0= ruleWConnection )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1574:3: lv_to_12_0= ruleWConnection
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getWLinkAccess().getToWConnectionParserRuleCall_3_0_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleWConnection_in_ruleWLink3386);
            	    lv_to_12_0=ruleWConnection();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getWLinkRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"to",
            	            		lv_to_12_0, 
            	            		"WConnection");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }

            	    otherlv_13=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleWLink3399); 

            	        	newLeafNode(otherlv_13, grammarAccess.getWLinkAccess().getRightCurlyBracketKeyword_3_1());
            	        

            	    }


            	    }

            	     
            	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getWLinkAccess().getUnorderedGroup());
            	    	 				

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt36 >= 1 ) break loop36;
                        EarlyExitException eee =
                            new EarlyExitException(36, input);
                        throw eee;
                }
                cnt36++;
            } while (true);

            if ( ! getUnorderedGroupHelper().canLeave(grammarAccess.getWLinkAccess().getUnorderedGroup()) ) {
                throw new FailedPredicateException(input, "ruleWLink", "getUnorderedGroupHelper().canLeave(grammarAccess.getWLinkAccess().getUnorderedGroup())");
            }

            }


            }

             
            	  getUnorderedGroupHelper().leave(grammarAccess.getWLinkAccess().getUnorderedGroup());
            	

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWLink"


    // $ANTLR start "entryRuleWAttribute"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1617:1: entryRuleWAttribute returns [EObject current=null] : iv_ruleWAttribute= ruleWAttribute EOF ;
    public final EObject entryRuleWAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWAttribute = null;


        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1618:2: (iv_ruleWAttribute= ruleWAttribute EOF )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1619:2: iv_ruleWAttribute= ruleWAttribute EOF
            {
             newCompositeNode(grammarAccess.getWAttributeRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleWAttribute_in_entryRuleWAttribute3481);
            iv_ruleWAttribute=ruleWAttribute();

            state._fsp--;

             current =iv_ruleWAttribute; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleWAttribute3491); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWAttribute"


    // $ANTLR start "ruleWAttribute"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1626:1: ruleWAttribute returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) )? (otherlv_9= 'isPublic' ( (lv_isPublic_10_0= ruleEBooleanObject ) ) )? (otherlv_11= 'placeholder' ( (lv_placeholder_12_0= ruleEString ) ) )? (otherlv_13= 'important' ( (lv_important_14_0= ruleEBooleanObject ) ) )? (otherlv_15= 'validationRegex' ( (lv_validationRegex_16_0= ruleEString ) ) )? (otherlv_17= 'validationMessage' ( (lv_validationMessage_18_0= ruleEString ) ) )? (otherlv_19= 'allowNull' ( (lv_allowNull_20_0= ruleEBooleanObject ) ) )? (otherlv_21= 'type' ( (lv_type_22_0= ruleEType ) ) )? (otherlv_23= 'length' ( (lv_length_24_0= ruleEIntegerObject ) ) )? (otherlv_25= 'enumeration' ( ( ruleEString ) ) )? (otherlv_27= 'disabled' ( (lv_disabled_28_0= ruleEBooleanObject ) ) )? (otherlv_29= 'message' ( (lv_message_30_0= ruleEString ) ) )? (otherlv_31= 'messageDev' ( (lv_messageDev_32_0= ruleEString ) ) )? otherlv_33= '}' )? ) ;
    public final EObject ruleWAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_23=null;
        Token otherlv_25=null;
        Token otherlv_27=null;
        Token otherlv_29=null;
        Token otherlv_31=null;
        Token otherlv_33=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_version_4_0 = null;

        AntlrDatatypeRuleToken lv_label_6_0 = null;

        AntlrDatatypeRuleToken lv_generate_8_0 = null;

        AntlrDatatypeRuleToken lv_isPublic_10_0 = null;

        AntlrDatatypeRuleToken lv_placeholder_12_0 = null;

        AntlrDatatypeRuleToken lv_important_14_0 = null;

        AntlrDatatypeRuleToken lv_validationRegex_16_0 = null;

        AntlrDatatypeRuleToken lv_validationMessage_18_0 = null;

        AntlrDatatypeRuleToken lv_allowNull_20_0 = null;

        Enumerator lv_type_22_0 = null;

        AntlrDatatypeRuleToken lv_length_24_0 = null;

        AntlrDatatypeRuleToken lv_disabled_28_0 = null;

        AntlrDatatypeRuleToken lv_message_30_0 = null;

        AntlrDatatypeRuleToken lv_messageDev_32_0 = null;


         enterRule(); 
            
        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1629:28: ( ( () ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) )? (otherlv_9= 'isPublic' ( (lv_isPublic_10_0= ruleEBooleanObject ) ) )? (otherlv_11= 'placeholder' ( (lv_placeholder_12_0= ruleEString ) ) )? (otherlv_13= 'important' ( (lv_important_14_0= ruleEBooleanObject ) ) )? (otherlv_15= 'validationRegex' ( (lv_validationRegex_16_0= ruleEString ) ) )? (otherlv_17= 'validationMessage' ( (lv_validationMessage_18_0= ruleEString ) ) )? (otherlv_19= 'allowNull' ( (lv_allowNull_20_0= ruleEBooleanObject ) ) )? (otherlv_21= 'type' ( (lv_type_22_0= ruleEType ) ) )? (otherlv_23= 'length' ( (lv_length_24_0= ruleEIntegerObject ) ) )? (otherlv_25= 'enumeration' ( ( ruleEString ) ) )? (otherlv_27= 'disabled' ( (lv_disabled_28_0= ruleEBooleanObject ) ) )? (otherlv_29= 'message' ( (lv_message_30_0= ruleEString ) ) )? (otherlv_31= 'messageDev' ( (lv_messageDev_32_0= ruleEString ) ) )? otherlv_33= '}' )? ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1630:1: ( () ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) )? (otherlv_9= 'isPublic' ( (lv_isPublic_10_0= ruleEBooleanObject ) ) )? (otherlv_11= 'placeholder' ( (lv_placeholder_12_0= ruleEString ) ) )? (otherlv_13= 'important' ( (lv_important_14_0= ruleEBooleanObject ) ) )? (otherlv_15= 'validationRegex' ( (lv_validationRegex_16_0= ruleEString ) ) )? (otherlv_17= 'validationMessage' ( (lv_validationMessage_18_0= ruleEString ) ) )? (otherlv_19= 'allowNull' ( (lv_allowNull_20_0= ruleEBooleanObject ) ) )? (otherlv_21= 'type' ( (lv_type_22_0= ruleEType ) ) )? (otherlv_23= 'length' ( (lv_length_24_0= ruleEIntegerObject ) ) )? (otherlv_25= 'enumeration' ( ( ruleEString ) ) )? (otherlv_27= 'disabled' ( (lv_disabled_28_0= ruleEBooleanObject ) ) )? (otherlv_29= 'message' ( (lv_message_30_0= ruleEString ) ) )? (otherlv_31= 'messageDev' ( (lv_messageDev_32_0= ruleEString ) ) )? otherlv_33= '}' )? )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1630:1: ( () ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) )? (otherlv_9= 'isPublic' ( (lv_isPublic_10_0= ruleEBooleanObject ) ) )? (otherlv_11= 'placeholder' ( (lv_placeholder_12_0= ruleEString ) ) )? (otherlv_13= 'important' ( (lv_important_14_0= ruleEBooleanObject ) ) )? (otherlv_15= 'validationRegex' ( (lv_validationRegex_16_0= ruleEString ) ) )? (otherlv_17= 'validationMessage' ( (lv_validationMessage_18_0= ruleEString ) ) )? (otherlv_19= 'allowNull' ( (lv_allowNull_20_0= ruleEBooleanObject ) ) )? (otherlv_21= 'type' ( (lv_type_22_0= ruleEType ) ) )? (otherlv_23= 'length' ( (lv_length_24_0= ruleEIntegerObject ) ) )? (otherlv_25= 'enumeration' ( ( ruleEString ) ) )? (otherlv_27= 'disabled' ( (lv_disabled_28_0= ruleEBooleanObject ) ) )? (otherlv_29= 'message' ( (lv_message_30_0= ruleEString ) ) )? (otherlv_31= 'messageDev' ( (lv_messageDev_32_0= ruleEString ) ) )? otherlv_33= '}' )? )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1630:2: () ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) )? (otherlv_9= 'isPublic' ( (lv_isPublic_10_0= ruleEBooleanObject ) ) )? (otherlv_11= 'placeholder' ( (lv_placeholder_12_0= ruleEString ) ) )? (otherlv_13= 'important' ( (lv_important_14_0= ruleEBooleanObject ) ) )? (otherlv_15= 'validationRegex' ( (lv_validationRegex_16_0= ruleEString ) ) )? (otherlv_17= 'validationMessage' ( (lv_validationMessage_18_0= ruleEString ) ) )? (otherlv_19= 'allowNull' ( (lv_allowNull_20_0= ruleEBooleanObject ) ) )? (otherlv_21= 'type' ( (lv_type_22_0= ruleEType ) ) )? (otherlv_23= 'length' ( (lv_length_24_0= ruleEIntegerObject ) ) )? (otherlv_25= 'enumeration' ( ( ruleEString ) ) )? (otherlv_27= 'disabled' ( (lv_disabled_28_0= ruleEBooleanObject ) ) )? (otherlv_29= 'message' ( (lv_message_30_0= ruleEString ) ) )? (otherlv_31= 'messageDev' ( (lv_messageDev_32_0= ruleEString ) ) )? otherlv_33= '}' )?
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1630:2: ()
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1631:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getWAttributeAccess().getWAttributeAction_0(),
                        current);
                

            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1636:2: ( (lv_name_1_0= ruleEString ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1637:1: (lv_name_1_0= ruleEString )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1637:1: (lv_name_1_0= ruleEString )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1638:3: lv_name_1_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getWAttributeAccess().getNameEStringParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWAttribute3546);
            lv_name_1_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getWAttributeRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1654:2: (otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) )? (otherlv_9= 'isPublic' ( (lv_isPublic_10_0= ruleEBooleanObject ) ) )? (otherlv_11= 'placeholder' ( (lv_placeholder_12_0= ruleEString ) ) )? (otherlv_13= 'important' ( (lv_important_14_0= ruleEBooleanObject ) ) )? (otherlv_15= 'validationRegex' ( (lv_validationRegex_16_0= ruleEString ) ) )? (otherlv_17= 'validationMessage' ( (lv_validationMessage_18_0= ruleEString ) ) )? (otherlv_19= 'allowNull' ( (lv_allowNull_20_0= ruleEBooleanObject ) ) )? (otherlv_21= 'type' ( (lv_type_22_0= ruleEType ) ) )? (otherlv_23= 'length' ( (lv_length_24_0= ruleEIntegerObject ) ) )? (otherlv_25= 'enumeration' ( ( ruleEString ) ) )? (otherlv_27= 'disabled' ( (lv_disabled_28_0= ruleEBooleanObject ) ) )? (otherlv_29= 'message' ( (lv_message_30_0= ruleEString ) ) )? (otherlv_31= 'messageDev' ( (lv_messageDev_32_0= ruleEString ) ) )? otherlv_33= '}' )?
            int alt52=2;
            int LA52_0 = input.LA(1);

            if ( (LA52_0==12) ) {
                alt52=1;
            }
            switch (alt52) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1654:4: otherlv_2= '{' (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) )? (otherlv_9= 'isPublic' ( (lv_isPublic_10_0= ruleEBooleanObject ) ) )? (otherlv_11= 'placeholder' ( (lv_placeholder_12_0= ruleEString ) ) )? (otherlv_13= 'important' ( (lv_important_14_0= ruleEBooleanObject ) ) )? (otherlv_15= 'validationRegex' ( (lv_validationRegex_16_0= ruleEString ) ) )? (otherlv_17= 'validationMessage' ( (lv_validationMessage_18_0= ruleEString ) ) )? (otherlv_19= 'allowNull' ( (lv_allowNull_20_0= ruleEBooleanObject ) ) )? (otherlv_21= 'type' ( (lv_type_22_0= ruleEType ) ) )? (otherlv_23= 'length' ( (lv_length_24_0= ruleEIntegerObject ) ) )? (otherlv_25= 'enumeration' ( ( ruleEString ) ) )? (otherlv_27= 'disabled' ( (lv_disabled_28_0= ruleEBooleanObject ) ) )? (otherlv_29= 'message' ( (lv_message_30_0= ruleEString ) ) )? (otherlv_31= 'messageDev' ( (lv_messageDev_32_0= ruleEString ) ) )? otherlv_33= '}'
                    {
                    otherlv_2=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleWAttribute3559); 

                        	newLeafNode(otherlv_2, grammarAccess.getWAttributeAccess().getLeftCurlyBracketKeyword_2_0());
                        
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1658:1: (otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) ) )?
                    int alt37=2;
                    int LA37_0 = input.LA(1);

                    if ( (LA37_0==14) ) {
                        alt37=1;
                    }
                    switch (alt37) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1658:3: otherlv_3= 'version' ( (lv_version_4_0= ruleEIntegerObject ) )
                            {
                            otherlv_3=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleWAttribute3572); 

                                	newLeafNode(otherlv_3, grammarAccess.getWAttributeAccess().getVersionKeyword_2_1_0());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1662:1: ( (lv_version_4_0= ruleEIntegerObject ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1663:1: (lv_version_4_0= ruleEIntegerObject )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1663:1: (lv_version_4_0= ruleEIntegerObject )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1664:3: lv_version_4_0= ruleEIntegerObject
                            {
                             
                            	        newCompositeNode(grammarAccess.getWAttributeAccess().getVersionEIntegerObjectParserRuleCall_2_1_1_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleEIntegerObject_in_ruleWAttribute3593);
                            lv_version_4_0=ruleEIntegerObject();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWAttributeRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"version",
                                    		lv_version_4_0, 
                                    		"EIntegerObject");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1680:4: (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )?
                    int alt38=2;
                    int LA38_0 = input.LA(1);

                    if ( (LA38_0==25) ) {
                        alt38=1;
                    }
                    switch (alt38) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1680:6: otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) )
                            {
                            otherlv_5=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleWAttribute3608); 

                                	newLeafNode(otherlv_5, grammarAccess.getWAttributeAccess().getLabelKeyword_2_2_0());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1684:1: ( (lv_label_6_0= ruleEString ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1685:1: (lv_label_6_0= ruleEString )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1685:1: (lv_label_6_0= ruleEString )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1686:3: lv_label_6_0= ruleEString
                            {
                             
                            	        newCompositeNode(grammarAccess.getWAttributeAccess().getLabelEStringParserRuleCall_2_2_1_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWAttribute3629);
                            lv_label_6_0=ruleEString();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWAttributeRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"label",
                                    		lv_label_6_0, 
                                    		"EString");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1702:4: (otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) ) )?
                    int alt39=2;
                    int LA39_0 = input.LA(1);

                    if ( (LA39_0==27) ) {
                        alt39=1;
                    }
                    switch (alt39) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1702:6: otherlv_7= 'generate' ( (lv_generate_8_0= ruleEBooleanObject ) )
                            {
                            otherlv_7=(Token)match(input,27,FollowSets000.FOLLOW_27_in_ruleWAttribute3644); 

                                	newLeafNode(otherlv_7, grammarAccess.getWAttributeAccess().getGenerateKeyword_2_3_0());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1706:1: ( (lv_generate_8_0= ruleEBooleanObject ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1707:1: (lv_generate_8_0= ruleEBooleanObject )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1707:1: (lv_generate_8_0= ruleEBooleanObject )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1708:3: lv_generate_8_0= ruleEBooleanObject
                            {
                             
                            	        newCompositeNode(grammarAccess.getWAttributeAccess().getGenerateEBooleanObjectParserRuleCall_2_3_1_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleEBooleanObject_in_ruleWAttribute3665);
                            lv_generate_8_0=ruleEBooleanObject();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWAttributeRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"generate",
                                    		lv_generate_8_0, 
                                    		"EBooleanObject");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1724:4: (otherlv_9= 'isPublic' ( (lv_isPublic_10_0= ruleEBooleanObject ) ) )?
                    int alt40=2;
                    int LA40_0 = input.LA(1);

                    if ( (LA40_0==43) ) {
                        alt40=1;
                    }
                    switch (alt40) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1724:6: otherlv_9= 'isPublic' ( (lv_isPublic_10_0= ruleEBooleanObject ) )
                            {
                            otherlv_9=(Token)match(input,43,FollowSets000.FOLLOW_43_in_ruleWAttribute3680); 

                                	newLeafNode(otherlv_9, grammarAccess.getWAttributeAccess().getIsPublicKeyword_2_4_0());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1728:1: ( (lv_isPublic_10_0= ruleEBooleanObject ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1729:1: (lv_isPublic_10_0= ruleEBooleanObject )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1729:1: (lv_isPublic_10_0= ruleEBooleanObject )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1730:3: lv_isPublic_10_0= ruleEBooleanObject
                            {
                             
                            	        newCompositeNode(grammarAccess.getWAttributeAccess().getIsPublicEBooleanObjectParserRuleCall_2_4_1_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleEBooleanObject_in_ruleWAttribute3701);
                            lv_isPublic_10_0=ruleEBooleanObject();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWAttributeRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"isPublic",
                                    		lv_isPublic_10_0, 
                                    		"EBooleanObject");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1746:4: (otherlv_11= 'placeholder' ( (lv_placeholder_12_0= ruleEString ) ) )?
                    int alt41=2;
                    int LA41_0 = input.LA(1);

                    if ( (LA41_0==44) ) {
                        alt41=1;
                    }
                    switch (alt41) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1746:6: otherlv_11= 'placeholder' ( (lv_placeholder_12_0= ruleEString ) )
                            {
                            otherlv_11=(Token)match(input,44,FollowSets000.FOLLOW_44_in_ruleWAttribute3716); 

                                	newLeafNode(otherlv_11, grammarAccess.getWAttributeAccess().getPlaceholderKeyword_2_5_0());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1750:1: ( (lv_placeholder_12_0= ruleEString ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1751:1: (lv_placeholder_12_0= ruleEString )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1751:1: (lv_placeholder_12_0= ruleEString )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1752:3: lv_placeholder_12_0= ruleEString
                            {
                             
                            	        newCompositeNode(grammarAccess.getWAttributeAccess().getPlaceholderEStringParserRuleCall_2_5_1_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWAttribute3737);
                            lv_placeholder_12_0=ruleEString();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWAttributeRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"placeholder",
                                    		lv_placeholder_12_0, 
                                    		"EString");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1768:4: (otherlv_13= 'important' ( (lv_important_14_0= ruleEBooleanObject ) ) )?
                    int alt42=2;
                    int LA42_0 = input.LA(1);

                    if ( (LA42_0==45) ) {
                        alt42=1;
                    }
                    switch (alt42) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1768:6: otherlv_13= 'important' ( (lv_important_14_0= ruleEBooleanObject ) )
                            {
                            otherlv_13=(Token)match(input,45,FollowSets000.FOLLOW_45_in_ruleWAttribute3752); 

                                	newLeafNode(otherlv_13, grammarAccess.getWAttributeAccess().getImportantKeyword_2_6_0());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1772:1: ( (lv_important_14_0= ruleEBooleanObject ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1773:1: (lv_important_14_0= ruleEBooleanObject )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1773:1: (lv_important_14_0= ruleEBooleanObject )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1774:3: lv_important_14_0= ruleEBooleanObject
                            {
                             
                            	        newCompositeNode(grammarAccess.getWAttributeAccess().getImportantEBooleanObjectParserRuleCall_2_6_1_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleEBooleanObject_in_ruleWAttribute3773);
                            lv_important_14_0=ruleEBooleanObject();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWAttributeRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"important",
                                    		lv_important_14_0, 
                                    		"EBooleanObject");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1790:4: (otherlv_15= 'validationRegex' ( (lv_validationRegex_16_0= ruleEString ) ) )?
                    int alt43=2;
                    int LA43_0 = input.LA(1);

                    if ( (LA43_0==46) ) {
                        alt43=1;
                    }
                    switch (alt43) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1790:6: otherlv_15= 'validationRegex' ( (lv_validationRegex_16_0= ruleEString ) )
                            {
                            otherlv_15=(Token)match(input,46,FollowSets000.FOLLOW_46_in_ruleWAttribute3788); 

                                	newLeafNode(otherlv_15, grammarAccess.getWAttributeAccess().getValidationRegexKeyword_2_7_0());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1794:1: ( (lv_validationRegex_16_0= ruleEString ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1795:1: (lv_validationRegex_16_0= ruleEString )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1795:1: (lv_validationRegex_16_0= ruleEString )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1796:3: lv_validationRegex_16_0= ruleEString
                            {
                             
                            	        newCompositeNode(grammarAccess.getWAttributeAccess().getValidationRegexEStringParserRuleCall_2_7_1_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWAttribute3809);
                            lv_validationRegex_16_0=ruleEString();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWAttributeRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"validationRegex",
                                    		lv_validationRegex_16_0, 
                                    		"EString");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1812:4: (otherlv_17= 'validationMessage' ( (lv_validationMessage_18_0= ruleEString ) ) )?
                    int alt44=2;
                    int LA44_0 = input.LA(1);

                    if ( (LA44_0==47) ) {
                        alt44=1;
                    }
                    switch (alt44) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1812:6: otherlv_17= 'validationMessage' ( (lv_validationMessage_18_0= ruleEString ) )
                            {
                            otherlv_17=(Token)match(input,47,FollowSets000.FOLLOW_47_in_ruleWAttribute3824); 

                                	newLeafNode(otherlv_17, grammarAccess.getWAttributeAccess().getValidationMessageKeyword_2_8_0());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1816:1: ( (lv_validationMessage_18_0= ruleEString ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1817:1: (lv_validationMessage_18_0= ruleEString )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1817:1: (lv_validationMessage_18_0= ruleEString )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1818:3: lv_validationMessage_18_0= ruleEString
                            {
                             
                            	        newCompositeNode(grammarAccess.getWAttributeAccess().getValidationMessageEStringParserRuleCall_2_8_1_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWAttribute3845);
                            lv_validationMessage_18_0=ruleEString();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWAttributeRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"validationMessage",
                                    		lv_validationMessage_18_0, 
                                    		"EString");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1834:4: (otherlv_19= 'allowNull' ( (lv_allowNull_20_0= ruleEBooleanObject ) ) )?
                    int alt45=2;
                    int LA45_0 = input.LA(1);

                    if ( (LA45_0==48) ) {
                        alt45=1;
                    }
                    switch (alt45) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1834:6: otherlv_19= 'allowNull' ( (lv_allowNull_20_0= ruleEBooleanObject ) )
                            {
                            otherlv_19=(Token)match(input,48,FollowSets000.FOLLOW_48_in_ruleWAttribute3860); 

                                	newLeafNode(otherlv_19, grammarAccess.getWAttributeAccess().getAllowNullKeyword_2_9_0());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1838:1: ( (lv_allowNull_20_0= ruleEBooleanObject ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1839:1: (lv_allowNull_20_0= ruleEBooleanObject )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1839:1: (lv_allowNull_20_0= ruleEBooleanObject )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1840:3: lv_allowNull_20_0= ruleEBooleanObject
                            {
                             
                            	        newCompositeNode(grammarAccess.getWAttributeAccess().getAllowNullEBooleanObjectParserRuleCall_2_9_1_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleEBooleanObject_in_ruleWAttribute3881);
                            lv_allowNull_20_0=ruleEBooleanObject();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWAttributeRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"allowNull",
                                    		lv_allowNull_20_0, 
                                    		"EBooleanObject");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1856:4: (otherlv_21= 'type' ( (lv_type_22_0= ruleEType ) ) )?
                    int alt46=2;
                    int LA46_0 = input.LA(1);

                    if ( (LA46_0==32) ) {
                        alt46=1;
                    }
                    switch (alt46) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1856:6: otherlv_21= 'type' ( (lv_type_22_0= ruleEType ) )
                            {
                            otherlv_21=(Token)match(input,32,FollowSets000.FOLLOW_32_in_ruleWAttribute3896); 

                                	newLeafNode(otherlv_21, grammarAccess.getWAttributeAccess().getTypeKeyword_2_10_0());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1860:1: ( (lv_type_22_0= ruleEType ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1861:1: (lv_type_22_0= ruleEType )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1861:1: (lv_type_22_0= ruleEType )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1862:3: lv_type_22_0= ruleEType
                            {
                             
                            	        newCompositeNode(grammarAccess.getWAttributeAccess().getTypeETypeEnumRuleCall_2_10_1_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleEType_in_ruleWAttribute3917);
                            lv_type_22_0=ruleEType();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWAttributeRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"type",
                                    		lv_type_22_0, 
                                    		"EType");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1878:4: (otherlv_23= 'length' ( (lv_length_24_0= ruleEIntegerObject ) ) )?
                    int alt47=2;
                    int LA47_0 = input.LA(1);

                    if ( (LA47_0==49) ) {
                        alt47=1;
                    }
                    switch (alt47) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1878:6: otherlv_23= 'length' ( (lv_length_24_0= ruleEIntegerObject ) )
                            {
                            otherlv_23=(Token)match(input,49,FollowSets000.FOLLOW_49_in_ruleWAttribute3932); 

                                	newLeafNode(otherlv_23, grammarAccess.getWAttributeAccess().getLengthKeyword_2_11_0());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1882:1: ( (lv_length_24_0= ruleEIntegerObject ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1883:1: (lv_length_24_0= ruleEIntegerObject )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1883:1: (lv_length_24_0= ruleEIntegerObject )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1884:3: lv_length_24_0= ruleEIntegerObject
                            {
                             
                            	        newCompositeNode(grammarAccess.getWAttributeAccess().getLengthEIntegerObjectParserRuleCall_2_11_1_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleEIntegerObject_in_ruleWAttribute3953);
                            lv_length_24_0=ruleEIntegerObject();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWAttributeRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"length",
                                    		lv_length_24_0, 
                                    		"EIntegerObject");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1900:4: (otherlv_25= 'enumeration' ( ( ruleEString ) ) )?
                    int alt48=2;
                    int LA48_0 = input.LA(1);

                    if ( (LA48_0==50) ) {
                        alt48=1;
                    }
                    switch (alt48) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1900:6: otherlv_25= 'enumeration' ( ( ruleEString ) )
                            {
                            otherlv_25=(Token)match(input,50,FollowSets000.FOLLOW_50_in_ruleWAttribute3968); 

                                	newLeafNode(otherlv_25, grammarAccess.getWAttributeAccess().getEnumerationKeyword_2_12_0());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1904:1: ( ( ruleEString ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1905:1: ( ruleEString )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1905:1: ( ruleEString )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1906:3: ruleEString
                            {

                            			if (current==null) {
                            	            current = createModelElement(grammarAccess.getWAttributeRule());
                            	        }
                                    
                             
                            	        newCompositeNode(grammarAccess.getWAttributeAccess().getEnumerationWEnumCrossReference_2_12_1_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWAttribute3991);
                            ruleEString();

                            state._fsp--;

                             
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1919:4: (otherlv_27= 'disabled' ( (lv_disabled_28_0= ruleEBooleanObject ) ) )?
                    int alt49=2;
                    int LA49_0 = input.LA(1);

                    if ( (LA49_0==51) ) {
                        alt49=1;
                    }
                    switch (alt49) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1919:6: otherlv_27= 'disabled' ( (lv_disabled_28_0= ruleEBooleanObject ) )
                            {
                            otherlv_27=(Token)match(input,51,FollowSets000.FOLLOW_51_in_ruleWAttribute4006); 

                                	newLeafNode(otherlv_27, grammarAccess.getWAttributeAccess().getDisabledKeyword_2_13_0());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1923:1: ( (lv_disabled_28_0= ruleEBooleanObject ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1924:1: (lv_disabled_28_0= ruleEBooleanObject )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1924:1: (lv_disabled_28_0= ruleEBooleanObject )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1925:3: lv_disabled_28_0= ruleEBooleanObject
                            {
                             
                            	        newCompositeNode(grammarAccess.getWAttributeAccess().getDisabledEBooleanObjectParserRuleCall_2_13_1_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleEBooleanObject_in_ruleWAttribute4027);
                            lv_disabled_28_0=ruleEBooleanObject();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWAttributeRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"disabled",
                                    		lv_disabled_28_0, 
                                    		"EBooleanObject");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1941:4: (otherlv_29= 'message' ( (lv_message_30_0= ruleEString ) ) )?
                    int alt50=2;
                    int LA50_0 = input.LA(1);

                    if ( (LA50_0==29) ) {
                        alt50=1;
                    }
                    switch (alt50) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1941:6: otherlv_29= 'message' ( (lv_message_30_0= ruleEString ) )
                            {
                            otherlv_29=(Token)match(input,29,FollowSets000.FOLLOW_29_in_ruleWAttribute4042); 

                                	newLeafNode(otherlv_29, grammarAccess.getWAttributeAccess().getMessageKeyword_2_14_0());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1945:1: ( (lv_message_30_0= ruleEString ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1946:1: (lv_message_30_0= ruleEString )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1946:1: (lv_message_30_0= ruleEString )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1947:3: lv_message_30_0= ruleEString
                            {
                             
                            	        newCompositeNode(grammarAccess.getWAttributeAccess().getMessageEStringParserRuleCall_2_14_1_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWAttribute4063);
                            lv_message_30_0=ruleEString();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWAttributeRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"message",
                                    		lv_message_30_0, 
                                    		"EString");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1963:4: (otherlv_31= 'messageDev' ( (lv_messageDev_32_0= ruleEString ) ) )?
                    int alt51=2;
                    int LA51_0 = input.LA(1);

                    if ( (LA51_0==30) ) {
                        alt51=1;
                    }
                    switch (alt51) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1963:6: otherlv_31= 'messageDev' ( (lv_messageDev_32_0= ruleEString ) )
                            {
                            otherlv_31=(Token)match(input,30,FollowSets000.FOLLOW_30_in_ruleWAttribute4078); 

                                	newLeafNode(otherlv_31, grammarAccess.getWAttributeAccess().getMessageDevKeyword_2_15_0());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1967:1: ( (lv_messageDev_32_0= ruleEString ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1968:1: (lv_messageDev_32_0= ruleEString )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1968:1: (lv_messageDev_32_0= ruleEString )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1969:3: lv_messageDev_32_0= ruleEString
                            {
                             
                            	        newCompositeNode(grammarAccess.getWAttributeAccess().getMessageDevEStringParserRuleCall_2_15_1_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWAttribute4099);
                            lv_messageDev_32_0=ruleEString();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWAttributeRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"messageDev",
                                    		lv_messageDev_32_0, 
                                    		"EString");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    otherlv_33=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleWAttribute4113); 

                        	newLeafNode(otherlv_33, grammarAccess.getWAttributeAccess().getRightCurlyBracketKeyword_2_16());
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWAttribute"


    // $ANTLR start "entryRuleWEnum"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1997:1: entryRuleWEnum returns [EObject current=null] : iv_ruleWEnum= ruleWEnum EOF ;
    public final EObject entryRuleWEnum() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWEnum = null;


        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1998:2: (iv_ruleWEnum= ruleWEnum EOF )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:1999:2: iv_ruleWEnum= ruleWEnum EOF
            {
             newCompositeNode(grammarAccess.getWEnumRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleWEnum_in_entryRuleWEnum4151);
            iv_ruleWEnum=ruleWEnum();

            state._fsp--;

             current =iv_ruleWEnum; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleWEnum4161); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWEnum"


    // $ANTLR start "ruleWEnum"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2006:1: ruleWEnum returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'representation' ( (lv_representation_4_0= ruleEEnumRepresentation ) ) )? (otherlv_5= 'elements' otherlv_6= '{' ( (lv_elements_7_0= ruleWEnumValue ) ) (otherlv_8= ',' ( (lv_elements_9_0= ruleWEnumValue ) ) )* otherlv_10= '}' ) otherlv_11= '}' ) ;
    public final EObject ruleWEnum() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        Enumerator lv_representation_4_0 = null;

        EObject lv_elements_7_0 = null;

        EObject lv_elements_9_0 = null;


         enterRule(); 
            
        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2009:28: ( ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'representation' ( (lv_representation_4_0= ruleEEnumRepresentation ) ) )? (otherlv_5= 'elements' otherlv_6= '{' ( (lv_elements_7_0= ruleWEnumValue ) ) (otherlv_8= ',' ( (lv_elements_9_0= ruleWEnumValue ) ) )* otherlv_10= '}' ) otherlv_11= '}' ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2010:1: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'representation' ( (lv_representation_4_0= ruleEEnumRepresentation ) ) )? (otherlv_5= 'elements' otherlv_6= '{' ( (lv_elements_7_0= ruleWEnumValue ) ) (otherlv_8= ',' ( (lv_elements_9_0= ruleWEnumValue ) ) )* otherlv_10= '}' ) otherlv_11= '}' )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2010:1: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'representation' ( (lv_representation_4_0= ruleEEnumRepresentation ) ) )? (otherlv_5= 'elements' otherlv_6= '{' ( (lv_elements_7_0= ruleWEnumValue ) ) (otherlv_8= ',' ( (lv_elements_9_0= ruleWEnumValue ) ) )* otherlv_10= '}' ) otherlv_11= '}' )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2010:2: () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'representation' ( (lv_representation_4_0= ruleEEnumRepresentation ) ) )? (otherlv_5= 'elements' otherlv_6= '{' ( (lv_elements_7_0= ruleWEnumValue ) ) (otherlv_8= ',' ( (lv_elements_9_0= ruleWEnumValue ) ) )* otherlv_10= '}' ) otherlv_11= '}'
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2010:2: ()
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2011:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getWEnumAccess().getWEnumAction_0(),
                        current);
                

            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2016:2: ( (lv_name_1_0= ruleEString ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2017:1: (lv_name_1_0= ruleEString )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2017:1: (lv_name_1_0= ruleEString )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2018:3: lv_name_1_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getWEnumAccess().getNameEStringParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWEnum4216);
            lv_name_1_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getWEnumRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleWEnum4228); 

                	newLeafNode(otherlv_2, grammarAccess.getWEnumAccess().getLeftCurlyBracketKeyword_2());
                
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2038:1: (otherlv_3= 'representation' ( (lv_representation_4_0= ruleEEnumRepresentation ) ) )?
            int alt53=2;
            int LA53_0 = input.LA(1);

            if ( (LA53_0==52) ) {
                alt53=1;
            }
            switch (alt53) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2038:3: otherlv_3= 'representation' ( (lv_representation_4_0= ruleEEnumRepresentation ) )
                    {
                    otherlv_3=(Token)match(input,52,FollowSets000.FOLLOW_52_in_ruleWEnum4241); 

                        	newLeafNode(otherlv_3, grammarAccess.getWEnumAccess().getRepresentationKeyword_3_0());
                        
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2042:1: ( (lv_representation_4_0= ruleEEnumRepresentation ) )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2043:1: (lv_representation_4_0= ruleEEnumRepresentation )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2043:1: (lv_representation_4_0= ruleEEnumRepresentation )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2044:3: lv_representation_4_0= ruleEEnumRepresentation
                    {
                     
                    	        newCompositeNode(grammarAccess.getWEnumAccess().getRepresentationEEnumRepresentationEnumRuleCall_3_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEEnumRepresentation_in_ruleWEnum4262);
                    lv_representation_4_0=ruleEEnumRepresentation();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getWEnumRule());
                    	        }
                           		set(
                           			current, 
                           			"representation",
                            		lv_representation_4_0, 
                            		"EEnumRepresentation");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2060:4: (otherlv_5= 'elements' otherlv_6= '{' ( (lv_elements_7_0= ruleWEnumValue ) ) (otherlv_8= ',' ( (lv_elements_9_0= ruleWEnumValue ) ) )* otherlv_10= '}' )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2060:6: otherlv_5= 'elements' otherlv_6= '{' ( (lv_elements_7_0= ruleWEnumValue ) ) (otherlv_8= ',' ( (lv_elements_9_0= ruleWEnumValue ) ) )* otherlv_10= '}'
            {
            otherlv_5=(Token)match(input,53,FollowSets000.FOLLOW_53_in_ruleWEnum4277); 

                	newLeafNode(otherlv_5, grammarAccess.getWEnumAccess().getElementsKeyword_4_0());
                
            otherlv_6=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleWEnum4289); 

                	newLeafNode(otherlv_6, grammarAccess.getWEnumAccess().getLeftCurlyBracketKeyword_4_1());
                
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2068:1: ( (lv_elements_7_0= ruleWEnumValue ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2069:1: (lv_elements_7_0= ruleWEnumValue )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2069:1: (lv_elements_7_0= ruleWEnumValue )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2070:3: lv_elements_7_0= ruleWEnumValue
            {
             
            	        newCompositeNode(grammarAccess.getWEnumAccess().getElementsWEnumValueParserRuleCall_4_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleWEnumValue_in_ruleWEnum4310);
            lv_elements_7_0=ruleWEnumValue();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getWEnumRule());
            	        }
                   		add(
                   			current, 
                   			"elements",
                    		lv_elements_7_0, 
                    		"WEnumValue");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2086:2: (otherlv_8= ',' ( (lv_elements_9_0= ruleWEnumValue ) ) )*
            loop54:
            do {
                int alt54=2;
                int LA54_0 = input.LA(1);

                if ( (LA54_0==19) ) {
                    alt54=1;
                }


                switch (alt54) {
            	case 1 :
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2086:4: otherlv_8= ',' ( (lv_elements_9_0= ruleWEnumValue ) )
            	    {
            	    otherlv_8=(Token)match(input,19,FollowSets000.FOLLOW_19_in_ruleWEnum4323); 

            	        	newLeafNode(otherlv_8, grammarAccess.getWEnumAccess().getCommaKeyword_4_3_0());
            	        
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2090:1: ( (lv_elements_9_0= ruleWEnumValue ) )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2091:1: (lv_elements_9_0= ruleWEnumValue )
            	    {
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2091:1: (lv_elements_9_0= ruleWEnumValue )
            	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2092:3: lv_elements_9_0= ruleWEnumValue
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getWEnumAccess().getElementsWEnumValueParserRuleCall_4_3_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleWEnumValue_in_ruleWEnum4344);
            	    lv_elements_9_0=ruleWEnumValue();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getWEnumRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"elements",
            	            		lv_elements_9_0, 
            	            		"WEnumValue");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop54;
                }
            } while (true);

            otherlv_10=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleWEnum4358); 

                	newLeafNode(otherlv_10, grammarAccess.getWEnumAccess().getRightCurlyBracketKeyword_4_4());
                

            }

            otherlv_11=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleWEnum4371); 

                	newLeafNode(otherlv_11, grammarAccess.getWEnumAccess().getRightCurlyBracketKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWEnum"


    // $ANTLR start "entryRuleWEnumValue"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2124:1: entryRuleWEnumValue returns [EObject current=null] : iv_ruleWEnumValue= ruleWEnumValue EOF ;
    public final EObject entryRuleWEnumValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWEnumValue = null;


        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2125:2: (iv_ruleWEnumValue= ruleWEnumValue EOF )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2126:2: iv_ruleWEnumValue= ruleWEnumValue EOF
            {
             newCompositeNode(grammarAccess.getWEnumValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleWEnumValue_in_entryRuleWEnumValue4407);
            iv_ruleWEnumValue=ruleWEnumValue();

            state._fsp--;

             current =iv_ruleWEnumValue; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleWEnumValue4417); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWEnumValue"


    // $ANTLR start "ruleWEnumValue"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2133:1: ruleWEnumValue returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) ) ;
    public final EObject ruleWEnumValue() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_1_0 = null;


         enterRule(); 
            
        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2136:28: ( ( () ( (lv_name_1_0= ruleEString ) ) ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2137:1: ( () ( (lv_name_1_0= ruleEString ) ) )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2137:1: ( () ( (lv_name_1_0= ruleEString ) ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2137:2: () ( (lv_name_1_0= ruleEString ) )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2137:2: ()
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2138:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getWEnumValueAccess().getWEnumValueAction_0(),
                        current);
                

            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2143:2: ( (lv_name_1_0= ruleEString ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2144:1: (lv_name_1_0= ruleEString )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2144:1: (lv_name_1_0= ruleEString )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2145:3: lv_name_1_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getWEnumValueAccess().getNameEStringParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWEnumValue4472);
            lv_name_1_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getWEnumValueRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWEnumValue"


    // $ANTLR start "entryRuleWConnection"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2169:1: entryRuleWConnection returns [EObject current=null] : iv_ruleWConnection= ruleWConnection EOF ;
    public final EObject entryRuleWConnection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWConnection = null;


        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2170:2: (iv_ruleWConnection= ruleWConnection EOF )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2171:2: iv_ruleWConnection= ruleWConnection EOF
            {
             newCompositeNode(grammarAccess.getWConnectionRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleWConnection_in_entryRuleWConnection4508);
            iv_ruleWConnection=ruleWConnection();

            state._fsp--;

             current =iv_ruleWConnection; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleWConnection4518); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWConnection"


    // $ANTLR start "ruleWConnection"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2178:1: ruleWConnection returns [EObject current=null] : ( () ( ( ruleEString ) ) otherlv_2= 'as' ( (lv_name_3_0= ruleEString ) ) (otherlv_4= '{' (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? (otherlv_7= 'min' ( (lv_min_8_0= ruleECardinalityMin ) ) )? (otherlv_9= 'max' ( (lv_max_10_0= ruleECardinalityMax ) ) )? (otherlv_11= 'editable' ( (lv_editable_12_0= ruleEBooleanObject ) ) )? otherlv_13= '}' )? ) ;
    public final EObject ruleWConnection() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        AntlrDatatypeRuleToken lv_name_3_0 = null;

        AntlrDatatypeRuleToken lv_label_6_0 = null;

        Enumerator lv_min_8_0 = null;

        Enumerator lv_max_10_0 = null;

        AntlrDatatypeRuleToken lv_editable_12_0 = null;


         enterRule(); 
            
        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2181:28: ( ( () ( ( ruleEString ) ) otherlv_2= 'as' ( (lv_name_3_0= ruleEString ) ) (otherlv_4= '{' (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? (otherlv_7= 'min' ( (lv_min_8_0= ruleECardinalityMin ) ) )? (otherlv_9= 'max' ( (lv_max_10_0= ruleECardinalityMax ) ) )? (otherlv_11= 'editable' ( (lv_editable_12_0= ruleEBooleanObject ) ) )? otherlv_13= '}' )? ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2182:1: ( () ( ( ruleEString ) ) otherlv_2= 'as' ( (lv_name_3_0= ruleEString ) ) (otherlv_4= '{' (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? (otherlv_7= 'min' ( (lv_min_8_0= ruleECardinalityMin ) ) )? (otherlv_9= 'max' ( (lv_max_10_0= ruleECardinalityMax ) ) )? (otherlv_11= 'editable' ( (lv_editable_12_0= ruleEBooleanObject ) ) )? otherlv_13= '}' )? )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2182:1: ( () ( ( ruleEString ) ) otherlv_2= 'as' ( (lv_name_3_0= ruleEString ) ) (otherlv_4= '{' (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? (otherlv_7= 'min' ( (lv_min_8_0= ruleECardinalityMin ) ) )? (otherlv_9= 'max' ( (lv_max_10_0= ruleECardinalityMax ) ) )? (otherlv_11= 'editable' ( (lv_editable_12_0= ruleEBooleanObject ) ) )? otherlv_13= '}' )? )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2182:2: () ( ( ruleEString ) ) otherlv_2= 'as' ( (lv_name_3_0= ruleEString ) ) (otherlv_4= '{' (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? (otherlv_7= 'min' ( (lv_min_8_0= ruleECardinalityMin ) ) )? (otherlv_9= 'max' ( (lv_max_10_0= ruleECardinalityMax ) ) )? (otherlv_11= 'editable' ( (lv_editable_12_0= ruleEBooleanObject ) ) )? otherlv_13= '}' )?
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2182:2: ()
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2183:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getWConnectionAccess().getWConnectionAction_0(),
                        current);
                

            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2188:2: ( ( ruleEString ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2189:1: ( ruleEString )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2189:1: ( ruleEString )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2190:3: ruleEString
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getWConnectionRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getWConnectionAccess().getEntityWEntityCrossReference_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWConnection4575);
            ruleEString();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,54,FollowSets000.FOLLOW_54_in_ruleWConnection4587); 

                	newLeafNode(otherlv_2, grammarAccess.getWConnectionAccess().getAsKeyword_2());
                
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2207:1: ( (lv_name_3_0= ruleEString ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2208:1: (lv_name_3_0= ruleEString )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2208:1: (lv_name_3_0= ruleEString )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2209:3: lv_name_3_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getWConnectionAccess().getNameEStringParserRuleCall_3_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWConnection4608);
            lv_name_3_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getWConnectionRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_3_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2225:2: (otherlv_4= '{' (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? (otherlv_7= 'min' ( (lv_min_8_0= ruleECardinalityMin ) ) )? (otherlv_9= 'max' ( (lv_max_10_0= ruleECardinalityMax ) ) )? (otherlv_11= 'editable' ( (lv_editable_12_0= ruleEBooleanObject ) ) )? otherlv_13= '}' )?
            int alt59=2;
            int LA59_0 = input.LA(1);

            if ( (LA59_0==12) ) {
                alt59=1;
            }
            switch (alt59) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2225:4: otherlv_4= '{' (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? (otherlv_7= 'min' ( (lv_min_8_0= ruleECardinalityMin ) ) )? (otherlv_9= 'max' ( (lv_max_10_0= ruleECardinalityMax ) ) )? (otherlv_11= 'editable' ( (lv_editable_12_0= ruleEBooleanObject ) ) )? otherlv_13= '}'
                    {
                    otherlv_4=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleWConnection4621); 

                        	newLeafNode(otherlv_4, grammarAccess.getWConnectionAccess().getLeftCurlyBracketKeyword_4_0());
                        
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2229:1: (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )?
                    int alt55=2;
                    int LA55_0 = input.LA(1);

                    if ( (LA55_0==25) ) {
                        alt55=1;
                    }
                    switch (alt55) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2229:3: otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) )
                            {
                            otherlv_5=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleWConnection4634); 

                                	newLeafNode(otherlv_5, grammarAccess.getWConnectionAccess().getLabelKeyword_4_1_0());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2233:1: ( (lv_label_6_0= ruleEString ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2234:1: (lv_label_6_0= ruleEString )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2234:1: (lv_label_6_0= ruleEString )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2235:3: lv_label_6_0= ruleEString
                            {
                             
                            	        newCompositeNode(grammarAccess.getWConnectionAccess().getLabelEStringParserRuleCall_4_1_1_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWConnection4655);
                            lv_label_6_0=ruleEString();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWConnectionRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"label",
                                    		lv_label_6_0, 
                                    		"EString");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2251:4: (otherlv_7= 'min' ( (lv_min_8_0= ruleECardinalityMin ) ) )?
                    int alt56=2;
                    int LA56_0 = input.LA(1);

                    if ( (LA56_0==55) ) {
                        alt56=1;
                    }
                    switch (alt56) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2251:6: otherlv_7= 'min' ( (lv_min_8_0= ruleECardinalityMin ) )
                            {
                            otherlv_7=(Token)match(input,55,FollowSets000.FOLLOW_55_in_ruleWConnection4670); 

                                	newLeafNode(otherlv_7, grammarAccess.getWConnectionAccess().getMinKeyword_4_2_0());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2255:1: ( (lv_min_8_0= ruleECardinalityMin ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2256:1: (lv_min_8_0= ruleECardinalityMin )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2256:1: (lv_min_8_0= ruleECardinalityMin )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2257:3: lv_min_8_0= ruleECardinalityMin
                            {
                             
                            	        newCompositeNode(grammarAccess.getWConnectionAccess().getMinECardinalityMinEnumRuleCall_4_2_1_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleECardinalityMin_in_ruleWConnection4691);
                            lv_min_8_0=ruleECardinalityMin();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWConnectionRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"min",
                                    		lv_min_8_0, 
                                    		"ECardinalityMin");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2273:4: (otherlv_9= 'max' ( (lv_max_10_0= ruleECardinalityMax ) ) )?
                    int alt57=2;
                    int LA57_0 = input.LA(1);

                    if ( (LA57_0==56) ) {
                        alt57=1;
                    }
                    switch (alt57) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2273:6: otherlv_9= 'max' ( (lv_max_10_0= ruleECardinalityMax ) )
                            {
                            otherlv_9=(Token)match(input,56,FollowSets000.FOLLOW_56_in_ruleWConnection4706); 

                                	newLeafNode(otherlv_9, grammarAccess.getWConnectionAccess().getMaxKeyword_4_3_0());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2277:1: ( (lv_max_10_0= ruleECardinalityMax ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2278:1: (lv_max_10_0= ruleECardinalityMax )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2278:1: (lv_max_10_0= ruleECardinalityMax )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2279:3: lv_max_10_0= ruleECardinalityMax
                            {
                             
                            	        newCompositeNode(grammarAccess.getWConnectionAccess().getMaxECardinalityMaxEnumRuleCall_4_3_1_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleECardinalityMax_in_ruleWConnection4727);
                            lv_max_10_0=ruleECardinalityMax();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWConnectionRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"max",
                                    		lv_max_10_0, 
                                    		"ECardinalityMax");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2295:4: (otherlv_11= 'editable' ( (lv_editable_12_0= ruleEBooleanObject ) ) )?
                    int alt58=2;
                    int LA58_0 = input.LA(1);

                    if ( (LA58_0==57) ) {
                        alt58=1;
                    }
                    switch (alt58) {
                        case 1 :
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2295:6: otherlv_11= 'editable' ( (lv_editable_12_0= ruleEBooleanObject ) )
                            {
                            otherlv_11=(Token)match(input,57,FollowSets000.FOLLOW_57_in_ruleWConnection4742); 

                                	newLeafNode(otherlv_11, grammarAccess.getWConnectionAccess().getEditableKeyword_4_4_0());
                                
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2299:1: ( (lv_editable_12_0= ruleEBooleanObject ) )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2300:1: (lv_editable_12_0= ruleEBooleanObject )
                            {
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2300:1: (lv_editable_12_0= ruleEBooleanObject )
                            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2301:3: lv_editable_12_0= ruleEBooleanObject
                            {
                             
                            	        newCompositeNode(grammarAccess.getWConnectionAccess().getEditableEBooleanObjectParserRuleCall_4_4_1_0()); 
                            	    
                            pushFollow(FollowSets000.FOLLOW_ruleEBooleanObject_in_ruleWConnection4763);
                            lv_editable_12_0=ruleEBooleanObject();

                            state._fsp--;


                            	        if (current==null) {
                            	            current = createModelElementForParent(grammarAccess.getWConnectionRule());
                            	        }
                                   		set(
                                   			current, 
                                   			"editable",
                                    		lv_editable_12_0, 
                                    		"EBooleanObject");
                            	        afterParserOrEnumRuleCall();
                            	    

                            }


                            }


                            }
                            break;

                    }

                    otherlv_13=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleWConnection4777); 

                        	newLeafNode(otherlv_13, grammarAccess.getWConnectionAccess().getRightCurlyBracketKeyword_4_5());
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWConnection"


    // $ANTLR start "entryRuleWReport"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2329:1: entryRuleWReport returns [EObject current=null] : iv_ruleWReport= ruleWReport EOF ;
    public final EObject entryRuleWReport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWReport = null;


        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2330:2: (iv_ruleWReport= ruleWReport EOF )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2331:2: iv_ruleWReport= ruleWReport EOF
            {
             newCompositeNode(grammarAccess.getWReportRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleWReport_in_entryRuleWReport4815);
            iv_ruleWReport=ruleWReport();

            state._fsp--;

             current =iv_ruleWReport; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleWReport4825); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWReport"


    // $ANTLR start "ruleWReport"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2338:1: ruleWReport returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+ {...}?) ) )? ) ;
    public final EObject ruleWReport() throws RecognitionException {
        EObject current = null;

        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_version_5_0 = null;

        AntlrDatatypeRuleToken lv_label_7_0 = null;

        AntlrDatatypeRuleToken lv_generate_9_0 = null;

        EObject lv_attributes_12_0 = null;

        EObject lv_attributes_14_0 = null;


         enterRule(); 
            
        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2341:28: ( ( () ( (lv_name_1_0= ruleEString ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+ {...}?) ) )? ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2342:1: ( () ( (lv_name_1_0= ruleEString ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+ {...}?) ) )? )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2342:1: ( () ( (lv_name_1_0= ruleEString ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+ {...}?) ) )? )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2342:2: () ( (lv_name_1_0= ruleEString ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+ {...}?) ) )?
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2342:2: ()
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2343:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getWReportAccess().getWReportAction_0(),
                        current);
                

            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2348:2: ( (lv_name_1_0= ruleEString ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2349:1: (lv_name_1_0= ruleEString )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2349:1: (lv_name_1_0= ruleEString )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2350:3: lv_name_1_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getWReportAccess().getNameEStringParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWReport4880);
            lv_name_1_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getWReportRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2366:2: ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+ {...}?) ) )?
            int alt63=2;
            int LA63_0 = input.LA(1);

            if ( (LA63_0==12||LA63_0==25||LA63_0==27||LA63_0==40) ) {
                alt63=1;
            }
            switch (alt63) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2368:1: ( ( ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+ {...}?) )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2368:1: ( ( ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+ {...}?) )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2369:2: ( ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+ {...}?)
                    {
                     
                    	  getUnorderedGroupHelper().enter(grammarAccess.getWReportAccess().getUnorderedGroup_2());
                    	
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2372:2: ( ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+ {...}?)
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2373:3: ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+ {...}?
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2373:3: ( ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) ) )+
                    int cnt62=0;
                    loop62:
                    do {
                        int alt62=5;
                        int LA62_0 = input.LA(1);

                        if ( LA62_0 ==12 && getUnorderedGroupHelper().canSelect(grammarAccess.getWReportAccess().getUnorderedGroup_2(), 0) ) {
                            alt62=1;
                        }
                        else if ( LA62_0 ==25 && getUnorderedGroupHelper().canSelect(grammarAccess.getWReportAccess().getUnorderedGroup_2(), 1) ) {
                            alt62=2;
                        }
                        else if ( LA62_0 ==27 && getUnorderedGroupHelper().canSelect(grammarAccess.getWReportAccess().getUnorderedGroup_2(), 2) ) {
                            alt62=3;
                        }
                        else if ( LA62_0 ==40 && getUnorderedGroupHelper().canSelect(grammarAccess.getWReportAccess().getUnorderedGroup_2(), 3) ) {
                            alt62=4;
                        }


                        switch (alt62) {
                    	case 1 :
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2375:4: ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) )
                    	    {
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2375:4: ({...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2376:5: {...}? => ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) )
                    	    {
                    	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getWReportAccess().getUnorderedGroup_2(), 0) ) {
                    	        throw new FailedPredicateException(input, "ruleWReport", "getUnorderedGroupHelper().canSelect(grammarAccess.getWReportAccess().getUnorderedGroup_2(), 0)");
                    	    }
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2376:104: ( ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2377:6: ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) )
                    	    {
                    	     
                    	    	 				  getUnorderedGroupHelper().select(grammarAccess.getWReportAccess().getUnorderedGroup_2(), 0);
                    	    	 				
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2380:6: ({...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2380:7: {...}? => (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? )
                    	    {
                    	    if ( !((true)) ) {
                    	        throw new FailedPredicateException(input, "ruleWReport", "true");
                    	    }
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2380:16: (otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )? )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2380:18: otherlv_3= '{' (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )?
                    	    {
                    	    otherlv_3=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleWReport4938); 

                    	        	newLeafNode(otherlv_3, grammarAccess.getWReportAccess().getLeftCurlyBracketKeyword_2_0_0());
                    	        
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2384:1: (otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) ) )?
                    	    int alt60=2;
                    	    int LA60_0 = input.LA(1);

                    	    if ( (LA60_0==14) ) {
                    	        alt60=1;
                    	    }
                    	    switch (alt60) {
                    	        case 1 :
                    	            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2384:3: otherlv_4= 'version' ( (lv_version_5_0= ruleEIntegerObject ) )
                    	            {
                    	            otherlv_4=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleWReport4951); 

                    	                	newLeafNode(otherlv_4, grammarAccess.getWReportAccess().getVersionKeyword_2_0_1_0());
                    	                
                    	            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2388:1: ( (lv_version_5_0= ruleEIntegerObject ) )
                    	            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2389:1: (lv_version_5_0= ruleEIntegerObject )
                    	            {
                    	            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2389:1: (lv_version_5_0= ruleEIntegerObject )
                    	            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2390:3: lv_version_5_0= ruleEIntegerObject
                    	            {
                    	             
                    	            	        newCompositeNode(grammarAccess.getWReportAccess().getVersionEIntegerObjectParserRuleCall_2_0_1_1_0()); 
                    	            	    
                    	            pushFollow(FollowSets000.FOLLOW_ruleEIntegerObject_in_ruleWReport4972);
                    	            lv_version_5_0=ruleEIntegerObject();

                    	            state._fsp--;


                    	            	        if (current==null) {
                    	            	            current = createModelElementForParent(grammarAccess.getWReportRule());
                    	            	        }
                    	                   		set(
                    	                   			current, 
                    	                   			"version",
                    	                    		lv_version_5_0, 
                    	                    		"EIntegerObject");
                    	            	        afterParserOrEnumRuleCall();
                    	            	    

                    	            }


                    	            }


                    	            }
                    	            break;

                    	    }


                    	    }


                    	    }

                    	     
                    	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getWReportAccess().getUnorderedGroup_2());
                    	    	 				

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 2 :
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2413:4: ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) )
                    	    {
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2413:4: ({...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2414:5: {...}? => ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) )
                    	    {
                    	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getWReportAccess().getUnorderedGroup_2(), 1) ) {
                    	        throw new FailedPredicateException(input, "ruleWReport", "getUnorderedGroupHelper().canSelect(grammarAccess.getWReportAccess().getUnorderedGroup_2(), 1)");
                    	    }
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2414:104: ( ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2415:6: ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) )
                    	    {
                    	     
                    	    	 				  getUnorderedGroupHelper().select(grammarAccess.getWReportAccess().getUnorderedGroup_2(), 1);
                    	    	 				
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2418:6: ({...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2418:7: {...}? => (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) )
                    	    {
                    	    if ( !((true)) ) {
                    	        throw new FailedPredicateException(input, "ruleWReport", "true");
                    	    }
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2418:16: (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2418:18: otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) )
                    	    {
                    	    otherlv_6=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleWReport5042); 

                    	        	newLeafNode(otherlv_6, grammarAccess.getWReportAccess().getLabelKeyword_2_1_0());
                    	        
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2422:1: ( (lv_label_7_0= ruleEString ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2423:1: (lv_label_7_0= ruleEString )
                    	    {
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2423:1: (lv_label_7_0= ruleEString )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2424:3: lv_label_7_0= ruleEString
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getWReportAccess().getLabelEStringParserRuleCall_2_1_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleWReport5063);
                    	    lv_label_7_0=ruleEString();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getWReportRule());
                    	    	        }
                    	           		set(
                    	           			current, 
                    	           			"label",
                    	            		lv_label_7_0, 
                    	            		"EString");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }


                    	    }

                    	     
                    	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getWReportAccess().getUnorderedGroup_2());
                    	    	 				

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 3 :
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2447:4: ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) )
                    	    {
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2447:4: ({...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2448:5: {...}? => ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) )
                    	    {
                    	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getWReportAccess().getUnorderedGroup_2(), 2) ) {
                    	        throw new FailedPredicateException(input, "ruleWReport", "getUnorderedGroupHelper().canSelect(grammarAccess.getWReportAccess().getUnorderedGroup_2(), 2)");
                    	    }
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2448:104: ( ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2449:6: ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) )
                    	    {
                    	     
                    	    	 				  getUnorderedGroupHelper().select(grammarAccess.getWReportAccess().getUnorderedGroup_2(), 2);
                    	    	 				
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2452:6: ({...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2452:7: {...}? => (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) )
                    	    {
                    	    if ( !((true)) ) {
                    	        throw new FailedPredicateException(input, "ruleWReport", "true");
                    	    }
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2452:16: (otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2452:18: otherlv_8= 'generate' ( (lv_generate_9_0= ruleEBooleanObject ) )
                    	    {
                    	    otherlv_8=(Token)match(input,27,FollowSets000.FOLLOW_27_in_ruleWReport5131); 

                    	        	newLeafNode(otherlv_8, grammarAccess.getWReportAccess().getGenerateKeyword_2_2_0());
                    	        
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2456:1: ( (lv_generate_9_0= ruleEBooleanObject ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2457:1: (lv_generate_9_0= ruleEBooleanObject )
                    	    {
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2457:1: (lv_generate_9_0= ruleEBooleanObject )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2458:3: lv_generate_9_0= ruleEBooleanObject
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getWReportAccess().getGenerateEBooleanObjectParserRuleCall_2_2_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleEBooleanObject_in_ruleWReport5152);
                    	    lv_generate_9_0=ruleEBooleanObject();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getWReportRule());
                    	    	        }
                    	           		set(
                    	           			current, 
                    	           			"generate",
                    	            		lv_generate_9_0, 
                    	            		"EBooleanObject");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }


                    	    }

                    	     
                    	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getWReportAccess().getUnorderedGroup_2());
                    	    	 				

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 4 :
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2481:4: ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) )
                    	    {
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2481:4: ({...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2482:5: {...}? => ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) )
                    	    {
                    	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getWReportAccess().getUnorderedGroup_2(), 3) ) {
                    	        throw new FailedPredicateException(input, "ruleWReport", "getUnorderedGroupHelper().canSelect(grammarAccess.getWReportAccess().getUnorderedGroup_2(), 3)");
                    	    }
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2482:104: ( ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2483:6: ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) )
                    	    {
                    	     
                    	    	 				  getUnorderedGroupHelper().select(grammarAccess.getWReportAccess().getUnorderedGroup_2(), 3);
                    	    	 				
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2486:6: ({...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2486:7: {...}? => ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' )
                    	    {
                    	    if ( !((true)) ) {
                    	        throw new FailedPredicateException(input, "ruleWReport", "true");
                    	    }
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2486:16: ( (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}' )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2486:17: (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' ) otherlv_16= '}'
                    	    {
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2486:17: (otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}' )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2486:19: otherlv_10= 'attributes' otherlv_11= '{' ( (lv_attributes_12_0= ruleWAttribute ) ) (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )* otherlv_15= '}'
                    	    {
                    	    otherlv_10=(Token)match(input,40,FollowSets000.FOLLOW_40_in_ruleWReport5221); 

                    	        	newLeafNode(otherlv_10, grammarAccess.getWReportAccess().getAttributesKeyword_2_3_0_0());
                    	        
                    	    otherlv_11=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleWReport5233); 

                    	        	newLeafNode(otherlv_11, grammarAccess.getWReportAccess().getLeftCurlyBracketKeyword_2_3_0_1());
                    	        
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2494:1: ( (lv_attributes_12_0= ruleWAttribute ) )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2495:1: (lv_attributes_12_0= ruleWAttribute )
                    	    {
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2495:1: (lv_attributes_12_0= ruleWAttribute )
                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2496:3: lv_attributes_12_0= ruleWAttribute
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getWReportAccess().getAttributesWAttributeParserRuleCall_2_3_0_2_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleWAttribute_in_ruleWReport5254);
                    	    lv_attributes_12_0=ruleWAttribute();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getWReportRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"attributes",
                    	            		lv_attributes_12_0, 
                    	            		"WAttribute");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }

                    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2512:2: (otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) ) )*
                    	    loop61:
                    	    do {
                    	        int alt61=2;
                    	        int LA61_0 = input.LA(1);

                    	        if ( (LA61_0==19) ) {
                    	            alt61=1;
                    	        }


                    	        switch (alt61) {
                    	    	case 1 :
                    	    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2512:4: otherlv_13= ',' ( (lv_attributes_14_0= ruleWAttribute ) )
                    	    	    {
                    	    	    otherlv_13=(Token)match(input,19,FollowSets000.FOLLOW_19_in_ruleWReport5267); 

                    	    	        	newLeafNode(otherlv_13, grammarAccess.getWReportAccess().getCommaKeyword_2_3_0_3_0());
                    	    	        
                    	    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2516:1: ( (lv_attributes_14_0= ruleWAttribute ) )
                    	    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2517:1: (lv_attributes_14_0= ruleWAttribute )
                    	    	    {
                    	    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2517:1: (lv_attributes_14_0= ruleWAttribute )
                    	    	    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2518:3: lv_attributes_14_0= ruleWAttribute
                    	    	    {
                    	    	     
                    	    	    	        newCompositeNode(grammarAccess.getWReportAccess().getAttributesWAttributeParserRuleCall_2_3_0_3_1_0()); 
                    	    	    	    
                    	    	    pushFollow(FollowSets000.FOLLOW_ruleWAttribute_in_ruleWReport5288);
                    	    	    lv_attributes_14_0=ruleWAttribute();

                    	    	    state._fsp--;


                    	    	    	        if (current==null) {
                    	    	    	            current = createModelElementForParent(grammarAccess.getWReportRule());
                    	    	    	        }
                    	    	           		add(
                    	    	           			current, 
                    	    	           			"attributes",
                    	    	            		lv_attributes_14_0, 
                    	    	            		"WAttribute");
                    	    	    	        afterParserOrEnumRuleCall();
                    	    	    	    

                    	    	    }


                    	    	    }


                    	    	    }
                    	    	    break;

                    	    	default :
                    	    	    break loop61;
                    	        }
                    	    } while (true);

                    	    otherlv_15=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleWReport5302); 

                    	        	newLeafNode(otherlv_15, grammarAccess.getWReportAccess().getRightCurlyBracketKeyword_2_3_0_4());
                    	        

                    	    }

                    	    otherlv_16=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleWReport5315); 

                    	        	newLeafNode(otherlv_16, grammarAccess.getWReportAccess().getRightCurlyBracketKeyword_2_3_1());
                    	        

                    	    }


                    	    }

                    	     
                    	    	 				  getUnorderedGroupHelper().returnFromSelection(grammarAccess.getWReportAccess().getUnorderedGroup_2());
                    	    	 				

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt62 >= 1 ) break loop62;
                                EarlyExitException eee =
                                    new EarlyExitException(62, input);
                                throw eee;
                        }
                        cnt62++;
                    } while (true);

                    if ( ! getUnorderedGroupHelper().canLeave(grammarAccess.getWReportAccess().getUnorderedGroup_2()) ) {
                        throw new FailedPredicateException(input, "ruleWReport", "getUnorderedGroupHelper().canLeave(grammarAccess.getWReportAccess().getUnorderedGroup_2())");
                    }

                    }


                    }

                     
                    	  getUnorderedGroupHelper().leave(grammarAccess.getWReportAccess().getUnorderedGroup_2());
                    	

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWReport"


    // $ANTLR start "entryRuleEIntegerObject"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2565:1: entryRuleEIntegerObject returns [String current=null] : iv_ruleEIntegerObject= ruleEIntegerObject EOF ;
    public final String entryRuleEIntegerObject() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEIntegerObject = null;


        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2566:2: (iv_ruleEIntegerObject= ruleEIntegerObject EOF )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2567:2: iv_ruleEIntegerObject= ruleEIntegerObject EOF
            {
             newCompositeNode(grammarAccess.getEIntegerObjectRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEIntegerObject_in_entryRuleEIntegerObject5400);
            iv_ruleEIntegerObject=ruleEIntegerObject();

            state._fsp--;

             current =iv_ruleEIntegerObject.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEIntegerObject5411); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEIntegerObject"


    // $ANTLR start "ruleEIntegerObject"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2574:1: ruleEIntegerObject returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? this_INT_1= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleEIntegerObject() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;

         enterRule(); 
            
        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2577:28: ( ( (kw= '-' )? this_INT_1= RULE_INT ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2578:1: ( (kw= '-' )? this_INT_1= RULE_INT )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2578:1: ( (kw= '-' )? this_INT_1= RULE_INT )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2578:2: (kw= '-' )? this_INT_1= RULE_INT
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2578:2: (kw= '-' )?
            int alt64=2;
            int LA64_0 = input.LA(1);

            if ( (LA64_0==58) ) {
                alt64=1;
            }
            switch (alt64) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2579:2: kw= '-'
                    {
                    kw=(Token)match(input,58,FollowSets000.FOLLOW_58_in_ruleEIntegerObject5450); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getEIntegerObjectAccess().getHyphenMinusKeyword_0()); 
                        

                    }
                    break;

            }

            this_INT_1=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_ruleEIntegerObject5467); 

            		current.merge(this_INT_1);
                
             
                newLeafNode(this_INT_1, grammarAccess.getEIntegerObjectAccess().getINTTerminalRuleCall_1()); 
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEIntegerObject"


    // $ANTLR start "ruleECardinalityMin"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2599:1: ruleECardinalityMin returns [Enumerator current=null] : ( (enumLiteral_0= 'zero' ) | (enumLiteral_1= 'one' ) ) ;
    public final Enumerator ruleECardinalityMin() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;

         enterRule(); 
        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2601:28: ( ( (enumLiteral_0= 'zero' ) | (enumLiteral_1= 'one' ) ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2602:1: ( (enumLiteral_0= 'zero' ) | (enumLiteral_1= 'one' ) )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2602:1: ( (enumLiteral_0= 'zero' ) | (enumLiteral_1= 'one' ) )
            int alt65=2;
            int LA65_0 = input.LA(1);

            if ( (LA65_0==59) ) {
                alt65=1;
            }
            else if ( (LA65_0==60) ) {
                alt65=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 65, 0, input);

                throw nvae;
            }
            switch (alt65) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2602:2: (enumLiteral_0= 'zero' )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2602:2: (enumLiteral_0= 'zero' )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2602:4: enumLiteral_0= 'zero'
                    {
                    enumLiteral_0=(Token)match(input,59,FollowSets000.FOLLOW_59_in_ruleECardinalityMin5526); 

                            current = grammarAccess.getECardinalityMinAccess().getZeroEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getECardinalityMinAccess().getZeroEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2608:6: (enumLiteral_1= 'one' )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2608:6: (enumLiteral_1= 'one' )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2608:8: enumLiteral_1= 'one'
                    {
                    enumLiteral_1=(Token)match(input,60,FollowSets000.FOLLOW_60_in_ruleECardinalityMin5543); 

                            current = grammarAccess.getECardinalityMinAccess().getOneEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getECardinalityMinAccess().getOneEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleECardinalityMin"


    // $ANTLR start "ruleECardinalityMax"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2618:1: ruleECardinalityMax returns [Enumerator current=null] : ( (enumLiteral_0= 'one' ) | (enumLiteral_1= 'many' ) ) ;
    public final Enumerator ruleECardinalityMax() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;

         enterRule(); 
        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2620:28: ( ( (enumLiteral_0= 'one' ) | (enumLiteral_1= 'many' ) ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2621:1: ( (enumLiteral_0= 'one' ) | (enumLiteral_1= 'many' ) )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2621:1: ( (enumLiteral_0= 'one' ) | (enumLiteral_1= 'many' ) )
            int alt66=2;
            int LA66_0 = input.LA(1);

            if ( (LA66_0==60) ) {
                alt66=1;
            }
            else if ( (LA66_0==61) ) {
                alt66=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 66, 0, input);

                throw nvae;
            }
            switch (alt66) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2621:2: (enumLiteral_0= 'one' )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2621:2: (enumLiteral_0= 'one' )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2621:4: enumLiteral_0= 'one'
                    {
                    enumLiteral_0=(Token)match(input,60,FollowSets000.FOLLOW_60_in_ruleECardinalityMax5588); 

                            current = grammarAccess.getECardinalityMaxAccess().getOneEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getECardinalityMaxAccess().getOneEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2627:6: (enumLiteral_1= 'many' )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2627:6: (enumLiteral_1= 'many' )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2627:8: enumLiteral_1= 'many'
                    {
                    enumLiteral_1=(Token)match(input,61,FollowSets000.FOLLOW_61_in_ruleECardinalityMax5605); 

                            current = grammarAccess.getECardinalityMaxAccess().getManyEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getECardinalityMaxAccess().getManyEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleECardinalityMax"


    // $ANTLR start "ruleEEnumRepresentation"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2637:1: ruleEEnumRepresentation returns [Enumerator current=null] : ( (enumLiteral_0= 'Combobox' ) | (enumLiteral_1= 'Radio' ) ) ;
    public final Enumerator ruleEEnumRepresentation() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;

         enterRule(); 
        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2639:28: ( ( (enumLiteral_0= 'Combobox' ) | (enumLiteral_1= 'Radio' ) ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2640:1: ( (enumLiteral_0= 'Combobox' ) | (enumLiteral_1= 'Radio' ) )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2640:1: ( (enumLiteral_0= 'Combobox' ) | (enumLiteral_1= 'Radio' ) )
            int alt67=2;
            int LA67_0 = input.LA(1);

            if ( (LA67_0==62) ) {
                alt67=1;
            }
            else if ( (LA67_0==63) ) {
                alt67=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 67, 0, input);

                throw nvae;
            }
            switch (alt67) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2640:2: (enumLiteral_0= 'Combobox' )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2640:2: (enumLiteral_0= 'Combobox' )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2640:4: enumLiteral_0= 'Combobox'
                    {
                    enumLiteral_0=(Token)match(input,62,FollowSets000.FOLLOW_62_in_ruleEEnumRepresentation5650); 

                            current = grammarAccess.getEEnumRepresentationAccess().getComboboxEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getEEnumRepresentationAccess().getComboboxEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2646:6: (enumLiteral_1= 'Radio' )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2646:6: (enumLiteral_1= 'Radio' )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2646:8: enumLiteral_1= 'Radio'
                    {
                    enumLiteral_1=(Token)match(input,63,FollowSets000.FOLLOW_63_in_ruleEEnumRepresentation5667); 

                            current = grammarAccess.getEEnumRepresentationAccess().getRadioEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getEEnumRepresentationAccess().getRadioEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEEnumRepresentation"


    // $ANTLR start "ruleEChangesType"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2656:1: ruleEChangesType returns [Enumerator current=null] : ( (enumLiteral_0= 'Delete' ) | (enumLiteral_1= 'Insert' ) | (enumLiteral_2= 'Update' ) ) ;
    public final Enumerator ruleEChangesType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;

         enterRule(); 
        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2658:28: ( ( (enumLiteral_0= 'Delete' ) | (enumLiteral_1= 'Insert' ) | (enumLiteral_2= 'Update' ) ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2659:1: ( (enumLiteral_0= 'Delete' ) | (enumLiteral_1= 'Insert' ) | (enumLiteral_2= 'Update' ) )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2659:1: ( (enumLiteral_0= 'Delete' ) | (enumLiteral_1= 'Insert' ) | (enumLiteral_2= 'Update' ) )
            int alt68=3;
            switch ( input.LA(1) ) {
            case 64:
                {
                alt68=1;
                }
                break;
            case 65:
                {
                alt68=2;
                }
                break;
            case 66:
                {
                alt68=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 68, 0, input);

                throw nvae;
            }

            switch (alt68) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2659:2: (enumLiteral_0= 'Delete' )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2659:2: (enumLiteral_0= 'Delete' )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2659:4: enumLiteral_0= 'Delete'
                    {
                    enumLiteral_0=(Token)match(input,64,FollowSets000.FOLLOW_64_in_ruleEChangesType5712); 

                            current = grammarAccess.getEChangesTypeAccess().getDeleteEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getEChangesTypeAccess().getDeleteEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2665:6: (enumLiteral_1= 'Insert' )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2665:6: (enumLiteral_1= 'Insert' )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2665:8: enumLiteral_1= 'Insert'
                    {
                    enumLiteral_1=(Token)match(input,65,FollowSets000.FOLLOW_65_in_ruleEChangesType5729); 

                            current = grammarAccess.getEChangesTypeAccess().getInsertEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getEChangesTypeAccess().getInsertEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;
                case 3 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2671:6: (enumLiteral_2= 'Update' )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2671:6: (enumLiteral_2= 'Update' )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2671:8: enumLiteral_2= 'Update'
                    {
                    enumLiteral_2=(Token)match(input,66,FollowSets000.FOLLOW_66_in_ruleEChangesType5746); 

                            current = grammarAccess.getEChangesTypeAccess().getUpdateEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_2, grammarAccess.getEChangesTypeAccess().getUpdateEnumLiteralDeclaration_2()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEChangesType"


    // $ANTLR start "ruleEElementChangesType"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2681:1: ruleEElementChangesType returns [Enumerator current=null] : ( (enumLiteral_0= 'Attribute' ) | (enumLiteral_1= 'Entity' ) | (enumLiteral_2= 'Link' ) | (enumLiteral_3= 'Package' ) ) ;
    public final Enumerator ruleEElementChangesType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;

         enterRule(); 
        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2683:28: ( ( (enumLiteral_0= 'Attribute' ) | (enumLiteral_1= 'Entity' ) | (enumLiteral_2= 'Link' ) | (enumLiteral_3= 'Package' ) ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2684:1: ( (enumLiteral_0= 'Attribute' ) | (enumLiteral_1= 'Entity' ) | (enumLiteral_2= 'Link' ) | (enumLiteral_3= 'Package' ) )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2684:1: ( (enumLiteral_0= 'Attribute' ) | (enumLiteral_1= 'Entity' ) | (enumLiteral_2= 'Link' ) | (enumLiteral_3= 'Package' ) )
            int alt69=4;
            switch ( input.LA(1) ) {
            case 67:
                {
                alt69=1;
                }
                break;
            case 68:
                {
                alt69=2;
                }
                break;
            case 69:
                {
                alt69=3;
                }
                break;
            case 70:
                {
                alt69=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 69, 0, input);

                throw nvae;
            }

            switch (alt69) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2684:2: (enumLiteral_0= 'Attribute' )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2684:2: (enumLiteral_0= 'Attribute' )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2684:4: enumLiteral_0= 'Attribute'
                    {
                    enumLiteral_0=(Token)match(input,67,FollowSets000.FOLLOW_67_in_ruleEElementChangesType5791); 

                            current = grammarAccess.getEElementChangesTypeAccess().getAttributeEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getEElementChangesTypeAccess().getAttributeEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2690:6: (enumLiteral_1= 'Entity' )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2690:6: (enumLiteral_1= 'Entity' )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2690:8: enumLiteral_1= 'Entity'
                    {
                    enumLiteral_1=(Token)match(input,68,FollowSets000.FOLLOW_68_in_ruleEElementChangesType5808); 

                            current = grammarAccess.getEElementChangesTypeAccess().getEntityEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getEElementChangesTypeAccess().getEntityEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;
                case 3 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2696:6: (enumLiteral_2= 'Link' )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2696:6: (enumLiteral_2= 'Link' )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2696:8: enumLiteral_2= 'Link'
                    {
                    enumLiteral_2=(Token)match(input,69,FollowSets000.FOLLOW_69_in_ruleEElementChangesType5825); 

                            current = grammarAccess.getEElementChangesTypeAccess().getLinkEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_2, grammarAccess.getEElementChangesTypeAccess().getLinkEnumLiteralDeclaration_2()); 
                        

                    }


                    }
                    break;
                case 4 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2702:6: (enumLiteral_3= 'Package' )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2702:6: (enumLiteral_3= 'Package' )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2702:8: enumLiteral_3= 'Package'
                    {
                    enumLiteral_3=(Token)match(input,70,FollowSets000.FOLLOW_70_in_ruleEElementChangesType5842); 

                            current = grammarAccess.getEElementChangesTypeAccess().getPackageEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_3, grammarAccess.getEElementChangesTypeAccess().getPackageEnumLiteralDeclaration_3()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEElementChangesType"


    // $ANTLR start "ruleEType"
    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2712:1: ruleEType returns [Enumerator current=null] : ( (enumLiteral_0= 'StringSingleline' ) | (enumLiteral_1= 'StringMultiline' ) | (enumLiteral_2= 'Integer' ) | (enumLiteral_3= 'Double' ) | (enumLiteral_4= 'Date' ) | (enumLiteral_5= 'Boolean' ) | (enumLiteral_6= 'Enumeration' ) | (enumLiteral_7= 'Custom' ) ) ;
    public final Enumerator ruleEType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;
        Token enumLiteral_6=null;
        Token enumLiteral_7=null;

         enterRule(); 
        try {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2714:28: ( ( (enumLiteral_0= 'StringSingleline' ) | (enumLiteral_1= 'StringMultiline' ) | (enumLiteral_2= 'Integer' ) | (enumLiteral_3= 'Double' ) | (enumLiteral_4= 'Date' ) | (enumLiteral_5= 'Boolean' ) | (enumLiteral_6= 'Enumeration' ) | (enumLiteral_7= 'Custom' ) ) )
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2715:1: ( (enumLiteral_0= 'StringSingleline' ) | (enumLiteral_1= 'StringMultiline' ) | (enumLiteral_2= 'Integer' ) | (enumLiteral_3= 'Double' ) | (enumLiteral_4= 'Date' ) | (enumLiteral_5= 'Boolean' ) | (enumLiteral_6= 'Enumeration' ) | (enumLiteral_7= 'Custom' ) )
            {
            // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2715:1: ( (enumLiteral_0= 'StringSingleline' ) | (enumLiteral_1= 'StringMultiline' ) | (enumLiteral_2= 'Integer' ) | (enumLiteral_3= 'Double' ) | (enumLiteral_4= 'Date' ) | (enumLiteral_5= 'Boolean' ) | (enumLiteral_6= 'Enumeration' ) | (enumLiteral_7= 'Custom' ) )
            int alt70=8;
            switch ( input.LA(1) ) {
            case 71:
                {
                alt70=1;
                }
                break;
            case 72:
                {
                alt70=2;
                }
                break;
            case 73:
                {
                alt70=3;
                }
                break;
            case 74:
                {
                alt70=4;
                }
                break;
            case 75:
                {
                alt70=5;
                }
                break;
            case 76:
                {
                alt70=6;
                }
                break;
            case 77:
                {
                alt70=7;
                }
                break;
            case 78:
                {
                alt70=8;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 70, 0, input);

                throw nvae;
            }

            switch (alt70) {
                case 1 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2715:2: (enumLiteral_0= 'StringSingleline' )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2715:2: (enumLiteral_0= 'StringSingleline' )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2715:4: enumLiteral_0= 'StringSingleline'
                    {
                    enumLiteral_0=(Token)match(input,71,FollowSets000.FOLLOW_71_in_ruleEType5887); 

                            current = grammarAccess.getETypeAccess().getStringSinglelineEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getETypeAccess().getStringSinglelineEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2721:6: (enumLiteral_1= 'StringMultiline' )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2721:6: (enumLiteral_1= 'StringMultiline' )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2721:8: enumLiteral_1= 'StringMultiline'
                    {
                    enumLiteral_1=(Token)match(input,72,FollowSets000.FOLLOW_72_in_ruleEType5904); 

                            current = grammarAccess.getETypeAccess().getStringMultilineEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getETypeAccess().getStringMultilineEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;
                case 3 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2727:6: (enumLiteral_2= 'Integer' )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2727:6: (enumLiteral_2= 'Integer' )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2727:8: enumLiteral_2= 'Integer'
                    {
                    enumLiteral_2=(Token)match(input,73,FollowSets000.FOLLOW_73_in_ruleEType5921); 

                            current = grammarAccess.getETypeAccess().getIntegerEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_2, grammarAccess.getETypeAccess().getIntegerEnumLiteralDeclaration_2()); 
                        

                    }


                    }
                    break;
                case 4 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2733:6: (enumLiteral_3= 'Double' )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2733:6: (enumLiteral_3= 'Double' )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2733:8: enumLiteral_3= 'Double'
                    {
                    enumLiteral_3=(Token)match(input,74,FollowSets000.FOLLOW_74_in_ruleEType5938); 

                            current = grammarAccess.getETypeAccess().getDoubleEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_3, grammarAccess.getETypeAccess().getDoubleEnumLiteralDeclaration_3()); 
                        

                    }


                    }
                    break;
                case 5 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2739:6: (enumLiteral_4= 'Date' )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2739:6: (enumLiteral_4= 'Date' )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2739:8: enumLiteral_4= 'Date'
                    {
                    enumLiteral_4=(Token)match(input,75,FollowSets000.FOLLOW_75_in_ruleEType5955); 

                            current = grammarAccess.getETypeAccess().getDateEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_4, grammarAccess.getETypeAccess().getDateEnumLiteralDeclaration_4()); 
                        

                    }


                    }
                    break;
                case 6 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2745:6: (enumLiteral_5= 'Boolean' )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2745:6: (enumLiteral_5= 'Boolean' )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2745:8: enumLiteral_5= 'Boolean'
                    {
                    enumLiteral_5=(Token)match(input,76,FollowSets000.FOLLOW_76_in_ruleEType5972); 

                            current = grammarAccess.getETypeAccess().getBooleanEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_5, grammarAccess.getETypeAccess().getBooleanEnumLiteralDeclaration_5()); 
                        

                    }


                    }
                    break;
                case 7 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2751:6: (enumLiteral_6= 'Enumeration' )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2751:6: (enumLiteral_6= 'Enumeration' )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2751:8: enumLiteral_6= 'Enumeration'
                    {
                    enumLiteral_6=(Token)match(input,77,FollowSets000.FOLLOW_77_in_ruleEType5989); 

                            current = grammarAccess.getETypeAccess().getEnumerationEnumLiteralDeclaration_6().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_6, grammarAccess.getETypeAccess().getEnumerationEnumLiteralDeclaration_6()); 
                        

                    }


                    }
                    break;
                case 8 :
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2757:6: (enumLiteral_7= 'Custom' )
                    {
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2757:6: (enumLiteral_7= 'Custom' )
                    // ../wis.xtext.WIS/src-gen/wis/xtext/parser/antlr/internal/InternalWIS.g:2757:8: enumLiteral_7= 'Custom'
                    {
                    enumLiteral_7=(Token)match(input,78,FollowSets000.FOLLOW_78_in_ruleEType6006); 

                            current = grammarAccess.getETypeAccess().getCustomEnumLiteralDeclaration_7().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_7, grammarAccess.getETypeAccess().getCustomEnumLiteralDeclaration_7()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEType"

    // Delegated rules


 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_ruleWModel_in_entryRuleWModel75 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleWModel85 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_11_in_ruleWModel132 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleWModel144 = new BitSet(new long[]{0x0000000000002000L});
        public static final BitSet FOLLOW_13_in_ruleWModel157 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWModel178 = new BitSet(new long[]{0x0000000000004000L});
        public static final BitSet FOLLOW_14_in_ruleWModel192 = new BitSet(new long[]{0x0400000000000040L});
        public static final BitSet FOLLOW_ruleEIntegerObject_in_ruleWModel213 = new BitSet(new long[]{0x0000000000038000L});
        public static final BitSet FOLLOW_15_in_ruleWModel227 = new BitSet(new long[]{0x000000C000000000L});
        public static final BitSet FOLLOW_ruleEBooleanObject_in_ruleWModel248 = new BitSet(new long[]{0x0000000000030000L});
        public static final BitSet FOLLOW_16_in_ruleWModel263 = new BitSet(new long[]{0x0400000000000040L});
        public static final BitSet FOLLOW_ruleEIntegerObject_in_ruleWModel284 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleWModel298 = new BitSet(new long[]{0x0000000000740002L});
        public static final BitSet FOLLOW_18_in_ruleWModel312 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleWModel324 = new BitSet(new long[]{0x0000000000800000L});
        public static final BitSet FOLLOW_ruleWPackage_in_ruleWModel345 = new BitSet(new long[]{0x00000000000A0000L});
        public static final BitSet FOLLOW_19_in_ruleWModel358 = new BitSet(new long[]{0x0000000000800000L});
        public static final BitSet FOLLOW_ruleWPackage_in_ruleWModel379 = new BitSet(new long[]{0x00000000000A0000L});
        public static final BitSet FOLLOW_17_in_ruleWModel393 = new BitSet(new long[]{0x0000000000700002L});
        public static final BitSet FOLLOW_20_in_ruleWModel408 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_ruleWReports_in_ruleWModel429 = new BitSet(new long[]{0x0000000000600002L});
        public static final BitSet FOLLOW_21_in_ruleWModel444 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleWModel456 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleWEnumerations_in_ruleWModel477 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleWModel489 = new BitSet(new long[]{0x0000000000400002L});
        public static final BitSet FOLLOW_22_in_ruleWModel504 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleWModel516 = new BitSet(new long[]{0x0000000000021000L});
        public static final BitSet FOLLOW_ruleWChanges_in_ruleWModel537 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleWModel549 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleWPackage_in_entryRuleWPackage587 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleWPackage597 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_23_in_ruleWPackage643 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWPackage664 = new BitSet(new long[]{0x0000000000001002L});
        public static final BitSet FOLLOW_12_in_ruleWPackage677 = new BitSet(new long[]{0x0000000001004000L});
        public static final BitSet FOLLOW_14_in_ruleWPackage690 = new BitSet(new long[]{0x0400000000000040L});
        public static final BitSet FOLLOW_ruleEIntegerObject_in_ruleWPackage711 = new BitSet(new long[]{0x0000000001000000L});
        public static final BitSet FOLLOW_24_in_ruleWPackage726 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleWPackage738 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleWEntity_in_ruleWPackage759 = new BitSet(new long[]{0x00000000000A0000L});
        public static final BitSet FOLLOW_19_in_ruleWPackage772 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleWEntity_in_ruleWPackage793 = new BitSet(new long[]{0x00000000000A0000L});
        public static final BitSet FOLLOW_17_in_ruleWPackage807 = new BitSet(new long[]{0x000000001E020000L});
        public static final BitSet FOLLOW_25_in_ruleWPackage821 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWPackage842 = new BitSet(new long[]{0x000000001C020000L});
        public static final BitSet FOLLOW_26_in_ruleWPackage857 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleWPackage869 = new BitSet(new long[]{0x0000060008000030L});
        public static final BitSet FOLLOW_ruleWLink_in_ruleWPackage890 = new BitSet(new long[]{0x00000000000A0000L});
        public static final BitSet FOLLOW_19_in_ruleWPackage903 = new BitSet(new long[]{0x0000060008000030L});
        public static final BitSet FOLLOW_ruleWLink_in_ruleWPackage924 = new BitSet(new long[]{0x00000000000A0000L});
        public static final BitSet FOLLOW_17_in_ruleWPackage938 = new BitSet(new long[]{0x0000000018020000L});
        public static final BitSet FOLLOW_27_in_ruleWPackage953 = new BitSet(new long[]{0x000000C000000000L});
        public static final BitSet FOLLOW_ruleEBooleanObject_in_ruleWPackage974 = new BitSet(new long[]{0x0000000010020000L});
        public static final BitSet FOLLOW_28_in_ruleWPackage989 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleWPackage1001 = new BitSet(new long[]{0x0000000000800000L});
        public static final BitSet FOLLOW_ruleWPackage_in_ruleWPackage1022 = new BitSet(new long[]{0x00000000000A0000L});
        public static final BitSet FOLLOW_19_in_ruleWPackage1035 = new BitSet(new long[]{0x0000000000800000L});
        public static final BitSet FOLLOW_ruleWPackage_in_ruleWPackage1056 = new BitSet(new long[]{0x00000000000A0000L});
        public static final BitSet FOLLOW_17_in_ruleWPackage1070 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleWPackage1084 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleWReports_in_entryRuleWReports1122 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleWReports1132 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_12_in_ruleWReports1178 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleWReport_in_ruleWReports1199 = new BitSet(new long[]{0x00000000000A0000L});
        public static final BitSet FOLLOW_19_in_ruleWReports1212 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleWReport_in_ruleWReports1233 = new BitSet(new long[]{0x00000000000A0000L});
        public static final BitSet FOLLOW_17_in_ruleWReports1247 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleWEnumerations_in_entryRuleWEnumerations1283 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleWEnumerations1293 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleWEnum_in_ruleWEnumerations1348 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleWChanges_in_entryRuleWChanges1384 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleWChanges1394 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleWChange_in_ruleWChanges1450 = new BitSet(new long[]{0x0000000000080002L});
        public static final BitSet FOLLOW_19_in_ruleWChanges1463 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_ruleWChange_in_ruleWChanges1484 = new BitSet(new long[]{0x0000000000080002L});
        public static final BitSet FOLLOW_ruleWChange_in_entryRuleWChange1524 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleWChange1534 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_12_in_ruleWChange1581 = new BitSet(new long[]{0x00000007E0024000L});
        public static final BitSet FOLLOW_14_in_ruleWChange1594 = new BitSet(new long[]{0x0400000000000040L});
        public static final BitSet FOLLOW_ruleEIntegerObject_in_ruleWChange1615 = new BitSet(new long[]{0x00000007E0020000L});
        public static final BitSet FOLLOW_29_in_ruleWChange1630 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWChange1651 = new BitSet(new long[]{0x00000007C0020000L});
        public static final BitSet FOLLOW_30_in_ruleWChange1666 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWChange1687 = new BitSet(new long[]{0x0000000780020000L});
        public static final BitSet FOLLOW_31_in_ruleWChange1702 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWChange1723 = new BitSet(new long[]{0x0000000700020000L});
        public static final BitSet FOLLOW_32_in_ruleWChange1738 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000007L});
        public static final BitSet FOLLOW_ruleEChangesType_in_ruleWChange1759 = new BitSet(new long[]{0x0000000600020000L});
        public static final BitSet FOLLOW_33_in_ruleWChange1774 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000078L});
        public static final BitSet FOLLOW_ruleEElementChangesType_in_ruleWChange1795 = new BitSet(new long[]{0x0000000400020000L});
        public static final BitSet FOLLOW_34_in_ruleWChange1810 = new BitSet(new long[]{0x0000003800800000L});
        public static final BitSet FOLLOW_35_in_ruleWChange1824 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleWChange1836 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleWAttribute_in_ruleWChange1857 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleWChange1869 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_36_in_ruleWChange1889 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleWChange1901 = new BitSet(new long[]{0x0000060008000030L});
        public static final BitSet FOLLOW_ruleWLink_in_ruleWChange1922 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleWChange1934 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_37_in_ruleWChange1954 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleWChange1966 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleWEntity_in_ruleWChange1987 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleWChange1999 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_23_in_ruleWChange2019 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleWChange2031 = new BitSet(new long[]{0x0000000000800000L});
        public static final BitSet FOLLOW_ruleWPackage_in_ruleWChange2052 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleWChange2064 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleWChange2080 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_entryRuleEString2118 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEString2129 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_ruleEString2169 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_ruleEString2195 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEBooleanObject_in_entryRuleEBooleanObject2241 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEBooleanObject2252 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_38_in_ruleEBooleanObject2290 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_39_in_ruleEBooleanObject2309 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleWEntity_in_entryRuleWEntity2349 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleWEntity2359 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWEntity2414 = new BitSet(new long[]{0x000001000A001002L});
        public static final BitSet FOLLOW_12_in_ruleWEntity2472 = new BitSet(new long[]{0x000001000A005002L});
        public static final BitSet FOLLOW_14_in_ruleWEntity2485 = new BitSet(new long[]{0x0400000000000040L});
        public static final BitSet FOLLOW_ruleEIntegerObject_in_ruleWEntity2506 = new BitSet(new long[]{0x000001000A001002L});
        public static final BitSet FOLLOW_25_in_ruleWEntity2576 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWEntity2597 = new BitSet(new long[]{0x000001000A001002L});
        public static final BitSet FOLLOW_27_in_ruleWEntity2665 = new BitSet(new long[]{0x000000C000000000L});
        public static final BitSet FOLLOW_ruleEBooleanObject_in_ruleWEntity2686 = new BitSet(new long[]{0x000001000A001002L});
        public static final BitSet FOLLOW_40_in_ruleWEntity2755 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleWEntity2767 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleWAttribute_in_ruleWEntity2788 = new BitSet(new long[]{0x00000000000A0000L});
        public static final BitSet FOLLOW_19_in_ruleWEntity2801 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleWAttribute_in_ruleWEntity2822 = new BitSet(new long[]{0x00000000000A0000L});
        public static final BitSet FOLLOW_17_in_ruleWEntity2836 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleWEntity2849 = new BitSet(new long[]{0x000001000A001002L});
        public static final BitSet FOLLOW_ruleWLink_in_entryRuleWLink2933 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleWLink2943 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWLink3034 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleWLink3046 = new BitSet(new long[]{0x000006000A004032L});
        public static final BitSet FOLLOW_14_in_ruleWLink3059 = new BitSet(new long[]{0x0400000000000040L});
        public static final BitSet FOLLOW_ruleEIntegerObject_in_ruleWLink3080 = new BitSet(new long[]{0x000006000A000032L});
        public static final BitSet FOLLOW_25_in_ruleWLink3095 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWLink3116 = new BitSet(new long[]{0x0000060008000032L});
        public static final BitSet FOLLOW_27_in_ruleWLink3186 = new BitSet(new long[]{0x000000C000000000L});
        public static final BitSet FOLLOW_ruleEBooleanObject_in_ruleWLink3207 = new BitSet(new long[]{0x0000060008000032L});
        public static final BitSet FOLLOW_41_in_ruleWLink3275 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleWConnection_in_ruleWLink3296 = new BitSet(new long[]{0x0000060008000032L});
        public static final BitSet FOLLOW_42_in_ruleWLink3365 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleWConnection_in_ruleWLink3386 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleWLink3399 = new BitSet(new long[]{0x0000060008000032L});
        public static final BitSet FOLLOW_ruleWAttribute_in_entryRuleWAttribute3481 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleWAttribute3491 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWAttribute3546 = new BitSet(new long[]{0x0000000000001002L});
        public static final BitSet FOLLOW_12_in_ruleWAttribute3559 = new BitSet(new long[]{0x000FF8016A024000L});
        public static final BitSet FOLLOW_14_in_ruleWAttribute3572 = new BitSet(new long[]{0x0400000000000040L});
        public static final BitSet FOLLOW_ruleEIntegerObject_in_ruleWAttribute3593 = new BitSet(new long[]{0x000FF8016A020000L});
        public static final BitSet FOLLOW_25_in_ruleWAttribute3608 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWAttribute3629 = new BitSet(new long[]{0x000FF80168020000L});
        public static final BitSet FOLLOW_27_in_ruleWAttribute3644 = new BitSet(new long[]{0x000000C000000000L});
        public static final BitSet FOLLOW_ruleEBooleanObject_in_ruleWAttribute3665 = new BitSet(new long[]{0x000FF80160020000L});
        public static final BitSet FOLLOW_43_in_ruleWAttribute3680 = new BitSet(new long[]{0x000000C000000000L});
        public static final BitSet FOLLOW_ruleEBooleanObject_in_ruleWAttribute3701 = new BitSet(new long[]{0x000FF00160020000L});
        public static final BitSet FOLLOW_44_in_ruleWAttribute3716 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWAttribute3737 = new BitSet(new long[]{0x000FE00160020000L});
        public static final BitSet FOLLOW_45_in_ruleWAttribute3752 = new BitSet(new long[]{0x000000C000000000L});
        public static final BitSet FOLLOW_ruleEBooleanObject_in_ruleWAttribute3773 = new BitSet(new long[]{0x000FC00160020000L});
        public static final BitSet FOLLOW_46_in_ruleWAttribute3788 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWAttribute3809 = new BitSet(new long[]{0x000F800160020000L});
        public static final BitSet FOLLOW_47_in_ruleWAttribute3824 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWAttribute3845 = new BitSet(new long[]{0x000F000160020000L});
        public static final BitSet FOLLOW_48_in_ruleWAttribute3860 = new BitSet(new long[]{0x000000C000000000L});
        public static final BitSet FOLLOW_ruleEBooleanObject_in_ruleWAttribute3881 = new BitSet(new long[]{0x000E000160020000L});
        public static final BitSet FOLLOW_32_in_ruleWAttribute3896 = new BitSet(new long[]{0x0000000000000000L,0x0000000000007F80L});
        public static final BitSet FOLLOW_ruleEType_in_ruleWAttribute3917 = new BitSet(new long[]{0x000E000060020000L});
        public static final BitSet FOLLOW_49_in_ruleWAttribute3932 = new BitSet(new long[]{0x0400000000000040L});
        public static final BitSet FOLLOW_ruleEIntegerObject_in_ruleWAttribute3953 = new BitSet(new long[]{0x000C000060020000L});
        public static final BitSet FOLLOW_50_in_ruleWAttribute3968 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWAttribute3991 = new BitSet(new long[]{0x0008000060020000L});
        public static final BitSet FOLLOW_51_in_ruleWAttribute4006 = new BitSet(new long[]{0x000000C000000000L});
        public static final BitSet FOLLOW_ruleEBooleanObject_in_ruleWAttribute4027 = new BitSet(new long[]{0x0000000060020000L});
        public static final BitSet FOLLOW_29_in_ruleWAttribute4042 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWAttribute4063 = new BitSet(new long[]{0x0000000040020000L});
        public static final BitSet FOLLOW_30_in_ruleWAttribute4078 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWAttribute4099 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleWAttribute4113 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleWEnum_in_entryRuleWEnum4151 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleWEnum4161 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWEnum4216 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleWEnum4228 = new BitSet(new long[]{0x0030000000000000L});
        public static final BitSet FOLLOW_52_in_ruleWEnum4241 = new BitSet(new long[]{0xC000000000000000L});
        public static final BitSet FOLLOW_ruleEEnumRepresentation_in_ruleWEnum4262 = new BitSet(new long[]{0x0020000000000000L});
        public static final BitSet FOLLOW_53_in_ruleWEnum4277 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleWEnum4289 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleWEnumValue_in_ruleWEnum4310 = new BitSet(new long[]{0x00000000000A0000L});
        public static final BitSet FOLLOW_19_in_ruleWEnum4323 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleWEnumValue_in_ruleWEnum4344 = new BitSet(new long[]{0x00000000000A0000L});
        public static final BitSet FOLLOW_17_in_ruleWEnum4358 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleWEnum4371 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleWEnumValue_in_entryRuleWEnumValue4407 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleWEnumValue4417 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWEnumValue4472 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleWConnection_in_entryRuleWConnection4508 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleWConnection4518 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWConnection4575 = new BitSet(new long[]{0x0040000000000000L});
        public static final BitSet FOLLOW_54_in_ruleWConnection4587 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWConnection4608 = new BitSet(new long[]{0x0000000000001002L});
        public static final BitSet FOLLOW_12_in_ruleWConnection4621 = new BitSet(new long[]{0x0380000002020000L});
        public static final BitSet FOLLOW_25_in_ruleWConnection4634 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWConnection4655 = new BitSet(new long[]{0x0380000000020000L});
        public static final BitSet FOLLOW_55_in_ruleWConnection4670 = new BitSet(new long[]{0x1800000000000000L});
        public static final BitSet FOLLOW_ruleECardinalityMin_in_ruleWConnection4691 = new BitSet(new long[]{0x0300000000020000L});
        public static final BitSet FOLLOW_56_in_ruleWConnection4706 = new BitSet(new long[]{0x3000000000000000L});
        public static final BitSet FOLLOW_ruleECardinalityMax_in_ruleWConnection4727 = new BitSet(new long[]{0x0200000000020000L});
        public static final BitSet FOLLOW_57_in_ruleWConnection4742 = new BitSet(new long[]{0x000000C000000000L});
        public static final BitSet FOLLOW_ruleEBooleanObject_in_ruleWConnection4763 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleWConnection4777 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleWReport_in_entryRuleWReport4815 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleWReport4825 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWReport4880 = new BitSet(new long[]{0x000001000A001002L});
        public static final BitSet FOLLOW_12_in_ruleWReport4938 = new BitSet(new long[]{0x000001000A005002L});
        public static final BitSet FOLLOW_14_in_ruleWReport4951 = new BitSet(new long[]{0x0400000000000040L});
        public static final BitSet FOLLOW_ruleEIntegerObject_in_ruleWReport4972 = new BitSet(new long[]{0x000001000A001002L});
        public static final BitSet FOLLOW_25_in_ruleWReport5042 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleWReport5063 = new BitSet(new long[]{0x000001000A001002L});
        public static final BitSet FOLLOW_27_in_ruleWReport5131 = new BitSet(new long[]{0x000000C000000000L});
        public static final BitSet FOLLOW_ruleEBooleanObject_in_ruleWReport5152 = new BitSet(new long[]{0x000001000A001002L});
        public static final BitSet FOLLOW_40_in_ruleWReport5221 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleWReport5233 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleWAttribute_in_ruleWReport5254 = new BitSet(new long[]{0x00000000000A0000L});
        public static final BitSet FOLLOW_19_in_ruleWReport5267 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleWAttribute_in_ruleWReport5288 = new BitSet(new long[]{0x00000000000A0000L});
        public static final BitSet FOLLOW_17_in_ruleWReport5302 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleWReport5315 = new BitSet(new long[]{0x000001000A001002L});
        public static final BitSet FOLLOW_ruleEIntegerObject_in_entryRuleEIntegerObject5400 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEIntegerObject5411 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_58_in_ruleEIntegerObject5450 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_RULE_INT_in_ruleEIntegerObject5467 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_59_in_ruleECardinalityMin5526 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_60_in_ruleECardinalityMin5543 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_60_in_ruleECardinalityMax5588 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_61_in_ruleECardinalityMax5605 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_62_in_ruleEEnumRepresentation5650 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_63_in_ruleEEnumRepresentation5667 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_64_in_ruleEChangesType5712 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_65_in_ruleEChangesType5729 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_66_in_ruleEChangesType5746 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_67_in_ruleEElementChangesType5791 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_68_in_ruleEElementChangesType5808 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_69_in_ruleEElementChangesType5825 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_70_in_ruleEElementChangesType5842 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_71_in_ruleEType5887 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_72_in_ruleEType5904 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_73_in_ruleEType5921 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_74_in_ruleEType5938 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_75_in_ruleEType5955 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_76_in_ruleEType5972 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_77_in_ruleEType5989 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_78_in_ruleEType6006 = new BitSet(new long[]{0x0000000000000002L});
    }


}