/*
 * generated by Xtext
 */
package wis.xtext.ui;

import org.eclipse.xtext.ui.guice.AbstractGuiceAwareExecutableExtensionFactory;
import org.osgi.framework.Bundle;

import com.google.inject.Injector;

import wis.xtext.ui.internal.WISActivator;

/**
 * This class was generated. Customizations should only happen in a newly
 * introduced subclass. 
 */
public class WISExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	protected Bundle getBundle() {
		return WISActivator.getInstance().getBundle();
	}
	
	@Override
	protected Injector getInjector() {
		return WISActivator.getInstance().getInjector(WISActivator.WIS_XTEXT_WIS);
	}
	
}
