/*
 * generated by Xtext
 */
package wis.xtext.ui.contentassist.antlr;

import java.util.Collection;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.RecognitionException;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.ui.editor.contentassist.antlr.AbstractContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.FollowElement;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;

import com.google.inject.Inject;

import wis.xtext.services.WISGrammarAccess;

public class WISParser extends AbstractContentAssistParser {
	
	@Inject
	private WISGrammarAccess grammarAccess;
	
	private Map<AbstractElement, String> nameMappings;
	
	@Override
	protected wis.xtext.ui.contentassist.antlr.internal.InternalWISParser createParser() {
		wis.xtext.ui.contentassist.antlr.internal.InternalWISParser result = new wis.xtext.ui.contentassist.antlr.internal.InternalWISParser(null);
		result.setGrammarAccess(grammarAccess);
		return result;
	}
	
	@Override
	protected String getRuleName(AbstractElement element) {
		if (nameMappings == null) {
			nameMappings = new HashMap<AbstractElement, String>() {
				private static final long serialVersionUID = 1L;
				{
					put(grammarAccess.getWChangeAccess().getAlternatives_1_7_1(), "rule__WChange__Alternatives_1_7_1");
					put(grammarAccess.getEStringAccess().getAlternatives(), "rule__EString__Alternatives");
					put(grammarAccess.getEBooleanObjectAccess().getAlternatives(), "rule__EBooleanObject__Alternatives");
					put(grammarAccess.getECardinalityMinAccess().getAlternatives(), "rule__ECardinalityMin__Alternatives");
					put(grammarAccess.getECardinalityMaxAccess().getAlternatives(), "rule__ECardinalityMax__Alternatives");
					put(grammarAccess.getEEnumRepresentationAccess().getAlternatives(), "rule__EEnumRepresentation__Alternatives");
					put(grammarAccess.getEChangesTypeAccess().getAlternatives(), "rule__EChangesType__Alternatives");
					put(grammarAccess.getEElementChangesTypeAccess().getAlternatives(), "rule__EElementChangesType__Alternatives");
					put(grammarAccess.getETypeAccess().getAlternatives(), "rule__EType__Alternatives");
					put(grammarAccess.getWModelAccess().getGroup(), "rule__WModel__Group__0");
					put(grammarAccess.getWModelAccess().getGroup_1(), "rule__WModel__Group_1__0");
					put(grammarAccess.getWModelAccess().getGroup_1_2(), "rule__WModel__Group_1_2__0");
					put(grammarAccess.getWModelAccess().getGroup_1_3(), "rule__WModel__Group_1_3__0");
					put(grammarAccess.getWModelAccess().getGroup_1_4(), "rule__WModel__Group_1_4__0");
					put(grammarAccess.getWModelAccess().getGroup_1_5(), "rule__WModel__Group_1_5__0");
					put(grammarAccess.getWModelAccess().getGroup_2(), "rule__WModel__Group_2__0");
					put(grammarAccess.getWModelAccess().getGroup_2_3(), "rule__WModel__Group_2_3__0");
					put(grammarAccess.getWModelAccess().getGroup_3(), "rule__WModel__Group_3__0");
					put(grammarAccess.getWModelAccess().getGroup_4(), "rule__WModel__Group_4__0");
					put(grammarAccess.getWModelAccess().getGroup_5(), "rule__WModel__Group_5__0");
					put(grammarAccess.getWPackageAccess().getGroup(), "rule__WPackage__Group__0");
					put(grammarAccess.getWPackageAccess().getGroup_3(), "rule__WPackage__Group_3__0");
					put(grammarAccess.getWPackageAccess().getGroup_3_1(), "rule__WPackage__Group_3_1__0");
					put(grammarAccess.getWPackageAccess().getGroup_3_2(), "rule__WPackage__Group_3_2__0");
					put(grammarAccess.getWPackageAccess().getGroup_3_2_3(), "rule__WPackage__Group_3_2_3__0");
					put(grammarAccess.getWPackageAccess().getGroup_3_3(), "rule__WPackage__Group_3_3__0");
					put(grammarAccess.getWPackageAccess().getGroup_3_4(), "rule__WPackage__Group_3_4__0");
					put(grammarAccess.getWPackageAccess().getGroup_3_4_3(), "rule__WPackage__Group_3_4_3__0");
					put(grammarAccess.getWPackageAccess().getGroup_3_5(), "rule__WPackage__Group_3_5__0");
					put(grammarAccess.getWPackageAccess().getGroup_3_6(), "rule__WPackage__Group_3_6__0");
					put(grammarAccess.getWPackageAccess().getGroup_3_6_3(), "rule__WPackage__Group_3_6_3__0");
					put(grammarAccess.getWReportsAccess().getGroup(), "rule__WReports__Group__0");
					put(grammarAccess.getWReportsAccess().getGroup_3(), "rule__WReports__Group_3__0");
					put(grammarAccess.getWEnumerationsAccess().getGroup(), "rule__WEnumerations__Group__0");
					put(grammarAccess.getWChangesAccess().getGroup(), "rule__WChanges__Group__0");
					put(grammarAccess.getWChangesAccess().getGroup_1(), "rule__WChanges__Group_1__0");
					put(grammarAccess.getWChangesAccess().getGroup_1_1(), "rule__WChanges__Group_1_1__0");
					put(grammarAccess.getWChangeAccess().getGroup(), "rule__WChange__Group__0");
					put(grammarAccess.getWChangeAccess().getGroup_1(), "rule__WChange__Group_1__0");
					put(grammarAccess.getWChangeAccess().getGroup_1_1(), "rule__WChange__Group_1_1__0");
					put(grammarAccess.getWChangeAccess().getGroup_1_2(), "rule__WChange__Group_1_2__0");
					put(grammarAccess.getWChangeAccess().getGroup_1_3(), "rule__WChange__Group_1_3__0");
					put(grammarAccess.getWChangeAccess().getGroup_1_4(), "rule__WChange__Group_1_4__0");
					put(grammarAccess.getWChangeAccess().getGroup_1_5(), "rule__WChange__Group_1_5__0");
					put(grammarAccess.getWChangeAccess().getGroup_1_6(), "rule__WChange__Group_1_6__0");
					put(grammarAccess.getWChangeAccess().getGroup_1_7(), "rule__WChange__Group_1_7__0");
					put(grammarAccess.getWChangeAccess().getGroup_1_7_1_0(), "rule__WChange__Group_1_7_1_0__0");
					put(grammarAccess.getWChangeAccess().getGroup_1_7_1_1(), "rule__WChange__Group_1_7_1_1__0");
					put(grammarAccess.getWChangeAccess().getGroup_1_7_1_2(), "rule__WChange__Group_1_7_1_2__0");
					put(grammarAccess.getWChangeAccess().getGroup_1_7_1_3(), "rule__WChange__Group_1_7_1_3__0");
					put(grammarAccess.getWEntityAccess().getGroup(), "rule__WEntity__Group__0");
					put(grammarAccess.getWEntityAccess().getGroup_2_0(), "rule__WEntity__Group_2_0__0");
					put(grammarAccess.getWEntityAccess().getGroup_2_0_1(), "rule__WEntity__Group_2_0_1__0");
					put(grammarAccess.getWEntityAccess().getGroup_2_1(), "rule__WEntity__Group_2_1__0");
					put(grammarAccess.getWEntityAccess().getGroup_2_2(), "rule__WEntity__Group_2_2__0");
					put(grammarAccess.getWEntityAccess().getGroup_2_3(), "rule__WEntity__Group_2_3__0");
					put(grammarAccess.getWEntityAccess().getGroup_2_3_0(), "rule__WEntity__Group_2_3_0__0");
					put(grammarAccess.getWEntityAccess().getGroup_2_3_0_3(), "rule__WEntity__Group_2_3_0_3__0");
					put(grammarAccess.getWLinkAccess().getGroup_0(), "rule__WLink__Group_0__0");
					put(grammarAccess.getWLinkAccess().getGroup_0_2(), "rule__WLink__Group_0_2__0");
					put(grammarAccess.getWLinkAccess().getGroup_0_3(), "rule__WLink__Group_0_3__0");
					put(grammarAccess.getWLinkAccess().getGroup_1(), "rule__WLink__Group_1__0");
					put(grammarAccess.getWLinkAccess().getGroup_2(), "rule__WLink__Group_2__0");
					put(grammarAccess.getWLinkAccess().getGroup_3(), "rule__WLink__Group_3__0");
					put(grammarAccess.getWLinkAccess().getGroup_3_0(), "rule__WLink__Group_3_0__0");
					put(grammarAccess.getWAttributeAccess().getGroup(), "rule__WAttribute__Group__0");
					put(grammarAccess.getWAttributeAccess().getGroup_2(), "rule__WAttribute__Group_2__0");
					put(grammarAccess.getWAttributeAccess().getGroup_2_1(), "rule__WAttribute__Group_2_1__0");
					put(grammarAccess.getWAttributeAccess().getGroup_2_2(), "rule__WAttribute__Group_2_2__0");
					put(grammarAccess.getWAttributeAccess().getGroup_2_3(), "rule__WAttribute__Group_2_3__0");
					put(grammarAccess.getWAttributeAccess().getGroup_2_4(), "rule__WAttribute__Group_2_4__0");
					put(grammarAccess.getWAttributeAccess().getGroup_2_5(), "rule__WAttribute__Group_2_5__0");
					put(grammarAccess.getWAttributeAccess().getGroup_2_6(), "rule__WAttribute__Group_2_6__0");
					put(grammarAccess.getWAttributeAccess().getGroup_2_7(), "rule__WAttribute__Group_2_7__0");
					put(grammarAccess.getWAttributeAccess().getGroup_2_8(), "rule__WAttribute__Group_2_8__0");
					put(grammarAccess.getWAttributeAccess().getGroup_2_9(), "rule__WAttribute__Group_2_9__0");
					put(grammarAccess.getWAttributeAccess().getGroup_2_10(), "rule__WAttribute__Group_2_10__0");
					put(grammarAccess.getWAttributeAccess().getGroup_2_11(), "rule__WAttribute__Group_2_11__0");
					put(grammarAccess.getWAttributeAccess().getGroup_2_12(), "rule__WAttribute__Group_2_12__0");
					put(grammarAccess.getWAttributeAccess().getGroup_2_13(), "rule__WAttribute__Group_2_13__0");
					put(grammarAccess.getWAttributeAccess().getGroup_2_14(), "rule__WAttribute__Group_2_14__0");
					put(grammarAccess.getWAttributeAccess().getGroup_2_15(), "rule__WAttribute__Group_2_15__0");
					put(grammarAccess.getWEnumAccess().getGroup(), "rule__WEnum__Group__0");
					put(grammarAccess.getWEnumAccess().getGroup_3(), "rule__WEnum__Group_3__0");
					put(grammarAccess.getWEnumAccess().getGroup_4(), "rule__WEnum__Group_4__0");
					put(grammarAccess.getWEnumAccess().getGroup_4_3(), "rule__WEnum__Group_4_3__0");
					put(grammarAccess.getWEnumValueAccess().getGroup(), "rule__WEnumValue__Group__0");
					put(grammarAccess.getWConnectionAccess().getGroup(), "rule__WConnection__Group__0");
					put(grammarAccess.getWConnectionAccess().getGroup_4(), "rule__WConnection__Group_4__0");
					put(grammarAccess.getWConnectionAccess().getGroup_4_1(), "rule__WConnection__Group_4_1__0");
					put(grammarAccess.getWConnectionAccess().getGroup_4_2(), "rule__WConnection__Group_4_2__0");
					put(grammarAccess.getWConnectionAccess().getGroup_4_3(), "rule__WConnection__Group_4_3__0");
					put(grammarAccess.getWConnectionAccess().getGroup_4_4(), "rule__WConnection__Group_4_4__0");
					put(grammarAccess.getWReportAccess().getGroup(), "rule__WReport__Group__0");
					put(grammarAccess.getWReportAccess().getGroup_2_0(), "rule__WReport__Group_2_0__0");
					put(grammarAccess.getWReportAccess().getGroup_2_0_1(), "rule__WReport__Group_2_0_1__0");
					put(grammarAccess.getWReportAccess().getGroup_2_1(), "rule__WReport__Group_2_1__0");
					put(grammarAccess.getWReportAccess().getGroup_2_2(), "rule__WReport__Group_2_2__0");
					put(grammarAccess.getWReportAccess().getGroup_2_3(), "rule__WReport__Group_2_3__0");
					put(grammarAccess.getWReportAccess().getGroup_2_3_0(), "rule__WReport__Group_2_3_0__0");
					put(grammarAccess.getWReportAccess().getGroup_2_3_0_3(), "rule__WReport__Group_2_3_0_3__0");
					put(grammarAccess.getEIntegerObjectAccess().getGroup(), "rule__EIntegerObject__Group__0");
					put(grammarAccess.getWModelAccess().getNameAssignment_1_2_1(), "rule__WModel__NameAssignment_1_2_1");
					put(grammarAccess.getWModelAccess().getVersionAssignment_1_3_1(), "rule__WModel__VersionAssignment_1_3_1");
					put(grammarAccess.getWModelAccess().getDebugAssignment_1_4_1(), "rule__WModel__DebugAssignment_1_4_1");
					put(grammarAccess.getWModelAccess().getDebugVersionAssignment_1_5_1(), "rule__WModel__DebugVersionAssignment_1_5_1");
					put(grammarAccess.getWModelAccess().getPackagesAssignment_2_2(), "rule__WModel__PackagesAssignment_2_2");
					put(grammarAccess.getWModelAccess().getPackagesAssignment_2_3_1(), "rule__WModel__PackagesAssignment_2_3_1");
					put(grammarAccess.getWModelAccess().getReportsContainerAssignment_3_1(), "rule__WModel__ReportsContainerAssignment_3_1");
					put(grammarAccess.getWModelAccess().getEnumerationsAssignment_4_2(), "rule__WModel__EnumerationsAssignment_4_2");
					put(grammarAccess.getWModelAccess().getChangesContainerAssignment_5_2(), "rule__WModel__ChangesContainerAssignment_5_2");
					put(grammarAccess.getWPackageAccess().getNameAssignment_2(), "rule__WPackage__NameAssignment_2");
					put(grammarAccess.getWPackageAccess().getVersionAssignment_3_1_1(), "rule__WPackage__VersionAssignment_3_1_1");
					put(grammarAccess.getWPackageAccess().getEntitiesAssignment_3_2_2(), "rule__WPackage__EntitiesAssignment_3_2_2");
					put(grammarAccess.getWPackageAccess().getEntitiesAssignment_3_2_3_1(), "rule__WPackage__EntitiesAssignment_3_2_3_1");
					put(grammarAccess.getWPackageAccess().getLabelAssignment_3_3_1(), "rule__WPackage__LabelAssignment_3_3_1");
					put(grammarAccess.getWPackageAccess().getLinksAssignment_3_4_2(), "rule__WPackage__LinksAssignment_3_4_2");
					put(grammarAccess.getWPackageAccess().getLinksAssignment_3_4_3_1(), "rule__WPackage__LinksAssignment_3_4_3_1");
					put(grammarAccess.getWPackageAccess().getGenerateAssignment_3_5_1(), "rule__WPackage__GenerateAssignment_3_5_1");
					put(grammarAccess.getWPackageAccess().getSubpackagesAssignment_3_6_2(), "rule__WPackage__SubpackagesAssignment_3_6_2");
					put(grammarAccess.getWPackageAccess().getSubpackagesAssignment_3_6_3_1(), "rule__WPackage__SubpackagesAssignment_3_6_3_1");
					put(grammarAccess.getWReportsAccess().getReportsAssignment_2(), "rule__WReports__ReportsAssignment_2");
					put(grammarAccess.getWReportsAccess().getReportsAssignment_3_1(), "rule__WReports__ReportsAssignment_3_1");
					put(grammarAccess.getWEnumerationsAccess().getEnumerationsAssignment_1(), "rule__WEnumerations__EnumerationsAssignment_1");
					put(grammarAccess.getWChangesAccess().getChangesAssignment_1_0(), "rule__WChanges__ChangesAssignment_1_0");
					put(grammarAccess.getWChangesAccess().getChangesAssignment_1_1_1(), "rule__WChanges__ChangesAssignment_1_1_1");
					put(grammarAccess.getWChangeAccess().getVersionAssignment_1_1_1(), "rule__WChange__VersionAssignment_1_1_1");
					put(grammarAccess.getWChangeAccess().getMessageAssignment_1_2_1(), "rule__WChange__MessageAssignment_1_2_1");
					put(grammarAccess.getWChangeAccess().getMessageDevAssignment_1_3_1(), "rule__WChange__MessageDevAssignment_1_3_1");
					put(grammarAccess.getWChangeAccess().getPathAssignment_1_4_1(), "rule__WChange__PathAssignment_1_4_1");
					put(grammarAccess.getWChangeAccess().getTypeAssignment_1_5_1(), "rule__WChange__TypeAssignment_1_5_1");
					put(grammarAccess.getWChangeAccess().getElementTypeAssignment_1_6_1(), "rule__WChange__ElementTypeAssignment_1_6_1");
					put(grammarAccess.getWChangeAccess().getElementAssignment_1_7_1_0_2(), "rule__WChange__ElementAssignment_1_7_1_0_2");
					put(grammarAccess.getWChangeAccess().getElementAssignment_1_7_1_1_2(), "rule__WChange__ElementAssignment_1_7_1_1_2");
					put(grammarAccess.getWChangeAccess().getElementAssignment_1_7_1_2_2(), "rule__WChange__ElementAssignment_1_7_1_2_2");
					put(grammarAccess.getWChangeAccess().getElementAssignment_1_7_1_3_2(), "rule__WChange__ElementAssignment_1_7_1_3_2");
					put(grammarAccess.getWEntityAccess().getNameAssignment_1(), "rule__WEntity__NameAssignment_1");
					put(grammarAccess.getWEntityAccess().getVersionAssignment_2_0_1_1(), "rule__WEntity__VersionAssignment_2_0_1_1");
					put(grammarAccess.getWEntityAccess().getLabelAssignment_2_1_1(), "rule__WEntity__LabelAssignment_2_1_1");
					put(grammarAccess.getWEntityAccess().getGenerateAssignment_2_2_1(), "rule__WEntity__GenerateAssignment_2_2_1");
					put(grammarAccess.getWEntityAccess().getAttributesAssignment_2_3_0_2(), "rule__WEntity__AttributesAssignment_2_3_0_2");
					put(grammarAccess.getWEntityAccess().getAttributesAssignment_2_3_0_3_1(), "rule__WEntity__AttributesAssignment_2_3_0_3_1");
					put(grammarAccess.getWLinkAccess().getNameAssignment_0_0(), "rule__WLink__NameAssignment_0_0");
					put(grammarAccess.getWLinkAccess().getVersionAssignment_0_2_1(), "rule__WLink__VersionAssignment_0_2_1");
					put(grammarAccess.getWLinkAccess().getLabelAssignment_0_3_1(), "rule__WLink__LabelAssignment_0_3_1");
					put(grammarAccess.getWLinkAccess().getGenerateAssignment_1_1(), "rule__WLink__GenerateAssignment_1_1");
					put(grammarAccess.getWLinkAccess().getFromAssignment_2_1(), "rule__WLink__FromAssignment_2_1");
					put(grammarAccess.getWLinkAccess().getToAssignment_3_0_1(), "rule__WLink__ToAssignment_3_0_1");
					put(grammarAccess.getWAttributeAccess().getNameAssignment_1(), "rule__WAttribute__NameAssignment_1");
					put(grammarAccess.getWAttributeAccess().getVersionAssignment_2_1_1(), "rule__WAttribute__VersionAssignment_2_1_1");
					put(grammarAccess.getWAttributeAccess().getLabelAssignment_2_2_1(), "rule__WAttribute__LabelAssignment_2_2_1");
					put(grammarAccess.getWAttributeAccess().getGenerateAssignment_2_3_1(), "rule__WAttribute__GenerateAssignment_2_3_1");
					put(grammarAccess.getWAttributeAccess().getIsPublicAssignment_2_4_1(), "rule__WAttribute__IsPublicAssignment_2_4_1");
					put(grammarAccess.getWAttributeAccess().getPlaceholderAssignment_2_5_1(), "rule__WAttribute__PlaceholderAssignment_2_5_1");
					put(grammarAccess.getWAttributeAccess().getImportantAssignment_2_6_1(), "rule__WAttribute__ImportantAssignment_2_6_1");
					put(grammarAccess.getWAttributeAccess().getValidationRegexAssignment_2_7_1(), "rule__WAttribute__ValidationRegexAssignment_2_7_1");
					put(grammarAccess.getWAttributeAccess().getValidationMessageAssignment_2_8_1(), "rule__WAttribute__ValidationMessageAssignment_2_8_1");
					put(grammarAccess.getWAttributeAccess().getAllowNullAssignment_2_9_1(), "rule__WAttribute__AllowNullAssignment_2_9_1");
					put(grammarAccess.getWAttributeAccess().getTypeAssignment_2_10_1(), "rule__WAttribute__TypeAssignment_2_10_1");
					put(grammarAccess.getWAttributeAccess().getLengthAssignment_2_11_1(), "rule__WAttribute__LengthAssignment_2_11_1");
					put(grammarAccess.getWAttributeAccess().getEnumerationAssignment_2_12_1(), "rule__WAttribute__EnumerationAssignment_2_12_1");
					put(grammarAccess.getWAttributeAccess().getDisabledAssignment_2_13_1(), "rule__WAttribute__DisabledAssignment_2_13_1");
					put(grammarAccess.getWAttributeAccess().getMessageAssignment_2_14_1(), "rule__WAttribute__MessageAssignment_2_14_1");
					put(grammarAccess.getWAttributeAccess().getMessageDevAssignment_2_15_1(), "rule__WAttribute__MessageDevAssignment_2_15_1");
					put(grammarAccess.getWEnumAccess().getNameAssignment_1(), "rule__WEnum__NameAssignment_1");
					put(grammarAccess.getWEnumAccess().getRepresentationAssignment_3_1(), "rule__WEnum__RepresentationAssignment_3_1");
					put(grammarAccess.getWEnumAccess().getElementsAssignment_4_2(), "rule__WEnum__ElementsAssignment_4_2");
					put(grammarAccess.getWEnumAccess().getElementsAssignment_4_3_1(), "rule__WEnum__ElementsAssignment_4_3_1");
					put(grammarAccess.getWEnumValueAccess().getNameAssignment_1(), "rule__WEnumValue__NameAssignment_1");
					put(grammarAccess.getWConnectionAccess().getEntityAssignment_1(), "rule__WConnection__EntityAssignment_1");
					put(grammarAccess.getWConnectionAccess().getNameAssignment_3(), "rule__WConnection__NameAssignment_3");
					put(grammarAccess.getWConnectionAccess().getLabelAssignment_4_1_1(), "rule__WConnection__LabelAssignment_4_1_1");
					put(grammarAccess.getWConnectionAccess().getMinAssignment_4_2_1(), "rule__WConnection__MinAssignment_4_2_1");
					put(grammarAccess.getWConnectionAccess().getMaxAssignment_4_3_1(), "rule__WConnection__MaxAssignment_4_3_1");
					put(grammarAccess.getWConnectionAccess().getEditableAssignment_4_4_1(), "rule__WConnection__EditableAssignment_4_4_1");
					put(grammarAccess.getWReportAccess().getNameAssignment_1(), "rule__WReport__NameAssignment_1");
					put(grammarAccess.getWReportAccess().getVersionAssignment_2_0_1_1(), "rule__WReport__VersionAssignment_2_0_1_1");
					put(grammarAccess.getWReportAccess().getLabelAssignment_2_1_1(), "rule__WReport__LabelAssignment_2_1_1");
					put(grammarAccess.getWReportAccess().getGenerateAssignment_2_2_1(), "rule__WReport__GenerateAssignment_2_2_1");
					put(grammarAccess.getWReportAccess().getAttributesAssignment_2_3_0_2(), "rule__WReport__AttributesAssignment_2_3_0_2");
					put(grammarAccess.getWReportAccess().getAttributesAssignment_2_3_0_3_1(), "rule__WReport__AttributesAssignment_2_3_0_3_1");
					put(grammarAccess.getWEntityAccess().getUnorderedGroup_2(), "rule__WEntity__UnorderedGroup_2");
					put(grammarAccess.getWLinkAccess().getUnorderedGroup(), "rule__WLink__UnorderedGroup");
					put(grammarAccess.getWReportAccess().getUnorderedGroup_2(), "rule__WReport__UnorderedGroup_2");
				}
			};
		}
		return nameMappings.get(element);
	}
	
	@Override
	protected Collection<FollowElement> getFollowElements(AbstractInternalContentAssistParser parser) {
		try {
			wis.xtext.ui.contentassist.antlr.internal.InternalWISParser typedParser = (wis.xtext.ui.contentassist.antlr.internal.InternalWISParser) parser;
			typedParser.entryRuleWModel();
			return typedParser.getFollowElements();
		} catch(RecognitionException ex) {
			throw new RuntimeException(ex);
		}		
	}
	
	@Override
	protected String[] getInitialHiddenTokens() {
		return new String[] { "RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT" };
	}
	
	public WISGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}
	
	public void setGrammarAccess(WISGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
}
