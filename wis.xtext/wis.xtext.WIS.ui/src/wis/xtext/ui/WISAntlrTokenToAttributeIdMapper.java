package wis.xtext.ui;

import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultAntlrTokenToAttributeIdMapper;

public class WISAntlrTokenToAttributeIdMapper extends
		DefaultAntlrTokenToAttributeIdMapper {
	
	@Override
	protected String calculateId(String tokenName, int tokenType) {
		if( "'packages'".equals(tokenName)){
			return "packages";
		}else if("'reports'".equals(tokenName)){
			return "reports";
		}else if("'enumerations'".equals(tokenName)){
			return "enumerations";
		}else if("'package'".equals(tokenName)){
			return "package";
		}else if("'entities'".equals(tokenName)){
			return "entities";
		}else if("'label'".equals(tokenName)){
			return "label";
		}else if("'links'".equals(tokenName)){
			return "links";
		}else if("'generate'".equals(tokenName)){
			return "generate";
		}else if("'subpackages'".equals(tokenName)){
			return "subpackages";
		}else if("'true'".equals(tokenName)){
			return "true";
		}else if("'false'".equals(tokenName)){
			return "false";
		}else if("'attributes'".equals(tokenName)){
			return "attributes";
		}else if("'from'".equals(tokenName)){
			return "from";
		}else if("'to'".equals(tokenName)){
			return "to";
		}else if("'max'".equals(tokenName)){
			return "max";
		}else if("'min'".equals(tokenName)){
			return "min";
		}else if("'isPublic'".equals(tokenName)){
			return "isPublic";
		}else if("'placeholder'".equals(tokenName)){
			return "placeholder";
		}else if("'important'".equals(tokenName)){
			return "important";
		}else if("'validationRegex'".equals(tokenName)){
			return "validationRegex";
		}else if("'validationMessage'".equals(tokenName)){
			return "validationMessage";
		}else if("'allowNull'".equals(tokenName)){
			return "allowNull";
		}else if("'type'".equals(tokenName)){
			return "type";
		}else if("'length'".equals(tokenName)){
			return "length";
		}else if("'enumeration'".equals(tokenName)){
			return "enumeration";
		}else if("'StringSingleline'".equals(tokenName)){
			return "StringSingleline";
		}else if("'StringMultiline'".equals(tokenName)){
			return "StringMultiline";
		}else if("'Integer'".equals(tokenName)){
			return "Integer";
		}else if("'Double'".equals(tokenName)){
			return "Double";
		}else if("'Date'".equals(tokenName)){
			return "Date";
		}else if("'Boolean'".equals(tokenName)){
			return "Boolean";
		}else if("'Enumeration'".equals(tokenName)){
			return "Enumeration";
		}else if("'Custom'".equals(tokenName)){
			return "Custom";
		}else if("'representation'".equals(tokenName)){
			return "representation";
		}else if("'elements'".equals(tokenName)){
			return "elements";
		}else if("'Combobox'".equals(tokenName)){
			return "Combobox";
		}else if("'Radio'".equals(tokenName)){
			return "Radio";
		}else if("'as'".equals(tokenName)){
			return "as";
		}else if("'editable'".equals(tokenName)){
			return "editable";
		}else if("'zero'".equals(tokenName)){
			return "zero";
		}else if("'one'".equals(tokenName)){
			return "one";
		}else if("'many'".equals(tokenName)){
			return "many";
		}
		return super.calculateId(tokenName, tokenType);
	}

}
