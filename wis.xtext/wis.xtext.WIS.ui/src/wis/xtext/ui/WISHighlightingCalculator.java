package wis.xtext.ui;

import java.util.Iterator;

import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightedPositionAcceptor;
import org.eclipse.xtext.ui.editor.syntaxcoloring.ISemanticHighlightingCalculator;

public class WISHighlightingCalculator implements
		ISemanticHighlightingCalculator {

	protected static String context="packages";
	protected static String[] activeContext=null;
	
	protected static final String[] lModel={
		"model",
		"title",
		"version"
	};
	
	protected static final String[] lPackages={
		"packages",
		"entities",
		"label",
		"links",
		"generate",
		"subpackages",
		"attributes",
		"version",
		
		"from",
		"to",
		"max",
		"min",
		"editable",
		"zero",
		"one",
		"many",
		"as",
		
		"isPublic",
		"placeholder",
		"important",
		"validationRegex",
		"validationMessage",
		"allowNull",
		"disabled",
		"message",
		"messageDev",
		"type",
		"length",
		"enumeration",
		
		"StringSingleline",
		"StringMultiline",
		"Integer",
		"Double",
		"Date",
		"Boolean",
		"Enumeration",
		"Custom",
		
		"true",
		"false"
	};
	
	protected static final String[] lEnumerations={
		"enumerations",
		
		"representation",
		"elements",
		"Combobox",
		"Radio"
	};
	
	protected static final String[] lReports={
		"reports",
		"label",
		"generate",
		"attributes",
		"version",
		
		"isPublic",
		"placeholder",
		"important",
		"validationRegex",
		"validationMessage",
		"allowNull",
		"disabled",
		"message",
		"messageDev",
		"type",
		"length",
		"enumeration",
		
		"StringSingleline",
		"StringMultiline",
		"Integer",
		"Double",
		"Date",
		"Boolean",
		"Enumeration",
		"Custom",
		
		"true",
		"false"
	};

	@Override
	public void provideHighlightingFor(XtextResource resource,
			IHighlightedPositionAcceptor acceptor) {
		if (resource == null || resource.getParseResult() == null)
			return;
		Iterator<INode> allNodes = resource.getParseResult().getRootNode()
				.getAsTreeIterable().iterator();
		printLeafs(allNodes, acceptor);
	}

	public void printLeafs(Iterator<INode> it,
			IHighlightedPositionAcceptor acceptor) {
		INode node;
		if (it != null) {
			while (it.hasNext()) {
				node = it.next();
				String name = node.getText().trim();
				if (name.equals("model")) {
					context = "model";
					activeContext = lModel;
				}
				if (name.equals("packages")) {
					context = "packages";
					activeContext = lPackages;
				} else if (name.equals("reports")) {
					context = "reports";
					activeContext = lReports;
				} else if (name.equals("enumerations")) {
					context = "enumerations";
					activeContext = lEnumerations;
				}

				printLeaf(node, acceptor);
			}
		}
	}

	protected void printLeaf(INode node, IHighlightedPositionAcceptor acceptor) {
		if (activeContext != null
				&& contains(activeContext, node.getText().trim())) {
			acceptor.addPosition(node.getOffset(), node.getLength(), context);
		}
	}

	protected static boolean contains(String[] collection, String value) {
		for (String v : collection) {
			if (v.equals(value))
				return true;
		}
		return false;
	}
}
