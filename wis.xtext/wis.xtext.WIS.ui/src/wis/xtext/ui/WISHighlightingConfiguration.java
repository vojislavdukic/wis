package wis.xtext.ui;

import org.eclipse.swt.SWT;

import org.eclipse.swt.graphics.RGB;
import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultHighlightingConfiguration;
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightingConfigurationAcceptor;
import org.eclipse.xtext.ui.editor.utils.TextStyle;

public class WISHighlightingConfiguration extends
		DefaultHighlightingConfiguration {
	
	@Override
	public void configure(IHighlightingConfigurationAcceptor acceptor) {
		super.configure(acceptor);
		
		addType(acceptor, "model",170,0,30, SWT.BOLD);
		addType(acceptor, "packages",170,0,30, SWT.BOLD);
		addType(acceptor, "reports",0,102,0, SWT.BOLD);
		addType(acceptor, "enumerations",51,51,204, SWT.BOLD);
		/*
		addType(acceptor, "package",170,0,30, SWT.BOLD);
		addType(acceptor, "entities",170,0,30, SWT.BOLD);
		addType(acceptor, "label",170,0,30, SWT.BOLD);
		addType(acceptor, "links",170,0,30, SWT.BOLD);
		addType(acceptor, "generate",170,0,30, SWT.BOLD);
		addType(acceptor, "subpackages",170,0,30, SWT.BOLD);
		
		addType(acceptor, "true",170,0,30, SWT.BOLD);
		addType(acceptor, "false",170,0,30, SWT.BOLD);
		
		addType(acceptor, "attributes",170,0,30, SWT.BOLD);
		
		addType(acceptor, "from",170,0,30, SWT.BOLD);
		addType(acceptor, "to",170,0,30, SWT.BOLD);
		addType(acceptor, "max",170,0,30, SWT.BOLD);
		addType(acceptor, "min",170,0,30, SWT.BOLD);
		
		addType(acceptor, "isPublic",170,0,30, SWT.BOLD);
		addType(acceptor, "placeholder",170,0,30, SWT.BOLD);
		addType(acceptor, "important",170,0,30, SWT.BOLD);
		addType(acceptor, "validationRegex",170,0,30, SWT.BOLD);
		addType(acceptor, "validationMessage",170,0,30, SWT.BOLD);
		addType(acceptor, "allowNull",170,0,30, SWT.BOLD);
		addType(acceptor, "type",170,0,30, SWT.BOLD);
		addType(acceptor, "length",170,0,30, SWT.BOLD);
		addType(acceptor, "enumeration",170,0,30, SWT.BOLD);
		
		addType(acceptor, "StringSingleline",170,0,30, SWT.BOLD);
		addType(acceptor, "StringMultiline",170,0,30, SWT.BOLD);
		addType(acceptor, "Integer",170,0,30, SWT.BOLD);
		addType(acceptor, "Double",170,0,30, SWT.BOLD);
		addType(acceptor, "Date",170,0,30, SWT.BOLD);
		addType(acceptor, "Boolean",170,0,30, SWT.BOLD);
		addType(acceptor, "Enumeration",170,0,30, SWT.BOLD);
		addType(acceptor, "Custom",170,0,30, SWT.BOLD);
		
		addType(acceptor, "representation",170,0,30, SWT.BOLD);
		addType(acceptor, "elements",170,0,30, SWT.BOLD);
		
		addType(acceptor, "Combobox",170,0,30, SWT.BOLD);
		addType(acceptor, "Radio",170,0,30, SWT.BOLD);
		
		addType(acceptor, "as",170,0,30, SWT.BOLD);
		addType(acceptor, "editable",170,0,30, SWT.BOLD);
		
		addType(acceptor, "zero",170,0,30, SWT.BOLD);
		addType(acceptor, "one",170,0,30, SWT.BOLD);
		addType(acceptor, "many",170,0,30, SWT.BOLD);
		*/
	}
	
	public void addType( IHighlightingConfigurationAcceptor acceptor, String s, int r, int g, int b, int style )
	 {
		 TextStyle textStyle = new TextStyle();
		 textStyle.setBackgroundColor(new RGB(255, 255, 255));
		 textStyle.setColor(new RGB(r, g, b));
		 textStyle.setStyle(style);
		 acceptor.acceptDefaultHighlighting(s, s, textStyle);
	 }

}
