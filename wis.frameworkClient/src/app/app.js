'use strict';

angular.module( 'WIS', [
	'templates-app',
	'templates-common',
	'WIS.common',
	'WIS.projectManagement',
	'WIS.reports',
	'ui.router',
	'ui.bootstrap'
])

.config( function myAppConfig ( $stateProvider, $urlRouterProvider, $httpProvider ) {
	//$urlRouterProvider.otherwise( '/home' );
	$httpProvider.interceptors.push(function ($q) {
		return {
			'request': function (config) {
				//redirect requests for server app
				if(config.url.lastIndexOf('SERVER', 0) === 0){
					//remove SERVER from url
					config.url = 'http://localhost:8080/GenericProject' + config.url.substring(6);
				}
				return config || $q.when(config);
			}
		};
	});
})

.run(function run(){})

.controller( 'AppCtrl', function AppCtrl ( $scope, $location ) {
	$scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
		if ( angular.isDefined( toState.data.pageTitle ) ) {
			$scope.pageTitle = toState.data.pageTitle + ' | WIS' ;
		}
	});

})
;

