'use strict';

angular.module('WIS.projectManagement').factory('ServiceEmployee', ['restResource', function ($restResource) {
  return $restResource('projectManagement/employee');
}]);
