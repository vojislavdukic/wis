'use strict';

angular.module('WIS.projectManagement').factory('ServiceProject', ['restResource', function ($restResource) {
  return $restResource('projectManagement/project');
}]);
