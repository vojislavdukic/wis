'use strict';

angular.module('WIS.projectManagement')
.controller('ctrlEmployee',['$scope',
function($scope){
	$scope.model={
		label:'Employee',
		serviceName:'ServiceEmployee',
		fields:[
			{
				label:'First name',
				name:'firstName',
				type:'text',
				important: true,
				validation:{
					required: false,
					vType: 'stringsingleline',
					customValidation:[
					]
				}
			},
			{
				label:'Last name',
				name:'lastName',
				type:'text',
				important: true,
				validation:{
					required: false,
					vType: 'stringsingleline',
					customValidation:[
					]
				}
			},
			{
				label:'Wage',
				name:'wage',
				type:'text',
				classes: 'border-green',
				important: true,
				message: '2. Wage added',
				validation:{
					required: false,
					vType: 'double',
					customValidation:[
					]
				}
			},
			{
				label:'Additional income',
				name:'additionalIncome',
				type:'text',
				important: true,
				validation:{
					required: false,
					vType: 'double',
					customValidation:[
					]
				}
			},
			{
				label:'Date of birth',
				name:'dateOfBirth',
				type:'text',
				dateFormat: 'dd/MM/yyyy',
				classes: 'border-green',
				important: true,
				message: '3. Date of birth added',
				validation:{
					required: false,
					vType: 'date',
					customValidation:[
					]
				}
			},
			{
				label:'Gender',
				name:'gender',
				type:'combobox',
				enumValues: ['Male','Female'],
				important: true,
				validation:{
					required: false,
					vType: 'enumeration',
					customValidation:[
					]
				}
			},
			{
				label:'Address',
				name:'address',
				type:'text',
				disabled: true,
				classes: 'border-red',
				important: false,
				message: '5. Address delted',
				validation:{
					required: false,
					vType: 'stringsingleline',
					customValidation:[
					]
				}
			}
		],
		relationships:[
			{
				entity: {
					label:'Project',
					serviceName:'ServiceProject',
					fields:[
						{
							label:'Name',
							name:'name',
							type:'text',
							important: true
						},
						{
							label:'Purchaser',
							name:'purchaser',
							type:'text',
							multiline: true,
							important: true
						}
					]
				},
				label: 'Managed projects',
				name: 'managedProjects',
				editable: true,
				type: 'many'
			},
			{
				entity: {
					label:'Project',
					serviceName:'ServiceProject',
					fields:[
						{
							label:'Name',
							name:'name',
							type:'text',
							important: true
						},
						{
							label:'Purchaser',
							name:'purchaser',
							type:'text',
							multiline: true,
							important: true
						}
					]
				},
				label: 'Projects',
				name: 'projects',
				editable: true,
				type: 'many'
			},
			{
				entity: {
					label:'Employee',
					serviceName:'ServiceEmployee',
					fields:[
						{
							label:'First name',
							name:'firstName',
							type:'text',
							important: true
						},
						{
							label:'Last name',
							name:'lastName',
							type:'text',
							important: true
						},
						{
							label:'Wage',
							name:'wage',
							type:'text',
							important: true
						},
						{
							label:'Additional income',
							name:'additionalIncome',
							type:'text',
							important: true
						},
						{
							label:'Date of birth',
							name:'dateOfBirth',
							type:'text',
							dateFormat: 'dd/MM/yyyy',
							important: true
						},
						{
							label:'Gender',
							name:'gender',
							type:'combobox',
							enumValues: ['Male','Female'],
							important: true
						}
					]
				},
				label: 'Boss',
				name: 'boss',
				editable: true,
				type: 'one'
			},
			{
				entity: {
					label:'Employee',
					serviceName:'ServiceEmployee',
					fields:[
						{
							label:'First name',
							name:'firstName',
							type:'text',
							important: true
						},
						{
							label:'Last name',
							name:'lastName',
							type:'text',
							important: true
						},
						{
							label:'Wage',
							name:'wage',
							type:'text',
							important: true
						},
						{
							label:'Additional income',
							name:'additionalIncome',
							type:'text',
							important: true
						},
						{
							label:'Date of birth',
							name:'dateOfBirth',
							type:'text',
							dateFormat: 'dd/MM/yyyy',
							important: true
						},
						{
							label:'Gender',
							name:'gender',
							type:'combobox',
							enumValues: ['Male','Female'],
							important: true
						}
					]
				},
				label: 'Subworker',
				name: 'subworker',
				editable: true,
				type: 'many'
			}
		]
	};
}]);
