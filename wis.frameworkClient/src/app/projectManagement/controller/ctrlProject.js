'use strict';

angular.module('WIS.projectManagement')
.controller('ctrlProject',['$scope',
function($scope){
	$scope.model={
		label:'Project',
		serviceName:'ServiceProject',
		fields:[
			{
				label:'Name',
				name:'name',
				type:'text',
				important: true,
				validation:{
					required: false,
					vType: 'stringsingleline',
					customValidation:[
					]
				}
			},
			{
				label:'Purchaser',
				name:'purchaser',
				type:'text',
				multiline: true,
				important: true,
				validation:{
					required: false,
					vType: 'stringmultiline',
					customValidation:[
					]
				}
			}
		],
		relationships:[
			{
				entity: {
					label:'Employee',
					serviceName:'ServiceEmployee',
					fields:[
						{
							label:'First name',
							name:'firstName',
							type:'text',
							important: true
						},
						{
							label:'Last name',
							name:'lastName',
							type:'text',
							important: true
						},
						{
							label:'Wage',
							name:'wage',
							type:'text',
							important: true
						},
						{
							label:'Additional income',
							name:'additionalIncome',
							type:'text',
							important: true
						},
						{
							label:'Date of birth',
							name:'dateOfBirth',
							type:'text',
							dateFormat: 'dd/MM/yyyy',
							important: true
						},
						{
							label:'Gender',
							name:'gender',
							type:'combobox',
							enumValues: ['Male','Female'],
							important: true
						}
					]
				},
				label: 'Project manager',
				name: 'projectManager',
				editable: true,
				type: 'one'
			},
			{
				entity: {
					label:'Employee',
					serviceName:'ServiceEmployee',
					fields:[
						{
							label:'First name',
							name:'firstName',
							type:'text',
							important: true
						},
						{
							label:'Last name',
							name:'lastName',
							type:'text',
							important: true
						},
						{
							label:'Wage',
							name:'wage',
							type:'text',
							important: true
						},
						{
							label:'Additional income',
							name:'additionalIncome',
							type:'text',
							important: true
						},
						{
							label:'Date of birth',
							name:'dateOfBirth',
							type:'text',
							dateFormat: 'dd/MM/yyyy',
							important: true
						},
						{
							label:'Gender',
							name:'gender',
							type:'combobox',
							enumValues: ['Male','Female'],
							important: true
						}
					]
				},
				label: 'Employees',
				name: 'employees',
				editable: true,
				type: 'many'
			}
		]
	};
}]);
