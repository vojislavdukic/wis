'use strict';

angular.module('WIS.projectManagement', [
    'ngRoute',
    'ngResource',
	'smart-table',
	'dialogs.main',
    'checklist-model'
]);
