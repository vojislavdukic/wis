'use strict';

angular.module('WIS.projectManagement').config(['$routeProvider', function ($routeProvider) {
  $routeProvider
  	.when('/projectManagement/employee/', {
  		templateUrl:'common/directive/GenericView/tmplGenericView.tpl.html',
  		controller:'ctrlEmployee'
  	})
  	.when('/projectManagement/project/', {
  		templateUrl:'common/directive/GenericView/tmplGenericView.tpl.html',
  		controller:'ctrlProject'
  	})
;}]);
