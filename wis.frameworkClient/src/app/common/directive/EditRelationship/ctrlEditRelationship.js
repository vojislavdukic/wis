'use strict';

angular.module('WIS.common')
.controller('ctrlEditRelationship',['$scope','$injector','ServiceUtil',
function($scope,$injector,ServiceUtil){
	/*$scope.numberToEnum=function(item,relationshipModel){
		for(var ind=0;ind<model.fields.length;ind=ind+1){
			if(model.fields[ind].enumValues){
				//if field is empty
				if(!item[model.fields[ind].name]){
					item[model.fields[ind].name]=model.fields[ind].enumValues[0];
				}else{
					item[model.fields[ind].name]=
						model.fields[ind].enumValues[item[model.fields[ind].name]];
				}
			}			
		}
	};
	$scope.numberToEnumList=function(items,model){
		for(var ind=0;ind<items.length;ind=ind+1){
			$scope.numberToEnum(items[ind],model);
		}
	};*/

	$scope.relationshipModel=$scope.$parent.$parent.$parent.relationshipModel;
	$scope.globalModel=$scope.$parent.$parent.$parent.globalModel;
	$scope.item=$scope.$parent.$parent.$parent.relationshipItem;
	$scope.globalItem=$scope.$parent.$parent.$parent.globalItem;
	$scope.modalInstance=$scope.$parent.$parent.modalInstance;

	$scope.service=$injector.get($scope.relationshipModel.entity.serviceName);

	$scope.choosen={};
	$scope.choosens=[];

	$scope.flagSingle=($scope.relationshipModel.type==='one');
	$scope.flagMultiple=($scope.relationshipModel.type==='many');

	$scope.spanSize=1;
	for(var ind=0;ind<$scope.relationshipModel.entity.fields.length;ind=ind+1){
		if($scope.relationshipModel.entity.fields[ind].important){
			$scope.spanSize=$scope.spanSize+1;
		}
	}

	$scope.service.query(function(items) {
		/*new ServiceUtil().fromNumberToEnumList(items,$scope.relationshipModel.entity,false);*/
    	$scope.items=items;
		if($scope.item){
			if($scope.flagMultiple){
				for(var ind=0;ind<$scope.item.length;ind=ind+1){
	    			var addIndex = $scope.items.map(function(item) { return item.id; }).indexOf($scope.item[ind].id);
					if(addIndex!==-1){
						$scope.choosens.push($scope.items[addIndex]);
					}
	    		}
			}else{
				var currentIndex = $scope.items.map(function(item) { return item.id; }).indexOf($scope.item.id);
				if(currentIndex!==-1){
					$scope.choosen=$scope.items[currentIndex];
					$scope.lastId=$scope.choosen.id;
				}
			}

			//remove myself
			if($scope.globalModel.serviceName===$scope.relationshipModel.entity.serviceName){
				var removeIndex = $scope.items.map(function(item) { return item.id; }).indexOf($scope.globalItem.id);
				if(removeIndex!==-1){
					$scope.items.splice(removeIndex, 1);
				}
			}
		}
    });

	$scope.choose=function(){
		if($scope.flagSingle){
			angular.copy($scope.choosen,$scope.item);
		}else if($scope.flagMultiple){
			angular.copy($scope.choosens,$scope.item);
		}
		$scope.modalInstance.close();
	};

	$scope.cancel=function(){
		$scope.modalInstance.close();
	};

	$scope.uncheck = function (item) {
	    if ($scope.lastId === item.id){
	        $scope.choosen = {};
	        $scope.lastId = -1;
	    }else{
	    	$scope.choosen = item;
	    	$scope.lastId = item.id;
	    }
	};

}]);
