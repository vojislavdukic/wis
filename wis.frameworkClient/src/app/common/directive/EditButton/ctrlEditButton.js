'use strict';

angular.module('WIS.common')
.controller('ctrlEditButton',['$scope','$modal',
function($scope,$modal){
	$scope.openEditDialog=function(){
		$scope.modalInstance={};
		$scope.modalInstance=$modal.open({
			templateUrl: 'common/directive/GeneralEdit/tmplGeneralEdit.tpl.html', 
            size: 'sm',
            windowClass: 'wide-modal-dialog',
            scope: $scope
		});
		/*$scope.modalInstance.result.then(function(newPost) {
			if(newPost) $scope.posts.push(newPost);
		});*/
	};
}]);
