'use strict';

angular.module('WIS.common')
.controller('ctrlEditRelationshipButton',['$scope','$modal',
function($scope,$modal){
	$scope.openChooseDialog=function(){
		$scope.modalInstance={};
		$scope.modalInstance=$modal.open({
			templateUrl: 'common/directive/EditRelationship/tmplEditRelationship.tpl.html', 
            size: 'sm',
            windowClass: 'wide-modal-dialog',
            scope: $scope
		});
	};
}]);
