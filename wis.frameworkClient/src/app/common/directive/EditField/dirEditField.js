'use strict';

angular.module('WIS.common')
.directive('ngEditField',[ function() {
	var directive={};
	directive.restrict='A';
	directive.scope={
		fieldModel:'=',
		item:'=',
		error:'='
	};
	directive.templateUrl='common/directive/EditField/tmplEditField.tpl.html';
	directive.controller='ctrlEditField';
	directive.link= function (scope, element, attributes) {};
    return directive;
}]);