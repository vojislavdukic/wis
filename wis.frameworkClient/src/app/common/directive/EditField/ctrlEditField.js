'use strict';

angular.module('WIS.common')
.controller('ctrlEditField',['$scope','$filter',
function($scope,$filter){
	/*$scope.fieldModel=$scope.$parent.fieldModel;*/

	if($scope.fieldModel.type==='combobox' && !$scope.item[$scope.fieldModel.name]){
		$scope.item[$scope.fieldModel.name]=$scope.fieldModel.enumValues[0];
	}

	//date
	$scope.dateOptions = {
		formatYear: 'yy',
		startingDay: 1
	};
	$scope.opened=false;
	$scope.open = function($event) {
		$event.preventDefault();
		$event.stopPropagation();
		$scope.opened = true;
	};

	/*$scope.item=$scope.$parent.item;*/
}]);
