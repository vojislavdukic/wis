'use strict';

angular.module('WIS.common')
.controller('ctrlShowReport',['$scope','$sce','$http',
function($scope,$sce,$http){
	$scope.report={};

	$scope.showReport=function(){
		$http.post('SERVER/report/'+$scope.model.name,$scope.report, {responseType:'arraybuffer'})
		  .success(function (response) {
		       var file = new Blob([response], {type: 'application/pdf'});
		       var fileURL = URL.createObjectURL(file);
		       $scope.content = $sce.trustAsResourceUrl(fileURL);
		       window.open($scope.content);
		});
	};
}]);
