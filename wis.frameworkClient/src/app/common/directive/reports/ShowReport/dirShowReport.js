'use strict';

angular.module('WIS.common')
.directive('ngShowReport',function() {
	var directive={};
	directive.restrict='A';
	directive.scope={
		model:'='
	};
	directive.templateUrl='common/directive/reports/ShowReport/tmplShowReport.tpl.html';
	directive.controller='ctrlShowReport';
	directive.link= function (scope, element, attributes) {};
    return directive;
});