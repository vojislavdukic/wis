'use strict';

angular.module('WIS.common')
.controller('ctrlShowEntity',['$scope', '$injector', 'dialogs','ServiceUtil',
function($scope,$injector,dialogs,ServiceUtil){

	$scope.service=$injector.get($scope.model.serviceName);

	$scope.items={};
	//get all
	$scope.service.query(function(items) {
    	$scope.items=items;
    	/*new ServiceUtil().fromNumberToEnumList($scope.items,$scope.model,false);*/
    });

	$scope.spanSize=1;
	for(var ind=0;ind<$scope.model.fields.length;ind=ind+1){
		if($scope.model.fields[ind].important){
			$scope.spanSize=$scope.spanSize+1;
		}
	}

	$scope.remove=function(item){
		var dlg = dialogs.confirm('Please confirm', 'Are you sure you want to delete item ' + item.id + ' ?');
		dlg.result.then(function(btn) {
			var removeIndex = $scope.items.map(function(it) { return it.id; }).indexOf(item.id);
			$scope.items.splice(removeIndex, 1);
			item.$remove();
		});   
	};
    
    $scope.selectedItem = null;
    $scope.selection = function(item) {
        if(item.isSelected) {
            $scope.selectedItem  = item;
        } else {
            $scope.selectedItem  = null;
            item.isSelected = false; 
        }
    };
}]);
