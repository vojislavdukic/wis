'use strict';

angular.module('WIS.common').config(['$routeProvider', function ($routeProvider) {
	$routeProvider.when('/', {
	  	templateUrl:'common/util/mainPage.tpl.html'
  	});
}]);
