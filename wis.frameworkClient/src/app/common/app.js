'use strict';

angular.module('WIS.common', [
    'ngRoute',
    'ngResource',
	'smart-table',
	'dialogs.main',
    'checklist-model'
]);