'use strict';

angular.module('WIS.reports')
.controller('ctrlReportPrviIzvestaj',['$scope',
function($scope){
	$scope.model={
		label:'Prvi izvestaj',
		name:'prviIzvestaj',
		fields:[
			{
				label:'Prvi parametar',
				name:'p1',
				type:'text',
			},
			{
				label:'Drugi parametar',
				name:'p2',
				type:'checkbox',
			}
		]
	};
}]);
