'use strict';

angular.module('WIS.reports')
.controller('ctrlReportEmployeesGeneralReport',['$scope',
function($scope){
	$scope.model={
		label:'Employees general report',
		name:'employeesGeneralReport',
		fields:[
			{
				label:'BShow projects',
				name:'bShowProjects',
				type:'checkbox',
			}
		]
	};
}]);
