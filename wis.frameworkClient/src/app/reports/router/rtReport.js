'use strict';

angular.module('WIS.reports').config(['$routeProvider', function ($routeProvider) {
$routeProvider
  	.when('/reports/employeesgeneralreport', {
			  	templateUrl:'common/directive/reports/view/tmplGenericViewReport.tpl.html',
			  	controller:'ctrlReportEmployeesGeneralReport'
  	})
		    ;
}]);
