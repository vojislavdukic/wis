package com.genericproject.server.util;

public class ColorProp {
	public static final String idividualni="#B80000";
	public static final String idividualniZavrsen="#C63333";
	
	public static final String serijski="#006E25";
	public static final String serijskiZavrsen="#4D9A66";
	
	public static final String kurs="#0029A3";
	public static final String kursZavrsen="#4765BC";
}
