package com.genericproject.server.util;

public class RegExPatterns {
	public static final String dateShort="\\d{2}/\\d{2}/\\d{4}";
	public static final String dateShortNull="^$|\\d{2}/\\d{2}/\\d{4}";
	
	public static final String jmbgNull="^$|\\d{13}";
	public static final String jmbg="\\d{13}";
	
	public static final String integer="(^0$)|([1-9]\\d*)";
	public static final String integerNull="^$|[1-9]\\d*";
	
	public static final String time="\\d\\d:\\d\\d";
	public static final String timeNull="^$|\\d\\d:\\d\\d";
	
	public static final String decim="(0\\.\\d\\d?)|(1)|(1\\.0)|(1\\.00)";
	public static final String decimNull="^$|(0\\.\\d\\d?)|(1)|(1\\.0)|(1\\.00)";
}
