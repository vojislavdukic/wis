package com.genericproject.server.util.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;

public class HibernateAwareObjectMapper extends ObjectMapper {
	
    public HibernateAwareObjectMapper() {
    	Hibernate4Module module=new Hibernate4Module();
        registerModule(module);
    }
}
