package com.genericproject.server.util;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageUtil {

	public static BufferedImage scaleImage(BufferedImage image, int max){
		BufferedImage scaled;
	    int width = image.getWidth(null);
	    int height = image.getHeight(null);
	    double dWidth = 0;
	    double dHeight = 0;
	    if (width == height) {
	        dWidth = max;
	        dHeight = max;
	    } 
	    else if (width > height) {
	        dWidth = max;
	        dHeight = ((double) height / (double) width) * max;
	    }
	    else {
	        dHeight = max;
	        dWidth = ((double) width / (double) height) * max;
	    }
	    int fwidth = (int)dWidth;
	    int fheight = (int)dHeight;
	    
	    BufferedImage img = new BufferedImage(fwidth, fheight, BufferedImage.TYPE_INT_RGB);
	    Image scaledImage = image.getScaledInstance(fwidth, fheight, Image.SCALE_SMOOTH);
        img.createGraphics().drawImage(scaledImage, 0, 0, null);
        scaled = new BufferedImage(fwidth, fheight,BufferedImage.TYPE_INT_RGB);
        scaled = img.getSubimage(0,0, fwidth, fheight);
	    return scaled;
	}

}
