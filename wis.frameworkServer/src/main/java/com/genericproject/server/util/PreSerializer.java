package com.genericproject.server.util;


import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PreSerializer {

	private static final String PACKAGE_PREFIX = "com.genericproject";

	private List<Object> uniqueObjects= new ArrayList<Object>(); 
	
	public void preventRecursion(Object obj,int maxLevel){
		uniqueObjects.add(obj);
		for(Field f:obj.getClass().getDeclaredFields()){
			try {
				if(f.getType().getCanonicalName().startsWith(PACKAGE_PREFIX)){
					Object result=obj.getClass().getMethod("get"+capLetter(f.getName())).invoke(obj);
					if(result!=null){
						if(maxLevel==1){
							obj.getClass().getMethod("set"+capLetter(f.getName()),f.getType()).invoke(obj,new Object[]{null});
						}else{
							if(checkExistence(result)){
								obj.getClass().getMethod("set"+capLetter(f.getName()),f.getType()).invoke(obj,new Object[]{null});
							}else{
								preventRecursion(result, maxLevel-1);
							}
						}
					}
				}
				if(f.getType().isAssignableFrom(java.util.List.class)){
					List result=(List)obj.getClass().getMethod("get"+capLetter(f.getName())).invoke(obj);
					if(result!=null){
						if(maxLevel==1){
							obj.getClass().getMethod("set"+capLetter(f.getName()),f.getType()).invoke(obj,new Object[]{null});
						}else{
							for(Iterator<String> it = result.iterator(); it.hasNext();){
								Object current=it.next();
								if(checkExistence(current)){
									it.remove();
								}else{
									preventRecursion(current, maxLevel-1);
								}
							}
						}
					}
					
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	protected boolean checkExistence(Object obj){
		try {
			Object objId=obj.getClass().getMethod("getId").invoke(obj);
			for(Object o:uniqueObjects){
				Object oId=o.getClass().getMethod("getId").invoke(o);
				if(obj.getClass().equals(o.getClass())
						&&((objId==oId)||oId.equals(objId))){
					return true;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return false;
	}
	
	protected static String capLetter(String word){
		return word.substring(0, 1).toUpperCase()+ word.substring(1);
	}
}
