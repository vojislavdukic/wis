package com.genericproject.server.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;

public class HibernateUtil {

	public static void getEager(Object entity){
		Class entityType=entity.getClass();
		for (Field f : entityType.getDeclaredFields()) {
			if(f.getType().getCanonicalName().startsWith("com.doob")||
					f.getType().getCanonicalName().startsWith("java.util")){
				Method getter=null;
				try {
					String capLetter=f.getName().substring(0, 1).toUpperCase() + f.getName().substring(1);
					getter = entityType.getMethod("get"+ capLetter);
					Object fieldInstance = getter.invoke(entity);
					if (!Hibernate.isInitialized(fieldInstance)) {
						Hibernate.initialize(fieldInstance);
					}
				} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException | HibernateException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static String capitalizeFirstLetter(String value) {
		if (value != null && value.length() > 0)
			return value.substring(0, 1).toUpperCase() + value.substring(1);
		else
			return value;
	}
}
