package com.genericproject.server.util;

import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	public static Date lastFirstSeptember(){
		Calendar calendar=Calendar.getInstance();
		calendar.setTime(new Date());
		
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		//if it is not sep, oct, nov or dec
		if(calendar.get(Calendar.MONTH)<8){
			calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR)-1);
		}
		//set month to sept
		calendar.set(Calendar.MONTH, 8);
		//set day to first
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		
		return calendar.getTime();
	}
}
