package com.genericproject.server.aspect.logging.exception;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.genericproject.server.aspect.logging.util.Loggable;

@Component
@Aspect
public class ExceptionAspect {

	@Loggable
	private Logger logger;

	//hvata sve izuzetke iz kontrolera
	@AfterThrowing(pointcut = "within(rajakis.web.controller.*)",throwing = "exception")
	public void log(JoinPoint joinPoint, Throwable exception) {
		StringBuilder stringBuilder=new StringBuilder("");
		RequestAttributes requestAttributes=RequestContextHolder.currentRequestAttributes();
		if(requestAttributes!=null && requestAttributes instanceof ServletRequestAttributes){
			HttpServletRequest request=((ServletRequestAttributes)requestAttributes).getRequest();
			if(request!=null){
				stringBuilder.append("URL: ");
				stringBuilder.append(request.getRequestURL().toString());
				stringBuilder.append(System.getProperty("line.separator"));
				Enumeration<String> keys=request.getParameterNames();
				while(keys.hasMoreElements()){
					String key=keys.nextElement();
					stringBuilder.append(key+": "+request.getParameter(key));
					stringBuilder.append(System.getProperty("line.separator"));
				}
			}
		}
		stringBuilder.append("Method: ");
		stringBuilder.append(joinPoint.getSignature().getDeclaringTypeName());
		stringBuilder.append(System.getProperty("line.separator"));
		stringBuilder.append("Error: ");
		stringBuilder.append(exception.getClass().toString());
		stringBuilder.append(System.getProperty("line.separator"));
		stringBuilder.append("Stack trace: ");
		
		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		stringBuilder.append(sw.toString());
		
		stringBuilder.append(System.getProperty("line.separator"));
		logger.error(stringBuilder.toString());
	}
}
