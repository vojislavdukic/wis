package com.genericproject.server.aspect.logging.info;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.genericproject.server.aspect.logging.util.Loggable;

@Component
@Aspect
public class InfoAspect {

	@Loggable
	private Logger logger;

	//hvata sve zahteve loguje
	public void log(String message, JoinPoint joinPoint) {
		StringBuilder stringBuilder=new StringBuilder(message);
		stringBuilder.append(System.getProperty("line.separator"));
		RequestAttributes requestAttributes=RequestContextHolder.currentRequestAttributes();
		if(requestAttributes!=null && requestAttributes instanceof ServletRequestAttributes){
			HttpServletRequest request=((ServletRequestAttributes)requestAttributes).getRequest();
			if(request!=null){
				stringBuilder.append("URL: ");
				stringBuilder.append(request.getRequestURL().toString());
				stringBuilder.append(System.getProperty("line.separator"));
			}
		}
		stringBuilder.append("Method: ");
		stringBuilder.append(joinPoint.getSignature().getDeclaringTypeName()+"."+joinPoint.getSignature().getName());
		stringBuilder.append(System.getProperty("line.separator"));
		stringBuilder.append("Argumenst: ");
		for(Object o:joinPoint.getArgs()){
			if(o!=null && !(o instanceof BindingResult) && !(o instanceof Model)){
				stringBuilder.append(o.toString());
				stringBuilder.append(System.getProperty("line.separator"));
			}
		}
		stringBuilder.append(System.getProperty("line.separator"));
		logger.info(stringBuilder.toString());
	}
	
	// /////////////////////////////////
	// AJAX //
	// /////////////////////////////////
	/*
	@After("execution(* rajakis.web.controller.AjaxController.removeIndividualniCas(..))")
	public void ajaxObrisiIndividualniCas(JoinPoint joinPoint) {
		log("Обрисан индивидуални час",joinPoint);
	}
	*/
}
