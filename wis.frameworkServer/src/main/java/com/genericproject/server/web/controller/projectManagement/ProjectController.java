package com.genericproject.server.web.controller.projectManagement;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.genericproject.server.aspect.logging.util.Loggable;

import com.genericproject.server.model.repository.projectManagement.ProjectRepository;
import com.genericproject.server.model.jpa.projectManagement.ProjectBean;

@RestController
@RequestMapping("/projectManagement/project")
public class ProjectController {
	
	@Loggable
	Logger logger;

	@Autowired
	ProjectRepository projectRepository;

	@RequestMapping(method = RequestMethod.GET)
	public List<ProjectBean> getAll() {
		List<ProjectBean> retVal = projectRepository.findAll();
		return  retVal;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ProjectBean get(@PathVariable("id") Long id) {		
		return projectRepository.findOne(id);
	}
	
	@RequestMapping(value = "/{id}/eager", method = RequestMethod.GET)
	public ProjectBean getEager(@PathVariable("id") Long id) {		
		return projectRepository.findOneEager(id);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ProjectBean insert(@RequestBody ProjectBean projectBean) {
		return projectRepository.save(projectBean);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public String delete(@PathVariable("id") Long id) {
		projectRepository.delete(id);
		return "Ok";
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ProjectBean update(@PathVariable("id") Long id, @RequestBody ProjectBean projectBean) {
		projectRepository.save(projectBean);
		return projectBean;
	}
}
