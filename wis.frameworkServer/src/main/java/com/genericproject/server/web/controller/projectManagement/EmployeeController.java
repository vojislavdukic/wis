package com.genericproject.server.web.controller.projectManagement;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.genericproject.server.aspect.logging.util.Loggable;

import com.genericproject.server.model.repository.projectManagement.EmployeeRepository;
import com.genericproject.server.model.jpa.projectManagement.EmployeeBean;

@RestController
@RequestMapping("/projectManagement/employee")
public class EmployeeController {
	
	@Loggable
	Logger logger;

	@Autowired
	EmployeeRepository employeeRepository;

	@RequestMapping(method = RequestMethod.GET)
	public List<EmployeeBean> getAll() {
		List<EmployeeBean> retVal = employeeRepository.findAll();
		return  retVal;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public EmployeeBean get(@PathVariable("id") Long id) {		
		return employeeRepository.findOne(id);
	}
	
	@RequestMapping(value = "/{id}/eager", method = RequestMethod.GET)
	public EmployeeBean getEager(@PathVariable("id") Long id) {		
		return employeeRepository.findOneEager(id);
	}

	@RequestMapping(method = RequestMethod.POST)
	public EmployeeBean insert(@RequestBody EmployeeBean employeeBean) {
		return employeeRepository.save(employeeBean);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public String delete(@PathVariable("id") Long id) {
		employeeRepository.delete(id);
		return "Ok";
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public EmployeeBean update(@PathVariable("id") Long id, @RequestBody EmployeeBean employeeBean) {
		employeeRepository.save(employeeBean);
		return employeeBean;
	}
}
