package com.genericproject.server.model.repository.projectManagement;

import org.springframework.stereotype.Repository;
import com.genericproject.server.model.repository.util.GenericRepository;
import com.genericproject.server.model.jpa.projectManagement.EmployeeBean;

@Repository
public interface EmployeeRepository extends GenericRepository<EmployeeBean, Long>{

}
