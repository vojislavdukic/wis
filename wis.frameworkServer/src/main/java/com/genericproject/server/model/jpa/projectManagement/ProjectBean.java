package com.genericproject.server.model.jpa.projectManagement;

import java.util.List;
import java.util.Date;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.OneToMany;
import javax.persistence.ManyToOne;
import javax.persistence.ManyToMany;
import javax.persistence.Version;
import java.io.Serializable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.FetchType;
import javax.persistence.CascadeType;
import org.hibernate.annotations.Where;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonFormat;


import com.genericproject.server.model.jpa.projectManagement.EmployeeBean;
import com.genericproject.server.model.jpa.projectManagement.EmployeeBean;

@Entity
@Table(name="project")
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties({"$resolved","isSelected","$promise"})
public class ProjectBean implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "project_id")
	protected Long id;
	@Column(nullable = true,name="name")
	private String name ;
	
	@Column(nullable = true,name="purchaser")
	private String purchaser ;

		@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
	@JoinColumn(name = "projectmanager_employee_id")
	private EmployeeBean projectManager;				
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinTable(
		name="employees_projects",
		joinColumns={@JoinColumn(name="projects_project_id", nullable = false, updatable = false)},
		inverseJoinColumns={@JoinColumn(name="employees_employee_id", nullable = false, updatable = false)})
	private List<EmployeeBean> employees;

		public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public void setName(String name){
		this.name=name;
	}
	
	public String getName(){
		return name;
	}
	
	public void setPurchaser(String purchaser){
		this.purchaser=purchaser;
	}
	
	public String getPurchaser(){
		return purchaser;
	}

		public void setProjectManager(EmployeeBean projectManager){
		this.projectManager=projectManager;
	}
	
	public EmployeeBean getProjectManager(){
		return projectManager;
	}
	
	public void setEmployees(List<EmployeeBean> employees){
		this.employees=employees;
	}
	
	public List<EmployeeBean> getEmployees(){
		return employees;
	}

	}
