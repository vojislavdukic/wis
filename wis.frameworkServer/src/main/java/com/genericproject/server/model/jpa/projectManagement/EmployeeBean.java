package com.genericproject.server.model.jpa.projectManagement;

import java.util.List;
import java.util.Date;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.OneToMany;
import javax.persistence.ManyToOne;
import javax.persistence.ManyToMany;
import javax.persistence.Version;
import java.io.Serializable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.FetchType;
import javax.persistence.CascadeType;
import org.hibernate.annotations.Where;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonFormat;


import com.genericproject.server.model.jpa.projectManagement.ProjectBean;
import com.genericproject.server.model.jpa.projectManagement.ProjectBean;
import com.genericproject.server.model.jpa.projectManagement.EmployeeBean;
import com.genericproject.server.model.jpa.projectManagement.EmployeeBean;

@Entity
@Table(name="employee")
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties({"$resolved","isSelected","$promise"})
public class EmployeeBean implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "employee_id")
	protected Long id;
	@Column(nullable = true,name="firstname")
	private String firstName ;
	
	@Column(nullable = true,name="lastname")
	private String lastName ;
	
	@Column(nullable = true,name="wage")
	private Double wage ;
	
	@Column(nullable = true,name="additionalincome")
	private Double additionalIncome ;
	
	@Column(nullable = true,name="dateofbirth")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd/MM/yyyy")  
	private Date dateOfBirth = new Date();
	
	@Column(nullable = true,name="gender")
	private String gender ;
	
	@Column(nullable = true,name="address")
	private String address ;

		    		@OneToMany(mappedBy = "projectManager", fetch = FetchType.LAZY, cascade = { CascadeType.REFRESH }) 
	private List<ProjectBean> managedProjects;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinTable(
		name="employees_projects",
		joinColumns={@JoinColumn(name="employees_employee_id", nullable = false, updatable = false)},
		inverseJoinColumns={@JoinColumn(name="projects_project_id", nullable = false, updatable = false)})
	private List<ProjectBean> projects;
	
	    		@OneToMany(mappedBy = "boss", fetch = FetchType.LAZY, cascade = { CascadeType.REFRESH }) 
	private List<EmployeeBean> subworker;

		@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
	@JoinColumn(name = "boss_employee_id")
	private EmployeeBean boss;				

		public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public void setFirstName(String firstName){
		this.firstName=firstName;
	}
	
	public String getFirstName(){
		return firstName;
	}
	
	public void setLastName(String lastName){
		this.lastName=lastName;
	}
	
	public String getLastName(){
		return lastName;
	}
	
	public void setWage(Double wage){
		this.wage=wage;
	}
	
	public Double getWage(){
		return wage;
	}
	
	public void setAdditionalIncome(Double additionalIncome){
		this.additionalIncome=additionalIncome;
	}
	
	public Double getAdditionalIncome(){
		return additionalIncome;
	}
	
	public void setDateOfBirth(Date dateOfBirth){
		this.dateOfBirth=dateOfBirth;
	}
	
	public Date getDateOfBirth(){
		return dateOfBirth;
	}
	
	public void setGender(String gender){
		this.gender=gender;
	}
	
	public String getGender(){
		return gender;
	}
	
	public void setAddress(String address){
		this.address=address;
	}
	
	public String getAddress(){
		return address;
	}

		public void setManagedProjects(List<ProjectBean> managedProjects){
		this.managedProjects=managedProjects;
	}
	
	public List<ProjectBean> getManagedProjects(){
		return managedProjects;
	}
	
	public void setProjects(List<ProjectBean> projects){
		this.projects=projects;
	}
	
	public List<ProjectBean> getProjects(){
		return projects;
	}
	
	public void setSubworker(List<EmployeeBean> subworker){
		this.subworker=subworker;
	}
	
	public List<EmployeeBean> getSubworker(){
		return subworker;
	}

		public void setBoss(EmployeeBean boss){
		this.boss=boss;
	}
	
	public EmployeeBean getBoss(){
		return boss;
	}

	}
