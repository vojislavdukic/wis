package com.genericproject.server.model.repository.util;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import com.genericproject.server.util.PreSerializer;

public class GenericRepositoryImpl<T, ID extends Serializable> extends
		SimpleJpaRepository<T, ID> implements GenericRepository<T, ID> {

	//@NoRepositoryBean
	private EntityManager entityManager;
	private Class<T> domainClass;

	// There are two constructors to choose from, either can be used.
	public GenericRepositoryImpl(Class<T> domainClass,
			EntityManager entityManager) {
		super(domainClass, entityManager);

		// This is the recommended method for accessing inherited class
		// dependencies.
		this.domainClass=domainClass;
		this.entityManager = entityManager;
	}

	@Override
	public List<T> findAll() {
		PreSerializer preSerializer = new PreSerializer();
		List<T> result=super.findAll();
		for(Object o:result){
			preSerializer.preventRecursion(o,1);
		}
		return result; 
	}
	
	@Override
	public T findOneEager(ID id) {
		T entity = findOne(id);
		Class entityType = entity.getClass();
		try {
			for (Field f : entityType.getDeclaredFields()) {
				if (f.getType().getCanonicalName().startsWith("com.genericproject")) {
						Object fieldInstance = entityType.getMethod("get" + capLetter(f.getName())).invoke(entity);
						if (!Hibernate.isInitialized(fieldInstance)) {
							Hibernate.initialize(fieldInstance);
						}
						if (fieldInstance instanceof HibernateProxy) {
							fieldInstance = (T) ((HibernateProxy) fieldInstance).getHibernateLazyInitializer().getImplementation();
							entity.getClass().getMethod("set"+capLetter(f.getName()),fieldInstance.getClass()).invoke(entity,fieldInstance);
					    }
				}else if(f.getType().isAssignableFrom(java.util.List.class)){
					List fieldInstance = (List) entityType.getMethod("get" + capLetter(f.getName())).invoke(entity);
					if (!Hibernate.isInitialized(fieldInstance)) {
						Hibernate.initialize(fieldInstance);
					}
					for(int i=0;i<fieldInstance.size();i++){
						if (fieldInstance.get(i) instanceof HibernateProxy) {
							fieldInstance.set(i, ((HibernateProxy) fieldInstance.get(i)).getHibernateLazyInitializer().getImplementation());
					    }
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		entityManager.detach(entity);
		PreSerializer preSerializer = new PreSerializer();
		preSerializer.preventRecursion(entity,2);
		return entity;
	}
	
	@Override
	@SuppressWarnings(value={"unchecked","rawtypes"})
	public <S extends T> S save(S entity) {
		boolean fSave=false;
		boolean fUpdate=false;
		try {
			if(entity.getClass().getMethod("getId").invoke(entity)==null){
				fSave=true;
			}else{
				fUpdate=true;
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//Delete old one to one
		HashMap<String,Object> oneToOneInstances=new HashMap<String, Object>();
		for(Field f:entity.getClass().getDeclaredFields()){
			for(Annotation a:f.getAnnotations()){
				if(a.annotationType().equals(OneToOne.class)){
					String mappedBy=f.getAnnotation(OneToOne.class).mappedBy();
					if(mappedBy!=null && mappedBy.length()!=0){
						try{
							String otherField=mappedBy;
							Class<?> otherType=f.getType();
							String otherTablename=otherType.getAnnotation(Table.class).name();
							Field fi=otherType.getDeclaredField(otherField);
							String otherJoinColumn=fi.getAnnotation(JoinColumn.class).name();
							Object fieldInstance=entity.getClass().getMethod("get"+capLetter(f.getName())).invoke(entity);
							Object idName=fieldInstance.getClass().getDeclaredField("id").getAnnotation(Column.class).name();
							entityManager.createNativeQuery("UPDATE "+otherTablename+" SET "+otherJoinColumn+"=null WHERE "+idName+"="
									+fieldInstance.getClass().getMethod("getId").invoke(fieldInstance)).executeUpdate();
							oneToOneInstances.put(f.getName(), fieldInstance);
							entity.getClass().getMethod("set"+capLetter(f.getName()),f.getType()).invoke(entity,new Object[]{null});
						}catch(Exception e){
							e.printStackTrace();
						}
					}else{
						try{
							Class<?> type=entity.getClass();
							String tablename=type.getAnnotation(Table.class).name();
							String joinColumn=f.getAnnotation(JoinColumn.class).name();
							Object fieldInstance=entity.getClass().getMethod("get"+capLetter(f.getName())).invoke(entity);
							entityManager.createNativeQuery("UPDATE "+tablename+" SET "+joinColumn+"=null WHERE "+joinColumn+"="
									+fieldInstance.getClass().getMethod("getId").invoke(fieldInstance)).executeUpdate();
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
			}
		}
			
		super.save(entity);
		entityManager.detach(entity);
		entityManager.flush();
		entityManager.clear();
		try {
			Class<S> entityType = (Class<S>)entity.getClass();
			for (Field f : entityType.getDeclaredFields()) {
				for(Annotation a:f.getAnnotations()){
					//insert one to many
					if(a.annotationType().equals(OneToMany.class)){
						String otherField=f.getAnnotation(OneToMany.class).mappedBy();
						Class<?> otherType=(Class<?>)((ParameterizedType)f.getGenericType()).getActualTypeArguments()[0];
						String otherTablename=otherType.getAnnotation(Table.class).name();
						Field fi=otherType.getDeclaredField(otherField);
						String otherJoinColumn=fi.getAnnotation(JoinColumn.class).name();
						String otherIdColumn=otherType.getDeclaredField("id").getAnnotation(Column.class).name();
						entityManager.createNativeQuery("UPDATE "+otherTablename+" SET "+otherJoinColumn+"=null WHERE "+otherJoinColumn+"="
								+entity.getClass().getMethod("getId").invoke(entity)).executeUpdate();
						for(Object o:(List)entityType.getMethod("get" + capLetter(f.getName())).invoke(entity)){
							entityManager.createNativeQuery("UPDATE "+otherTablename+" SET "+otherJoinColumn+"="+entity.getClass().getMethod("getId").invoke(entity)
									+" WHERE "+otherIdColumn+"="
									+o.getClass().getMethod("getId").invoke(o)).executeUpdate();
						}
					}
					//insert one to one
					if(a.annotationType().equals(OneToOne.class)){
						String mappedBy=f.getAnnotation(OneToOne.class).mappedBy();
						if(mappedBy!=null && mappedBy.length()!=0){
							try{
								String otherField=mappedBy;
								Class<?> otherType=f.getType();
								String otherTablename=otherType.getAnnotation(Table.class).name();
								Field fi=otherType.getDeclaredField(otherField);
								String otherJoinColumn=fi.getAnnotation(JoinColumn.class).name();
								String otherIdColumn=otherType.getDeclaredField("id").getAnnotation(Column.class).name();
								Object fieldInstance=oneToOneInstances.get(f.getName());
								entityManager.createNativeQuery("UPDATE "+otherTablename+" SET "+otherJoinColumn+"="+entity.getClass().getMethod("getId").invoke(entity)
										+" WHERE "+otherIdColumn+"="
										+fieldInstance.getClass().getMethod("getId").invoke(fieldInstance)).executeUpdate();
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
					
					//insert ManyToMany if save
					if(a.annotationType().equals(ManyToMany.class) && fSave){
						String tableName=f.getAnnotation(JoinTable.class).name();
						String joinColumn=f.getAnnotation(JoinTable.class).joinColumns()[0].name();
						String inverseJoinColumn=f.getAnnotation(JoinTable.class).inverseJoinColumns()[0].name();
						for(Object o:(List)entityType.getMethod("get" + capLetter(f.getName())).invoke(entity)){
							entityManager.createNativeQuery("INSERT INTO "+tableName+" ("+ joinColumn+ ","+ inverseJoinColumn +") VALUES (" 
									+entity.getClass().getMethod("getId").invoke(entity)+","
									+o.getClass().getMethod("getId").invoke(o)+")"
									).executeUpdate();
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		return entity;
	}
	
	protected static String capLetter(String word){
		return word.substring(0, 1).toUpperCase()+ word.substring(1);
	}
}
